
AUTHOR
------
Jimmy Berry ("boombatower", http://drupal.org/user/214218)

PROJECT PAGE
------------
If you need more information, have an issue, or feature request please
visit the project page at: http://drupal.org/project/project_issue_file_review.

DESCRIPTION
-----------
Provides an abstracted client-server model and plugin API for performing
distributed operations such as code review and testing. Currently the project
focuses on supporting Drupal development, but due to the extent of the code
abstraction it can be extended to work with other platforms and environments.

-- Plugins --

The project currently includes the following plugins, but there are plans to
include a performance measuring plugin, code coverage plugin, security plugin,
etc.

  * SimpleTest - runs SimpleTest tests and aggregates results.
  * Coder - reviews code using the coder and coder_tough_love routines.

-- Design --

The project consists of a server, client, and a shared base module that
provides common functionality. The server manages the process, queue, and
results while the client performs all operations and reports back. An optional
project client can also be added to provide an integrated workflow with a
project management tool (for example PIFT which integrates with the drupal.org
project module) or the like. Both the project client API and the test client
API are based on a PULL architecture where the server never makes a request,
but simply responds to them.

-- Usage --

Currently Drupal.org makes use of this platform for its development. The
process is centered around qa.drupal.org which acts as the PIFR server. The
test clients are donated machines that are managed by volunteers. Please see
PIFR FAQs for more information on getting involved with qa.drupal.org.

-- Maintainer --

The project has been developed and is maintained by Jimmy Berry (boombatower).
