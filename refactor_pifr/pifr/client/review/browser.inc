<?php
/**
 * @file
 * Provide wrapper over SimpleTest 6.x-2.x drupal_web_test_case.php.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

require_once drupal_get_path('module', 'simpletest') . '/drupal_web_test_case.php';

class PIFRBrowser extends DrupalWebTestCase {

  public function drupalGet($path, array $options = array(), array $headers = array()) {
    return parent::drupalGet($path, $options, $headers);
  }

  public function drupalPost($path, $edit, $submit, array $options = array(), array $headers = array(), $form_html_id = NULL, $extra_post = NULL) {
    return parent::drupalPost($path, $edit, $submit, $options, $headers, $form_html_id, $extra_post);
  }

  public function xpath($xpath, array $arguments = array()) {
    return parent::xpath($xpath, $arguments);
  }

  public function assertText($text, $message = '', $group = 'Other') {
    return parent::assertText($text, $message, $group);
  }

  public function drupalLogin(stdClass $user) {
    parent::drupalLogin($user);
  }

  protected function assert($status, $message = '', $group = 'Other', array $caller = NULL) {
    // Convert boolean status to string status.
    if (is_bool($status)) {
      $status = $status ? 'pass' : 'fail';
    }

    // Increment summary result counter.
    $this->results['#' . $status]++;

    // Don't waste time storing stuff in database.
  }
}
