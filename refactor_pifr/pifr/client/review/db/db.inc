<?php
/**
 * @file
 * Provide Database interface for performing review operations.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Database interface for performing review operations.
 *
 * All operations should be relative to the 'pifr_checkout' connection as
 * defined in settings.php.
 */
interface pifr_client_db_interface {

  /**
   * Get the database information from the 'pifr_checkout' connection.
   *
   * @return array Associative array of database information, with keys:
   *   name, username, password, and host.
   */
  public function get_information();

  /**
   * Create the database referenced by the 'pifr_checkout' connection.
   *
   * @return boolean TRUE if successfull, otherwise FALSE.
   */
  public function create_database();

  /**
   * Drop the database referenced by the 'pifr_checkout' connection.
   *
   * @return boolean TRUE if successfull, otherwise FALSE.
   */
  public function drop_database();

  /**
   * Get the list of assertions from the {simpletest} table.
   *
   * @param string $sql SQL query to execute.
   * @return array List of rows or FALSE on failure.
   */
  public function query($sql);

  /**
   * Import database dump into the checkout database and prefix all the tables.
   *
   * @param string $file Database dump file location.
   * @return boolean Number of tables imported if successfull, otherwise FALSE.
   */
  public function import($file);
}
