<?php
/**
 * @file
 * SQLite implementation of review interface.
 *
 * @author Karoly Negyesi ("chx", http://drupal.org/user/9446)
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * SQLite implementation of review interface.
 */
class pifr_client_db_interface_sqlite implements pifr_client_db_interface {

  public function get_information() {
    global $db_url;

    $url = parse_url($db_url['pifr_checkout']);
    $db = array();
    $db['name'] = urldecode($url['path']);
    $db['username'] = urldecode($url['user']);
    $db['password'] = urldecode($url['pass']);
    $db['host'] = urldecode($url['host']);
    return $db;
  }

  /**
   * Return a connection to the SQLite database.
   *
   * We use one connection per query. Because we can, and to avoid having to do
   * crazy exception handling.
   */
  protected function connection() {
    $info = $this->getInformation();
    return new PDO('sqlite:' . $info['name'], NULL, NULL, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
  }

  public function create_database() {
    $info = $this->getInformation();

    // Remove any previous database that might share the same name.
    @unlink($info['name']);

    return (bool) $this->connection();
  }

  public function drop_database() {
    // Database file is stored in checkout directory which gets cleared.
    return TRUE;
  }

  public function query($sql) {
    if ($connection = $this->connection()) {
      try {
        $statement = $connection->prepare(db_prefix_tables($sql));
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
      }
      catch (PDOException $e) {
        return array();
      }
    }
    return array();
  }

  public function import($file) {
    if ($this->query(file_get_contents($file))) {
      $information = $this->get_information();
      $database = $information['name'];
      $tables = $this->query("SELECT table_name FROM information_schema.tables WHERE (table_schema = '$database' OR table_catalog = '$database')");
      return count($tables);
    }
    return FALSE;
  }
}
