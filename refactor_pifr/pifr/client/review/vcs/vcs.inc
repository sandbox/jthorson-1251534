<?php

/**
 * @file
 * Provide Version control system interface for performing review operations.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Version control system interface for performing review operations.
 */
abstract class pifr_client_vcs {

  /**
   * Perform a checkout.
   *
   * @param string $directory Directory to place the code in.
   * @param string $url Checkout URL containing all necessary repository info.
   * @param string $branch Branch indentifier.
   * @return boolean TRUE if successfull, otherwise FALSE.
   */
  public abstract function checkout($directory, $url, $branch);

  /**
   * Apply a patch to the codebase.
   *
   * @param string Location of patch file.
   * @return boolean TRUE if successfull, otherwise FALSE.
   */
  public function apply($patch) {
    return pifr_client_review::exec("patch -p0 -i $patch");
  }

  /**
   * Get a list of the files changed by a patch.
   *
   * @param string Location of patch file.
   * @return array List of changed files.
   */
  public function changed_files($patch) {
    $contents = file_get_contents($patch);
    preg_match_all('/^[\+-]{3}\s+(.*?)\s/m', $contents, $matches, PREG_SET_ORDER);

    $files = array();
    foreach ($matches as $match) {
      if ($match[1] != '/dev/null') {
        $files[] = $match[1];
      }
    }
    return array_unique($files);
  }

  /**
   * Get a display value for the checkout information.
   *
   * @return string Display value.
   */
  public function display($url, $branch) {
    return $branch;
  }

  /**
   * Get a map of line number to the last vcs user to touch the line.
   *
   * @param string $file File path relative checkout directory.
   * @return array Associative array keyed on line number.
   */
  public abstract function annotate($file);

  /**
   * Get last commit ID.
   *
   * @return string Last commit ID.
   */
  public abstract function commit_id();
}
