<?php

/**
 * @file
 * CVS implementation of VCS interface.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * CVS implementation of VCS interface.
 */
class pifr_client_vcs_cvs extends pifr_client_vcs {

  public function checkout($directory, $url, $branch) {
    if (!preg_match('/(.*?:\/cvs\/.*?)\/(.*?)$/m', $url, $matches)) {
      return FALSE;
    }
    $url = $matches[1];
    $module = $matches[2];

    // If no directory was specified then use the last part of the module.
    if (!$directory) {
      $directory = basename($module);
    }

    list($url, $directory, $branch, $module) = array_map('escapeshellarg', array($url, $directory, $branch, $module));
    return pifr_client_review::exec("cvs -z6 -Q -d$url checkout -d$directory -r $branch $module");
  }

  public function display($url, $branch) {
    return array_pop(explode('/', $url));
  }

  public function annotate($file) {
    $lines = pifr_client_review::exec_output("cvs annotate $file");

    $map = array();
    $number = 1;
    foreach ($lines as $line) {
      if (!empty($line) && is_numeric($line[0]) && preg_match('/.*?\s+\((.*?)\s+.*?\)|/', $line, $match)) {
        $map[$number++] = $match[1];
      }
    }
    return $map;
  }

  public function commit_id() {
    $lines = pifr_client_review::exec_output('cvs log -bN');

    $latest = 0;
    foreach ($lines as $line) {
      if (preg_match('/date: (.*?);/', $line, $match)) {
        $latest = max(strtotime($match[1]), $latest);
      }
    }
    return $latest;
  }
}
