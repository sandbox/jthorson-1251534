<?php

/**
 * @file
 * SVN implementation of VCS interface.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * SVN implementation of VCS interface.
 */
class pifr_client_vcs_svn extends pifr_client_vcs {

  public function checkout($directory, $url, $branch) {
    $url = escapeshellarg("$url/$branch");
    $directory = escapeshellarg($directory);
    return pifr_client_review::exec("svn checkout --non-interactive --trust-server-cert $url $directory");
  }

  public function annotate($file) {
    $lines = pifr_client_review::exec_output("svn annotate $file");

    $map = array();
    $number = 1;
    foreach ($lines as $line) {
      if (!empty($line) && is_numeric($line[0]) && preg_match('/\d+\s+(.*?) /', $line, $match)) {
        $map[$number++] = $match[1];
      }
    }
    return $map;
  }

  public function commit_id() {
    $lines = pifr_client_review::exec_output('svn up');

    foreach ($lines as $line) {
      if (preg_match('/^(At|Updated to) revision (\d+)\.$/m', $line, $match)) {
        return $match[2];
      }
    }
    return FALSE;
  }
}
