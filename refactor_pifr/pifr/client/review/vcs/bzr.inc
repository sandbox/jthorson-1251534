<?php

/**
 * @file
 * BZR implementation of VCS interface.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * BZR implementation of VCS interface.
 */
class pifr_client_vcs_bzr extends pifr_client_vcs {

  public function checkout($directory, $url, $branch) {
    $url = escapeshellarg("$url/$branch");
    $directory = escapeshellarg($directory);
    return pifr_client_review::exec("bzr checkout --lightweight $url $directory");
  }

  public function annotate($file) {
    $lines = pifr_client_review::exec_output("bzr annotate --long --all $file");

    $map = array();
    $number = 1;
    foreach ($lines as $line) {
      if (preg_match('/\d+\s+(.*?)\s+\d+\s+\|/', $line, $match)) {
        $map[$number++] = $match[1];
      }
    }
    return $map;
  }

  public function commit_id() {
    $lines = pifr_client_review::exec_output('bzr log -l 1');

    foreach ($lines as $line) {
      if (preg_match('/^revno: (\d+)$/m', $line, $match)) {
        return $match[1];
      }
    }
    return FALSE;
  }
}
