<?php

/**
 * @file
 * Provide XML-RPC callbacks and invoke functions.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Retrieve the PIFR version string.
 *
 * @return string PIFR version string.
 */
function pifr_client_xmlrpc_version() {
  $info = parse_ini_file(drupal_get_path('module', 'pifr') . '/pifr.info');
  return (string) (isset($info['version']) ? $info['version'] : 'unknown');
}

/**
 * Request the next test from the server.
 *
 * @return array Test array, or FALSE.
 */
function pifr_client_xmlrpc_request_next() {
  $response = xmlrpc(PIFR_CLIENT_SERVER . 'xmlrpc.php', 'pifr.next', PIFR_CLIENT_KEY);

  if ($response === FALSE) {
    watchdog('pifr_client', 'Failed to request next test: @message', array('@message' => xmlrpc_error_msg()), WATCHDOG_ERROR);
    return FALSE;
  }
  else if (isset($response['response'])) {
    watchdog('pifr_client', 'When requesting next test, server responded: @message',
                      array('@message' => pifr_response_code($response['response'])), WATCHDOG_NOTICE);
    return FALSE;
  }
  else if (!$response) {
    // Empty array, no tests.
    return FALSE;
  }
  pifr_debug('Requested and received test from server: XML-RPC pifr.next() response=<pre>%response</pre>', array('%response' => var_export($response, TRUE)));
  return $response;
}

/**
 * Send the results of review to the server.
 *
 * @param array $result Result information.
 * @return boolean TRUE if succesful, otherwise FALSE.
 */
function pifr_client_xmlrpc_send_result(array $result) {
  $response = xmlrpc(PIFR_CLIENT_SERVER . 'xmlrpc.php', 'pifr.result', PIFR_CLIENT_KEY, $result);

  if ($response === FALSE) {
    watchdog('pifr_client', 'Failed to send result: @message', array('@message' => xmlrpc_error_msg()), WATCHDOG_ERROR);
    return FALSE;
  }
  else if ($response != PIFR_RESPONSE_ACCEPTED) {
    watchdog('pifr_client', 'When sending result, server responded: @message',
                      array('@message' => pifr_response_code($response)), WATCHDOG_NOTICE);
    return FALSE;
  }

  watchdog('pifr_client', 'Test results successfully sent: (t: @test_id).', array('@test_id' => $result['test_id']));
  pifr_debug('Sent result to PIFR server; result=<pre>%result</pre>', array('%result' => var_export($result, TRUE)));

  return TRUE;
}
