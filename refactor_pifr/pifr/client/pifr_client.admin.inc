<?php
/**
 * @file
 * Provide general administration functionality.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Display report value on general dashboard.
 */
function pifr_client_admin_dashboard_form(&$form, $form_state) {
  module_load_include('cron.inc', 'pifr_client');
  $busy = pifr_client_cron_is_busy();
  $form['status']['client'] = array(
    '#type' => 'item',
    '#title' => t('Client'),
    '#value' => ($busy ?
                 t('reviewing (see below for details)') :
                 t('idle') . ' ' . l(t('(run tests locally)'), 'admin/pifr/run-test')) .
                 '<br />',
  );
  $form['status']['toggle_reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset tests'),
    '#submit' => array('pifr_client_admin_dashboard_form_toggle_submit'),
    '#weight' => 1001
  );

  // If the log has events then display the log.
  $log_file = file_directory_path() . '/review.log';
  if (file_exists($log_file) && ($log = file_get_contents($log_file))) {
    if ($busy) {
      module_load_include('review.inc', 'pifr_client');
      $test = variable_get('pifr_client_test', array());
      $class = pifr_client_review_load($test, TRUE);

      $form['review'] = array(
        '#type' => 'fieldset',
        '#title' => t('Review'),
        '#attributes' => array('class' => 'container-inline'),
        '#weight' => -7,
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      $form['review']['plugin'] = array(
        '#type' => 'item',
        '#title' => t('Plugin'),
        '#value' => $test['review']['plugin'] . '<br />',
      );
      $form['review']['status'] = array(
        '#type' => 'item',
        '#title' => t('Status'),
        '#value' => call_user_func(array($class, 'status')) . '<br />',
      );
    }

    $form['log'] = array(
      '#type' => 'fieldset',
      '#title' => t('Log'),
      '#description' => t('Last updated @date.', array('@date' => format_date(filemtime($log_file), 'custom', 'l, m/d/Y @ G:i:s'))),
      '#weight' => -6,
      '#value' => '<pre>' . $log . '</pre>',
      '#collapsible' => TRUE,
      '#collapsed' => !$busy,
    );

    // Hide non-essentials during review.
    if ($busy) {
      $form['role']['#access'] = FALSE;
      $form['cron']['#access'] = FALSE;
    }
  }
}

/**
 * Reset variables.
 */
function pifr_client_admin_dashboard_form_toggle_submit($form, &$form_state) {
  module_load_include('cron.inc', 'pifr_client');
  pifr_client_cron_reset();
}

/**
 * Add report setting to configuration form.
 */
function pifr_client_admin_configuration_form(&$form, $form_state) {
  $form['#validate'][] = 'pifr_client_admin_configuration_form_validate';
  $form['client'] = array(
    '#type' => 'fieldset',
    '#title' => t('Client'),
    '#weight' => -9
  );
  $form['client']['pifr_client_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#description' => t('The key assigned to client.'),
    '#default_value' => PIFR_CLIENT_KEY,
    '#required' => TRUE,
  );
  $form['client']['pifr_client_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Server URL'),
    '#description' => t('The URL of the server to retrive patches from, include ending forward slash.'),
    '#default_value' => PIFR_CLIENT_SERVER,
    '#required' => TRUE,
  );
  $form['client']['pifr_client_concurrency'] = array(
    '#type' => 'select',
    '#title' => t('Concurrency'),
    '#description' => t('The number of concurrent process to use when running tests (see run-tests.sh). ' .
                        'SQLite requires concurrency to be 1.'),
    '#options' => drupal_map_assoc(range(1, 64)),
    '#default_value' => PIFR_CLIENT_CONCURRENCY,
  );
  $form['client']['pifr_client_timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('Testing timeout'),
    '#description' => t('The maximum time, in minutes, allowed for tests to complete.'),
    '#size' => 4,
    '#default_value' => PIFR_CLIENT_TIMEOUT,
    '#required' => TRUE,
  );
  $form['client']['pifr_reference_repository'] = array(
    '#type' => 'textfield',
    '#title' => t('Reference repository'),
    '#description' => t('A reference repo that git can use to obtain objects without downloading them. Like "/var/cache/git/reference" or "git://nearby.com/reference.git".'),
    '#default_value' => variable_get('pifr_reference_repository', ''),
  );
  $form['client']['pifr_drupal_repository'] = array(
    '#type' => 'textfield',
    '#title' => t('Drupal core repository URL'),
    '#default_value' => PIFR_DRUPAL_REPOSITORY,
    '#required' => TRUE,
  );
  $form['client']['pifr_simpletest_repository'] = array(
    '#type' => 'textfield',
    '#title' => t('Simpletest module repository URL'),
    '#default_value' => PIFR_SIMPLETEST_REPOSITORY,
    '#required' => TRUE,
  );
  $form['client']['pifr_chart_repository'] = array(
    '#type' => 'textfield',
    '#title' => t('Chart module repository URL'),
    '#default_value' => PIFR_CHART_REPOSITORY,
    '#required' => TRUE,
  );
  $form['client']['pifr_multivariate_repository'] = array(
    '#type' => 'textfield',
    '#title' => t('Multivariate module repository URL'),
    '#default_value' => PIFR_MULTIVARIATE_REPOSITORY,
    '#required' => TRUE,
  );
  $form['client']['pifr_menu_rewrite_repository'] = array(
    '#type' => 'textfield',
    '#title' => t('Menu Rewrite module repository URL'),
    '#default_value' => PIFR_MENU_REWRITE_REPOSITORY,
    '#required' => TRUE,
  );
}

/**
 * Validate client settings.
 */
function pifr_client_admin_configuration_form_validate($form, &$form_state) {
  if (drupal_strlen($form_state['values']['pifr_client_key']) != 32) {
    form_set_error('pifr_client_key', t('Key must be 32 characters long.'));
  }

  if (!$form_state['values']['pifr_client_server'] || !@parse_url($form_state['values']['pifr_client_server'])) {
    form_set_error('pifr_client_server', t('Server URL must be a complete URL, schema included.'));
  }
  else if (drupal_substr($form_state['values']['pifr_client_server'],
                          drupal_strlen($form_state['values']['pifr_client_server']) - 1) != '/') {
    form_set_error('pifr_client_server', t('Server URL must end with a slash.'));
  }
  else {
    $result = xmlrpc($form_state['values']['pifr_client_server'] . 'xmlrpc.php', 'system.listMethods');
    if ($result !== FALSE) {
      // Check for PIFR server methods.
      if (!is_array($result) || !in_array('pifr.next', $result)) {
        form_set_error('pifr_client_server', t('The server does not appear to be running PIFR server 2.x.'));
      }
    }
    else {
      form_set_error('pifr_client_server', t('URL does not point to a valid server XML-RPC root.'));
    }
  }
}

/**
 * Test the server configuration and display the results.
 *
 * @return string HTML output.
 */
function pifr_client_admin_check_configuration() {
  drupal_add_css(drupal_get_path('module', 'pifr') . '/pifr.css');
  drupal_add_css(drupal_get_path('module', 'pifr_client') . '/pifr_client.css');

  $sections = array(
    'connection' => t('Firewall/Connection'),
    'cli' => t('Command line tools'),
    'extensions' => t('PHP Extensions'),
    'php_config' => t('PHP Configuration'),
  );

  $out = '';
  foreach ($sections as $key => $section) {
    $out .= '<h3>' . $section . '</h3>';
    $out .= '<div class="pifr-client-section">';

    $function  = 'pifr_client_admin_check_configuration_' . $key;
    $out .= $function();
    $out .= '</div>';
  }
  return $out;
}

/**
 * Test the firewall rules and connection abilities.
 *
 * Ensure that the server can communicate with itself ($base_url), drupal.org,
 * testing.drupal.org, and cvs.drupal.org as required.
 *
 * @return string HTML output.
 */
function pifr_client_admin_check_configuration_connection() {
  $connections = array(
    'required' => array(
      'title' => t('Required'),
      'description' => t('All of the following connection tests must pass in order for your slave to ' .
                         'be able to properly communicate with the testing server.'),
      'tests' => array(
        array(
          'hostname' => 'drupal.org',
          'port' => 80,
        ),
        array(
          'hostname' => 'testing.drupal.org',
          'port' => 80,
        ),
        array(
          'hostname' => 'cvs.drupal.org',
          'port' => 2401,
        ),
        array(
          'hostname' => parse_url(url('', array('absolute' => TRUE)), PHP_URL_HOST),
          'port' => 80,
        ),
      ),
    ),
    'recommended' => array(
      'title' => t('Recommended'),
      'description' => t('Because of the possibility that insecure code in a patch, being tested on this ' .
                         'machine, could be executed, it is recommended that outgoing connections from ' .
                         'this server be blocked except when required. <em>Therefore, for the most secure ' .
                         'configuration, all of the following tests should fail.</em>'),
      'tests' => array(
        array(
          'hostname' => 'www.google.com',
          'port' => 80,
        ),
        array(
          'hostname' => 'mail.google.com',
          'port' => 25,
        ),
      ),
    ),
  );

  $out = '';
  foreach ($connections as $key => $connection) {
    $out .= '<h4 class="pifr-test-connection-title">' . $connection['title'] . '</h4>';
    $out .= '<div class="pifr-test-connection-description">' . $connection['description'] . '</div>';
    $header = array(t('Host name'), t('Port'), t('Result'), t('Error message'));
    $rows = array();
    foreach ($connection['tests'] as $test) {
      $result = pifr_client_admin_test_connection($test['hostname'], $test['port']);
      if (($key == 'recommended' && empty($result['error_number'])) ||
          ($key != 'recommended' && (!empty($result['error_number']) || !empty($result['error_message'])))) {
        $rows[] = array(
          'data' => array($test['hostname'], $test['port'], t('Failed'), $result['error_message']),
          'class' => 'pifr-fail',
        );
      }
      else {
        $rows[] = array(
          'data' => array($test['hostname'], $test['port'], t('Passed'), $result['error_message']),
          'class' => 'pifr-pass',
        );
      }
    }
    $out .= theme('table', $header, $rows);
  }
  return $out;
}

/**
 * Check for required command line tools.
 *
 * @return string HTML output.
 */
function pifr_client_admin_check_configuration_cli() {
  $commands = array(
    array(
      'title' => 'patch',
      'cmd' => 'patch -v',
    ),
    array(
      'title' => 'git',
      'cmd' => 'git --version',
    ),
    array(
      'title' => 'curl',
      'cmd' => 'curl --version',
    ),
    array(
      'title' => 'php (cli)',
      'cmd' => 'php -v',
    ),
    array(
      'title' => 'grep',
      'cmd' => 'grep --version',
    ),
    array(
      'title' => 'mysql',
      'cmd' => 'mysql --version',
    ),
    array(
      'title' => 'postgres',
      'cmd' => 'psql --version',
    ),
  );

  $header = array(t('Name'), t('Shell command'), t('Output'));
  $rows = array();
  foreach ($commands as $command) {
    $cmd_output = trim(check_plain(shell_exec($command['cmd'])));
    $rows[] = array(
      'data' => array($command['title'], check_plain($command['cmd']), '<code>' . $cmd_output . '</code>'),
      'class' => $cmd_output ? 'pifr-pass' : 'pifr-fail',
    );
  }

  return theme('table', $header, $rows);
}

/**
 * Check for required PHP Extensions.
 *
 * @return string HTML output.
 */
function pifr_client_admin_check_configuration_extensions() {
  $header = array(t('Extension name'), t('Minimum required version'), t('Version found'), t('Result'));
  $rows = array();

  $extensions = array(
    'curl' => array(
      'title' => t('CURL'),
      'url' => 'http://www.php.net/manual/en/book.curl.php',
      'version_min' => '7',
    ),
    'gd' => array(
      'title' => t('gd'),
      'url' => 'http://www.php.net/manual/en/ref.image.php',
      'version_min' => '2',
    ),
    'mbstring' => array(
      'title' => t('mbstring'),
      'url' => 'http://www.php.net/manual/en/book.mbstring.php',
      'version_min' => UNICODE_MULTIBYTE,
    ),
    'pdo' => array(
      'title' => t('pdo'),
      'url' => 'http://www.php.net/manual/en/book.pdo.php',
      'version_min' => '1',
    ),
  );

  foreach ($extensions as $key => $extension) {
    $version_found = '';
    $pass = NULL;
    switch ($key) {
      case 'curl':
        if (function_exists('curl_version')) {
          $info = curl_version();
          $version_found = $info['version'];
        }
        break;
      case 'gd':
        if (function_exists('imagegd2')) {
          $version_found = '2';
        }
        break;
      case 'mbstring':
        $version_found = $GLOBALS['multibyte'];
        $pass = $extension['version_min'] <= $version_found;
        break;
      case 'pdo':
        $version_found = phpversion('pdo');
        $pass = $extension['version_min'] <= $version_found;
        break;
    }

    $pass = is_null($pass) ? version_compare($extension['version_min'], $version_found) <= 0 : $pass;
    $rows[] = array(
      'data' => array(l($extension['title'], $extension['url'], array('absolute' => TRUE)),
                      $extension['version_min'], $version_found, $pass ? t('Passed') : t('Failed')),
      'class' => $pass ? 'pifr-pass' : 'pifr-fail',
    );
  }

  return theme('table', $header, $rows);
}

/**
 * Check for recommended PHP configuration.
 *
 * @return string HTML output.
 */
function pifr_client_admin_check_configuration_php_config() {
  $header = array(t('Setting'), t('Recommended value'), t('Value found'), t('Result'));
  $rows = array();

  // Test memory limit (Apache).
  $result = FALSE;
  $memory_limit = ini_get('memory_limit');
  if (parse_size($memory_limit) >= parse_size(PIFR_RECOMMENDED_MEMORY)) {
    $result = TRUE;
  }
  $rows[] = array(
    'data' => array(l('memory_limit', 'http://www.php.net/manual/en/ini.core.php#ini.memory-limit', array('absolute' => TRUE)),
                   PIFR_RECOMMENDED_MEMORY, $memory_limit, $result ? t('Passed') : t('Failed')),
    'class' => $result ? 'pifr-pass' : 'pifr-fail',
  );

  // Test memory limit (cli).
  $result = FALSE;
  $memory_limit = `php -r "echo ini_get('memory_limit');"`;
  if (parse_size($memory_limit) >= parse_size(PIFR_RECOMMENDED_MEMORY)) {
    $result = TRUE;
  }
  $rows[] = array(
    'data' => array(l('memory_limit (cli)', 'http://www.php.net/manual/en/ini.core.php#ini.memory-limit',
                   array('absolute' => TRUE)), PIFR_RECOMMENDED_MEMORY, $memory_limit, $result ? t('Passed') : t('Failed')),
    'class' => $result ? 'pifr-pass' : 'pifr-fail',
  );

  return theme('table', $header, $rows);
}

/**
 * Test connectivity to a host via a certain port.
 *
 * @param string $hostname A host name or IP address to connect to.
 * @param integer $port The port number to connect to.
 * @param interger $timeout The number of seconds to wait before timing out on
 *   the connection attempt.
 * @return
 *   A keyed array containing the error number and error string as returned
 *   by the PHP function fsockopen().
 */
function pifr_client_admin_test_connection($hostname, $port, $timeout = 2) {
  $error_number = NULL;
  $error_string = '';
  $fp = @fsockopen($hostname, $port, $error_number, $error_message, $timeout);
  if ($fp) {
    fclose($fp);
  }
  return array('error_number' => $error_number, 'error_message' => $error_message);
}
