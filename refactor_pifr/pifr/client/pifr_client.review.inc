<?php
/**
 * @file
 * Provide local review functions.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Run the review.
 */
function pifr_client_review_run() {
  if (PIFR_CLIENT_TEST_START) {
    watchdog('pifr_client', 'Attempt to start review while already running.', array(), WATCHDOG_ALERT);
    return;
  }
  if (!variable_get('pifr_client_test', FALSE)) {
    watchdog('pifr_client', 'Attempt to start review without a test.', array(), WATCHDOG_ERROR);
    return;
  }

  // Load test information.
  $test = variable_get('pifr_client_test', FALSE);

  // Keep track of how long review has been running.
  variable_set('pifr_client_test_start', time());

  $review = pifr_client_review_load($test);
  if ($review) {
    $review->run();
  }

  // Reset variables.
  module_load_include('cron.inc', 'pifr_client');
  pifr_client_cron_clear_variables();

  // Allow for reporting to be disabled by addon modules.
  if (!defined('PIFR_CLIENT_REPORT') || PIFR_CLIENT_REPORT) {
    // Report results.
    module_load_include('xmlrpc.inc', 'pifr_client');
    if ($review) {
      pifr_client_xmlrpc_send_result($review->get_result());
    }

    if (PIFR_ACTIVE) {
      // Request next test.
      pifr_client_cron_request_next();
    }
  }
}

/**
 * Load review base class and plugin specific class.
 *
 * @param array $test Test information.
 * @param $return_class Return the class name instead of instance.
 * @return object Instantiated review class, or FALSE if invalid review plugin.
 */
function pifr_client_review_load(array $test, $return_class = FALSE) {
  // Load base review class.
  require_once drupal_get_path('module', 'pifr') . '/review/client.inc';

  // Load review plugin and run review.
  if (module_exists($test['review']['plugin'])) {
    require_once drupal_get_path('module', $test['review']['plugin']) . '/' . $test['review']['plugin'] . '.client.inc';
    $class = 'pifr_client_review_' . $test['review']['plugin'];

    if ($return_class) {
      return $class;
    }

    // Initialize review.
    return new $class($test);
  }
  else {
    watchdog('pifr_client', 'Review plugin module not found [@plugin].', array('@plugin' => $test['review']['plugin']), WATCHDOG_ERROR);
  }
  return FALSE;
}

/**
 * Get the review database type.
 *
 * @return string Review database type, either: mysql, pgsql, or sqlite, or
 *   FALSE if it could not be determined.
 */
function pifr_client_review_database_type() {
  global $db_url;
  static $type = FALSE;

  if (!$type) {
    $type = parse_url($db_url['pifr_checkout'], PHP_URL_SCHEME);
    foreach (array('mysql', 'pgsql', 'sqlite') as $backend) {
      if (strpos($type, $backend) !== FALSE) {
        $type = $backend;
        break;
      }
    }
  }
  return $type;
}

/**
 * Client review form.
 */
function pifr_client_review_form(&$form_state) {
  // Display plugin-specific form if $form_state['storage']['plugin'] is set.
  if (isset($form_state['storage']['plugin'])) {
    switch ($form_state['storage']['plugin']) {
      case 'coder':
        return pifr_client_review_form_coder($form_state);
        break;
      case 'simpletest':
      default:
        return pifr_client_review_form_simpletest($form_state);
    }
  }

  // No plugin set, so we display the plugin selection form.
  $form = array();

  $form['testtype'] = array(
    '#type' => 'fieldset',
    '#title' => t('Test Type'),
  );
  $options = array();
  if (module_exists('pifr_simpletest')) {
    $options['simpletest'] = t('SimpleTest');
  }
  if (module_exists('pifr_coder')) {
    $options['coder'] = t('Coder');
  }
  if (!empty($options)) {
    $form['testtype']['test'] = array(
      '#type' => 'select',
      '#title' => t('Select test plugin'),
      '#options' => $options,
      '#default_value' => 'pifr_simpletest',
      '#description' => t('Represents the review plugin which will be invoked for this test.'),
    );
  }
  else {
    $form['testtype']['test'] = array(
      '#type' => 'markup',
      '#value' => t('No test plugins enabled.  Please enable at least one PIFR test plugin (e.g. pifr_simpletest, pifr_coder).'),
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    );
    return $form;
  }
  $form['testtype']['target'] = array(
    '#type' => 'radios',
    '#title' => t('Test Type'),
    '#options' => array(
      'core' => 'Core',
      'branch' => 'Project Branch/Tag',
      'file' => 'File',
      'patch' => 'Patch',
    ),
    '#description' => t('Represents the object type which this test will target.  (For Simpletest tests, File and Patch are equivalent.)'),
    '#default_value' => 'branch',
  );
  $form['testtype']['interrupt'] = array(
    '#type' => 'checkbox',
    '#title' => t('Interrupt any existing tests in progress'),
  );

  $form['testtype']['selecttype'] = array(
    '#type' => 'submit',
    '#value' => t('Next >>'),
  );

  return $form;
}

/**
 * Client review form validation
 */
function pifr_client_review_form_validate($form, &$form_state) {

  // Ensure testbot is not actively testing
  if (variable_get('pifr_client_test', FALSE)) {
    module_load_include('inc', 'pifr_client', 'pifr_client.cron');

    $test = variable_get('pifr_client_test', FALSE);
    if ($form_state['values']['interrupt'] == TRUE) {
      // Reset client to prepare for next test.
      watchdog('pifr_client', 'Reset client to allow for local test run (t: @test_id).', array('@test_id' => $test['test_id']));
      pifr_client_cron_clear_variables();
      pifr_client_cron_test_processes_kill(pifr_client_cron_test_processes_get());
    }
    else {
      form_set_error('form', t('Unable to test due to an existing test in progress (t: @test_id).', array('@test_id' => $test['test_id'])));
    }
  }

  if (isset($form_state['storage']['plugin'])) {
    switch ($form_state['storage']['plugin']) {
      case 'coder':
        // Coder Form Validation Steps
        pifr_client_review_form_validate_coder($form, &$form_state);
        break;
      case 'simpletest':
      default:
        // Simpletest Form Validation Steps
        pifr_client_review_form_validate_simpletest($form, &$form_state);
        break;
    }
  }
}

/**
 * Client review form submission
 */
function pifr_client_review_form_submit($form, &$form_state) {

  if ($form_state['clicked_button']['#id'] == 'edit-selecttype') {
    // Handle plugin selection submission
    $form_state['storage']['plugin'] = check_plain($form_state['values']['test']);
    $form_state['storage']['target'] = check_plain($form_state['values']['target']);
  }
  else {
    switch ($form_state['storage']['plugin']) {
      case 'coder':
        // Coder Form Submission Steps
        pifr_client_review_form_submit_coder($form, $form_state);
        break;
      case 'simpletest':
      default:
        // Simpletest Form Submission Steps
        pifr_client_review_form_submit_simpletest($form, $form_state);
        break;
    }
    $form_state['rebuild'] = TRUE;
    unset($form_state['storage']['plugin']);
  }
}

/**
 * Client simpletest review form.
 */
function pifr_client_review_form_simpletest(&$form_state) {
  $form = array();

  $form['runsimpletest'] = array(
    '#type' => 'fieldset',
    '#title' => t('Customize Simpletest Test Parameters'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['runsimpletest']['version'] = array(
    '#type' => 'select',
    '#title' => t('Drupal Core Version'),
    '#options' => array(
      'd6' => t('Drupal 6'),
      'd7' => t('Drupal 7'),
    ),
    '#default_value' => 'd7',
  );

  if ($form_state['storage']['target'] != 'core') {
    $form['branchtests'] = array(
      '#type' => 'fieldset',
      '#title' => t('Identify Review Target'),
      '#collapsible' => FALSE,
    );

    $desc = t('Enter the name of the project you wish to test.');
    $desc .= ' ' . t('The value of this field needs to match the modules folder name inside your modules folder when installed.');
    $desc .= ' ' . t('For example, if your module normally resides in the "sites/all/modules/mymodule" directory, enter "mymodule".');

    $form['branchtests']['projectname'] = array(
      '#type' => 'textfield',
      '#title' => t('Project Name'),
      '#description' => $desc,
      '#required' => TRUE,
    );

    $desc = t('Please enter the Git repository location for the project which contains the branch or tag to test.');
    $desc .= ' ' . t('This may be a full project link such as <em>git://git.drupal.org/project/drupal.git</em>,');
    $desc .= ' ' . t('or a sandbox project link such as <em>git://git.drupal.org/sandbox/username/12345678.git</em>.');

    $form['branchtests']['repository'] = array(
      '#type' => 'textfield',
      '#title' => t('Project Repository'),
      '#description' => $desc,
      '#required' => TRUE,
    );

    $desc = t('Please indicate the branch or tag for which you want to run the test.');
    $desc .= ' ' . t('This value must match an existing tag or branch within your Git repository.');
    $desc .= ' ' . t('(Valid entries are listed under the "tags" and "heads" labels on the project repository summary page.)');

    $form['branchtests']['branch'] = array(
      '#type' => 'textfield',
      '#title' => t('Project Branch'),
      '#description' => $desc,
      '#required' => TRUE,
    );
  }

  switch ($form_state['storage']['target']) {
    case 'core':
      $form['runsimpletest']['coretest'] = array(
        '#type' => 'radios',
        '#title' => t('Type of core test to run'),
        '#options' => array(
          t('Full suite (All test cases)'),
          t('Quick test (Single test case)'),
          t('Failure scenario (Apply bad patch)'),
        ),
        '#default_value' => 0,
        '#description' => t('Full suite will be effected by the debug setting which can be set manually or auto triggered by the presence of SimpleTest.'),
      );

      // No additional parameters required for a 'core' review
      break;
    case 'file':
    case 'patch':
      $form['branchtests']['patchfile'] = array(
        '#type' => 'file',
        '#title' => t('Upload patch file'),
        '#size' => 40,
        '#weight' => 5,
      );
/*
      $form['branchtests']['patchdir'] = array(
        '#type' => 'textfield',
        '#title' => t('Patch root directory'),
        '#size' => 40,
        '#weight' => 6,
        '#description' => t('Defines the directory from which the patch should be applied, relative to the drupal root.  (Example:  sites/default/modules/modulename)'),
      );
*/
      $form['#attributes']['enctype'] = "multipart/form-data";
  }

  $form['op'] = array(
    '#type' => 'submit',
    '#value' => t('Run Test'),
  );

  return $form;
}

/**
 * Validate the client simpletest review form.
 */
function pifr_client_review_form_validate_simpletest($form, &$form_state) {
  // Target-specific validation
  if (isset($form_state['storage']['target'])) {
    switch ($form_state['storage']['target']) {
      case 'file':
      case 'patch':
        if (isset($_FILES['files']) && is_uploaded_file($_FILES['files']['tmp_name']['patchfile'])) {
          // Verify our local patches directory exists
          $path = file_create_path("localpatches");
          file_check_directory($path, FILE_CREATE_DIRECTORY);
          // Attempt to save the uploaded patch file
          $file = file_save_upload('patchfile', array('pifr_client_validate_patchname' => array($_FILES['files'])), $path, FILE_EXISTS_REPLACE);
          // Set error if file was not uploaded
          if (!$file) {
            form_set_error('patchfile', 'Error uploading patch file.');
            return;
          }
          // Set files to form_state for processing
          $form_state['values']['file'] = $file;
        }
      default:
        break;
    }
  }
}

/**
 * Submit the client simpletest review form.
 */
function pifr_client_review_form_submit_simpletest($form, &$form_state) {
  // Get the default arguments to be passed to client
  module_load_include('review.inc', 'pifr_server');
  $plugin = pifr_server_review_plugin_load('pifr_simpletest');
  $default = $plugin->argument_default_get();

  // Generate basic simpletest $test array
  $test = array(
    'test_id' => 17,
    'review' => array(
      'plugin' => 'pifr_simpletest',
      'argument' => $default,
    ),
    'vcs' => array(
      'main' => array(
        'repository' => array(
          'type' => 'git',
          'url' => variable_get('pifr_drupal_repository', 'git://git.drupal.org/project/drupal.git'),
        ),
        'vcs_identifier' => variable_get('pifr_confirmation_vcs_id', '7.2'),
      ),
      'dependencies' => array(),
    ),
    'files' => array(),
  );

  // Update vcs_identifier (and dependencies if needed)
  if ($form_state['values']['version'] == 'd6') {
    $vcs_identifier = PIFR_DRUPAL6_CURRENTSTABLEBRANCH;
    $test['review']['argument']['drupal.core.version'] = 6;
  }
  elseif ($form_state['values']['version'] == 'd7') {
    $vcs_identifier = PIFR_DRUPAL7_CURRENTSTABLEBRANCH;
    $test['review']['argument']['drupal.core.version'] = 7;
  }
  // Update with user-specific values
  switch($form_state['storage']['target']) {
    case 'patch':
    case 'file':
      $test['files'][] = file_create_url(file_create_path('localpatches') . '/' . $form_state['values']['file']->filename);
    case 'branch':
      // Update the repository section with the project to be reviewed
      $test['vcs']['main'] = array(
        'repository' => array(
          'type' => 'git',
          'url' => check_plain($form_state['values']['repository']),
        ),
        'vcs_identifier' => check_plain($form_state['values']['branch']),
      );
      // Set the core repository values as a dependency
      $test['vcs']['dependencies'][] = array(
        'repository' => array(
          'type' => 'git',
          'url' => variable_get('pifr_drupal_repository', 'git://git.drupal.org/project/drupal.git'),
        ),
        'vcs_identifier' => $vcs_identifier,
      );
      // Set the test directory argument
      $test['review']['argument']['test.directory.review'] = array('sites/default/modules/' . check_plain($form_state['values']['projectname']));
      break;
    case 'core':
      if ($form_state['values']['coretest'] == 1) {
        // Quick test
        if ($form_state['values']['version'] == 'd7') {
          $test['review']['argument']['simpletest.tests'] = array('NonDefaultBlockAdmin');
        }
        elseif ($form_state['values']['version'] == 'd6') {
          $test['review']['argument']['simpletest.tests'] = array('SimpleTestFolderTestCase');
        }
      }
      elseif ($form_state['values']['coretest'] == 2) {
        // Failure Scenario
        $test['files'] = array(
          url(drupal_get_path('module', 'pifr_simpletest') . '/confirmation/fail.patch', array('absolute' => TRUE)),
        );
      }
      break;
    default:
      break;
  }

  // Set the test variable so that the admin interface reacts accordingly.
  variable_set('pifr_client_test', $test);

  if ($review = pifr_client_review_load($test)) {
    variable_set('pifr_client_test_start', time());
    $review->run();

    // Display test information.
    drupal_set_message('Test: ' . pifr_client_review_export($test));
    drupal_set_message('Results: ' . pifr_client_review_export($review->get_result()));

    variable_del('pifr_client_test');
    variable_del('pifr_client_test_start');
  }
}

/**
 * Client coder review form.
 */
function pifr_client_review_form_coder(&$form_state) {
  $form = array();

  $form['runcoder'] = array(
    '#type' => 'fieldset',
    '#title' => t('Customize Coder Test Parameters'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['runcoder']['version'] = array(
    '#type' => 'select',
    '#title' => t('Drupal Core Version'),
    '#options' => array(
      'd6' => t('Drupal 6'),
      'd7' => t('Drupal 7'),
    ),
    '#default_value' => 'd7',
  );

  $reviews = array(
    'style' => t('Drupal Coding Standards'),
    'comment' => t('Drupal Commenting Standards'),
    'sql' => t('Drupal SQL Standards'),
    'security' => t('Drupal Security Standards'),
    'i18n' => t('Internationalization'),
    'coder_tough_love' => t('Coder Tough Love'),
  );
  $form['runcoder']['reviews'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Coder Reviews to run'),
    '#options' => $reviews,
    '#description' => t('Represents which Coder review components to enable for this test.'),
    '#default_value' => array('style', 'comment', 'sql', 'security', 'i18n'),
  );

  $form['runcoder']['severity'] = array(
    '#type' => 'radios',
    '#title' => t('Coder Severity warning level'),
    '#options' => array(
      'minor (most warnings)',
      'normal',
      'critical (fewest warnings)',
    ),
    '#default_value' => 1,
    '#description' => t('Represents the severity level of errors which will be considered a failed test'),
  );

  switch ($form_state['storage']['target']) {
    case 'core':
      // No additional parameters required for a 'core' review
      break;
    default:
      $form['branchtests'] = array(
        '#type' => 'fieldset',
        '#title' => t('Identify Review Target'),
        '#collapsible' => FALSE,
      );

      $desc = t('Please enter the Git repository location for the project which contains the branch or tag to test.');
      $desc .= ' ' . t('This may be a full project link such as <em>git://git.drupal.org/project/drupal.git</em>,');
      $desc .= ' ' . t('or a sandbox project link such as <em>git://git.drupal.org/sandbox/username/12345678.git</em>.');

      $form['branchtests']['repository'] = array(
        '#type' => 'textfield',
        '#title' => t('Project Repository'),
        '#description' => $desc,
      );

      $desc = t('Please indicate the branch or tag for which you want to run the test.');
      $desc .= ' ' . t('This value must match an existing tag or branch within your Git repository.');
      $desc .= ' ' . t('(Valid entries are listed under the "tags" and "heads" labels on the project repository summary page.)');

      $form['branchtests']['branch'] = array(
        '#type' => 'textfield',
        '#title' => t('Project Branch'),
        '#description' => $desc,
      );
  }

  // Add the 'file' or 'patch' form fields as needed
  if ($form_state['storage']['target'] == 'file') {

    $desc = t('Please supply the name and path of the file (relative to the git project directory above) which you want code reviewed.');
    $desc .= ' ' . t('(For example, if you want to review the "needsreview.inc" file in your module "includes" directory, enter "includes/needsreview.inc".)');

    $form['branchtests']['filename'] = array(
      '#type' => 'textfield',
      '#title' => t('Project File (Path + Name)'),
      '#description' => $desc,
      '#weight' => 5,
    );
  }
  elseif ($form_state['storage']['target'] == 'patch') {
    $form['branchtests']['patchfile'] = array(
      '#type' => 'file',
      '#title' => t('Upload patch file'),
      '#size' => 40,
    );
    $form['#attributes']['enctype'] = "multipart/form-data";
  }


  $form['op'] = array(
    '#type' => 'submit',
    '#value' => t('Run Test'),
  );

  return $form;
}

/**
 * Validate the client coder review form
 */
function pifr_client_review_form_validate_coder($form, &$form_state) {
  // At least one coder review must be checked
  if (count($form_state['values']['reviews']) < 1) {
    form_set_error('reviews', 'Please select at least one coder review component');
  }
  // Target-specific validation

  if (isset($form_state['storage']['target'])) {
    switch ($form_state['storage']['target']) {
      case 'patch':
        if (isset($_FILES['files']) && is_uploaded_file($_FILES['files']['tmp_name']['patchfile'])) {
          // Verify our local patches directory exists
          $path = file_create_path("localpatches");
          file_check_directory($path, FILE_CREATE_DIRECTORY);
          // Attempt to save the uploaded patch file
          $file = file_save_upload('patchfile', array('pifr_client_validate_patchname' => array($_FILES['files'])), $path, FILE_EXISTS_REPLACE);
          // Set error if file was not uploaded
          if (!$file) {
            form_set_error('patchfile', 'Error uploading patch file.');
            return;
          }
          // Set files to form_state for processing
          $form_state['values']['file'] = $file;
        }
      default:
        break;
    }
  }
}

/**
 * Validate patch file name
 */
function pifr_client_validate_patchname($files) {
  if (substr($files->filename, strlen($files->filename) - 6, 6) != '.patch') {
    return array('Invalid extension on patch file.');
  }
  return array();
}

/**
 * Submit the client coder review form
 */
function pifr_client_review_form_submit_coder($form, &$form_state) {
  // Get the default arguments to be passed to client.
  module_load_include('review.inc', 'pifr_server');
  $plugin = pifr_server_review_plugin_load('pifr_coder');
  $default = $plugin->argument_default_get();
  // Generate basic coder $test array
  $test = array(
    'test_id' => 17,
    'review' => array(
      'plugin' => 'pifr_coder',
      'argument' => $default,
    ),
    'vcs' => array(
      'main' => array(
        'repository' => array(
          'type' => 'git',
          'url' => variable_get('pifr_drupal_repository', 'git://git.drupal.org/project/drupal.git'),
        ),
        'vcs_identifier' => variable_get('pifr_confirmation_vcs_id', '7.x'),
      ),
      'dependencies' => array(),
    ),
    'files' => array(),
  );

  // Update vcs_identifier
  if ($form_state['values']['version'] == 'd6') {
    $test['vcs']['main']['vcs_identifier'] = PIFR_DRUPAL6_CURRENTSTABLEBRANCH;
    $test['review']['argument']['drupal.core.version'] = 6;
  }
  elseif ($form_state['values']['version'] == 'd7') {
    $test['vcs']['main']['vcs_identifier'] = PIFR_DRUPAL7_CURRENTSTABLEBRANCH;
    $test['review']['argument']['drupal.core.version'] = 7;
  }
  // Update with user-specific values
  switch ($form_state['storage']['target']) {
    case 'branch':
      // Update the 'vcs' section with the project to be reviewed.
      // No need to clone a full drupal install for code review!
      $test['vcs']['main']['repository']['url'] = check_plain($form_state['values']['repository']);
      $test['vcs']['main']['vcs_identifier'] = check_plain($form_state['values']['branch']);
      break;
    case 'file':
      // Update the 'vcs' section with the project to be reviewed.
      // No need to clone a full drupal install for code review!
      $test['vcs']['main']['repository']['url'] = check_plain($form_state['values']['repository']);
      $test['vcs']['main']['vcs_identifier'] = check_plain($form_state['values']['branch']);
      $test['review']['argument']['test.files'][] = check_plain($form_state['values']['filename']);
      break;
    case 'patch':
      $test['vcs']['main']['repository']['url'] = check_plain($form_state['values']['repository']);
      $test['vcs']['main']['vcs_identifier'] = check_plain($form_state['values']['branch']);
      // We stored the file in /localpatches in the files directory
      $test['files'][] = file_create_url(file_create_path('localpatches') . '/' . $form_state['values']['file']->filename);
      break;
    default:
      break;
  }

  // Update coder.reviews
  $test['review']['argument']['coder.reviews'] = $form_state['values']['reviews'];

  // Update coder.severity
  switch ($form_state['values']['severity']) {
    case 0:
      $test['review']['argument']['coder.severity'] = SEVERITY_MINOR;
      break;
    case 1:
      $test['review']['argument']['coder.severity'] = SEVERITY_NORMAL;
      break;
    case 2:
      $test['review']['argument']['coder.severity'] = SEVERITY_CRITICAL;
      break;
  }

  variable_set('pifr_client_test', $test);

  if ($review = pifr_client_review_load($test)) {
    variable_set('pifr_client_test_start', time());
    $review->run();

    // Display test information.
    drupal_set_message('Test: ' . pifr_client_review_export($test));
    drupal_set_message('Results: ' . pifr_client_review_export($review->get_result()));

    variable_del('pifr_client_test');
    variable_del('pifr_client_test_start');
  }
}

/**
 * Export a variable.
 *
 * @param mixed $var Variable to print.
 * @return string Exported variables.
 */
function pifr_client_review_export($var) {
  return '<div>' . highlight_string("<?php\n" . var_export($var, TRUE) . "\n?>", TRUE) . '</div>';
}

