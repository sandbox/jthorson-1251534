<?php
/**
 * @file
 * Provide cron functions.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

function pifr_client_cron_run() {
  if (pifr_client_cron_is_busy() && time() > PIFR_CLIENT_TEST_START + (PIFR_CLIENT_TIMEOUT * 60)) {
    // Reset client to prepare for next test.
    $test = variable_get('pifr_client_test', FALSE);
    watchdog('pifr_client', 'Reset client due to test timeout (t: @test_id).', array('@test_id' => $test['test_id']));

    pifr_client_cron_clear_variables();
    pifr_client_cron_test_processes_kill(pifr_client_cron_test_processes_get());
  }

  if (!pifr_client_cron_is_busy()) {
    pifr_client_cron_request_next();
  }
}

function pifr_client_cron_request_next() {
  module_load_include('xmlrpc.inc', 'pifr_client');
  if ($test = pifr_client_xmlrpc_request_next()) {
    variable_set('pifr_client_test',  $test);

    pifr_client_cron_exec_background('curl --insecure ' . url('pifr/client/review', array('absolute' => TRUE)));
    return TRUE;
  }
  return FALSE;
}

function pifr_client_cron_is_busy() {
  return variable_get('pifr_client_test', FALSE) || pifr_client_cron_test_processes_get();
}

function pifr_client_cron_reset() {
  watchdog('pifr_client', 'Client reset by administrator.');

  pifr_client_cron_clear_variables();
  pifr_client_cron_test_processes_kill(pifr_client_cron_test_processes_get());
}

function pifr_client_cron_clear_variables() {
  variable_del('pifr_client_test');
  variable_del('pifr_client_test_start');
}

/**
 * Find any PHP processes that are running the tests.
 *
 * @return array List of process IDs (pids).
 */
function pifr_client_cron_test_processes_get() {
  $pids = array();

  exec('ps aux | grep php', $output);
  foreach ($output as $line) {
    if (preg_match('/^.*? (\d+ ).*?(php \\.\\/scripts\\/run-tests\\.sh|curl.*?pifr\\/client\\/review)/m', $line, $match)) {
      // Process running: "php ./scripts/run-tests.sh" or "curl http://example.com/pifr/client/review".
      $pids[] = $match[1];
    }
  }

  return $pids;
}

/**
 * Kill the specified list of processes.
 *
 * @param array $pids List of process IDs (pids).
 */
function pifr_client_cron_test_processes_kill($pids) {
  foreach ($pids as $pid) {
    exec('kill ' . $pid);
  }
}

/**
 * Execute a system command in the background.
 *
 * @return integer proccess ID.
 */
function pifr_client_cron_exec_background($command) {
  return shell_exec("nohup $command > /dev/null 2> /dev/null & echo $!");
}
