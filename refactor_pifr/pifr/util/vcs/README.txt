
The scripts contained in this directory are intended to be used with their
repsective version control system to provide an on-commit review of the changed
files. To use the scripts locate the script prefixed with the version control
system of your choice, remove the prefix and install in the manor specific to
the version control system. The scripts will contain a reference to the queue
script they are to invoke which will need to be updated to suite your setup.
