<?php

/**
 * @file
 * Queue pre-defined tests based on GET arguments.
 *
 * The identification IDs can be configured using the constants in case
 * multiple queue scripts are needed for various projects. The batch format is
 * designed to work with an environment setup like the following:
 *
 * SimpleTest
 * ----------
 * Conditions
 *   - branches: 1, 2 (thus ignoring branch test 3 [file list])
 * Review
 *   - plugin: pifr_simpletest (SimpleTest)
 *   - arguments:
 *     - test.directory.review[]:sites/all/modules (location of custom code)
 *     - drupal.core.version:7 (or relevant core version)
 *     - drupal.core.url:bzr://bzr.example.com (only use if repository includes Drupal core)
 *     - simpletest.db:http://dev.example.com/dev-db.sql (db dump used with DrupalCloneTestCase)
 *     - drupal.user:admin (user #1 name found in db dump)
 *     - drupal.pass:admin (user #1 password found in db dump)
 *
 * Code Review
 * -----------
 * Review
 *   - plugin: pifr_coder (Coder)
 *   - arguments:
 *     - test.directory.review[]:sites/all/modules (location of custom code)
 *     - drupal.core.version:7 (or relevant core version)
 *     - drupal.core.url:bzr://bzr.example.com (only use if repository includes Drupal core)
 *     - coder.annotate:1 (enables blame feature)
 */

define('CLIENT_ID_PROJECT', 1);

define('PROJECT_ID', 1);

define('TEST_ID_NIGHTLY', 1);
define('TEST_ID_CORE', 2);
define('TEST_ID_FILE_LIST', 3);

// Load Drupal core.
require_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

// Process GET arguments.
$batch = generate_batch();
$response = queue($batch);
print_tests($response);

/**
 * Queue batch for review.
 *
 * @param $batch
 *   PIFR batch information.
 * @return
 *   XML-RPC response received from testing master.
 */
function queue(array $batch) {
  module_load_include('xmlrpc.inc', 'pifr_server');
  $client = pifr_server_client_get(CLIENT_ID_PROJECT);
  return pifr_server_xmlrpc_queue($client['client_key'], $batch);
}

/**
 * Print out links to each of the queued tests.
 */
function print_tests(array $response) {
  echo 'Queued tests:<br />';
  $tests = array();
  foreach (array_merge($response['branches'], $response['files']) as $test) {
    $tests[] = l('#' . $test, 'pifr/test/' . $test);
  }

  if ($tests) {
    echo theme('item_list', $tests);
  }
  else {
    echo 'No test(s) queued. The tests are most likely already queued.';
  }
}

/**
 * Generate batch information based on GET arguments.
 *
 * @return
 *   PIFR batch information.
 */
function generate_batch() {
  $batch = array(
    'branches' => array(),
    'files' => array(),
    'projects' => array(),
  );

  $batch['projects'][] = array(
    'client_identifier' => PROJECT_ID,
    'name' => 'Example Project',
    'repository_type' => 'bzr',
    'repository_url' => 'bzr://bzr.example.com',
    'link' => 'http://code.example.com',
  );

  if (!empty($_GET['file_list'])) {
    $files = explode(';', $_GET['file_list']);
    foreach ($files as &$file) {
      $file = trim($file);
    }

    // Queue test against list of files.
    $batch['branches'][] = array(
      'project_identifier' => PROJECT_ID,
      'client_identifier' => TEST_ID_FILE_LIST,
      'vcs_identifier' => 'example-branch',
      'plugin_argument' => array(
        'test.title' => 'Example Project - r' . $_GET['revno'] . ' by ' . $_GET['user'],
        'test.files' => $files,
      ),
      'test' => TRUE,
      'link' => 'http://code.example.com/example-branch',
    );
  }
  elseif (!empty($_GET['core'])) {
    // Queue full core review.
    $batch['branches'][] = array(
      'project_identifier' => PROJECT_ID,
      'client_identifier' => TEST_ID_CORE,
      'vcs_identifier' => 'example-branch',
      'plugin_argument' => array(
        'test.title' => 'Example Project - core',
        'test.directory.review' => array(''), // Override environment default.
      ),
      'test' => TRUE,
      'link' => 'http://code.example.com/example-branch',
    );
  }
  else {
    // Queue standard nightly test against selective code.
    $batch['branches'][] = array(
      'project_identifier' => PROJECT_ID,
      'client_identifier' => TEST_ID_NIGHTLY,
      'vcs_identifier' => 'example-branch',
      'plugin_argument' => array(
        'test.title' => 'Example Project - nightly',
      ),
      'test' => TRUE,
      'link' => 'http://code.example.com/example-branch',
    );
  }
  return $batch;
}
