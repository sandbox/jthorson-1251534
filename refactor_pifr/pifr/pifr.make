; $Id$
core = 6.x
projects[] = drupal

; Dependencies.
projects[] = views
projects[] = tabs

projects[chart][type] = module
projects[chart][download][type] = cvs
projects[chart][download][module] = contributions/modules/chart
projects[chart][download][revision] = HEAD
