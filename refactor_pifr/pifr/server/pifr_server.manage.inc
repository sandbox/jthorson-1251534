<?php
/**
 * @file
 * Provide client management functionality.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Display pifr client information on user view.
 *
 * @param object $account User object.
 */
function pifr_server_manage_user_view($account) {
  $account->content['pifr_clients'] = array(
    '#value' => '<h3>' . t('PIFR Clients') . '</h3>' . views_embed_view('pifr_server_client', 'default', $account->uid),
    '#weight' => 10
  );
}

/**
 * Display summary of PIFR client related information.
 *
 * @param object $account User object.
 * @return string HTML output.
 */
function pifr_server_manage_dashboard($account) {
  $out = '<h3>' . t('Clients') . '</h3>';
  $out .= views_embed_view('pifr_server_client', 'attachment_1', $account->uid);

  $out .= '<h3>' . t('Projects') . '</h3>';
  $out .= views_embed_view('pifr_server_project', 'attachment_1', $account->uid);

  $out .= '<h3>' . t('Branches') . '</h3>';
  $out .= views_embed_view('pifr_server_branch', 'attachment_1', $account->uid);

  return $out;
}

/**
 * Get an array containing client operation links.
 *
 * @param array $client Client.
 * @param object $account User object.
 * @return Array of links.
 */
function pifr_server_manage_client_list_operations(array $client, $account = NULL) {
  $links = array();
  $base = pifr_server_manage_base_url($account);
  $links['client_result'] = array(
    'title' => t('view test'),
    'href' => "pifr/test/{$client['test_id']}"
  );
  if (pifr_server_manage_access($client['client_id'], $account)) {
    if ($client['status'] == PIFR_SERVER_CLIENT_STATUS_ENABLED) {
      $links['client_disable'] = array(
        'title' => t('disable'),
        'href' => "$base/disable/{$client['client_id']}"
      );
    }
    elseif ($client['status'] == PIFR_SERVER_CLIENT_STATUS_DISABLED || $client['status'] == PIFR_SERVER_CLIENT_STATUS_FAIL) {
      $links['client_enable'] = array(
        'title' => t('request enable'),
        'href' => "$base/enable/{$client['client_id']}"
      );
    }
  }
  $links['client_edit'] = array(
    'title' => t('edit'),
    'href' => "$base/edit/{$client['client_id']}"
  );
  $links['client_remove'] = array(
    'title' => t('remove'),
    'href' => "$base/remove/{$client['client_id']}"
  );
  return $links;
}

/**
 * Render a set of operations as links.
 *
 * @param array $operations Operations to render.
 * @return string HTML output.
 */
function pifr_server_manage_render_operations(array $operations) {
  $out = '';
  foreach ($operations as $operation) {
    $out .= l($operation['title'], $operation['href']) . ' | ';
  }
  return substr($out, 0, strlen($out) - 3); // Remove last task pipe.
}

/**
 * Client information form.
 *
 * @param interger $client_id Client ID.
 * @param object $account User object.
 */
function pifr_server_manage_client_form(&$form_state, $client_id = NULL, $account = NULL) {
  drupal_add_js(drupal_get_path('module', 'pifr_server') . '/pifr_server.js');

  $form = array();

  // If a $uid is specified then form is not being viewed from administration page.
  $form['#redirect'] = pifr_server_manage_base_url($account);

  // If $client_id specified then load client information for editting.
  if ($client_id) {
    $client = pifr_server_client_get($client_id);
  }

  $form['client'] = array(
    '#type' => 'fieldset',
    '#title' => t('Client'),
  );
  $form['client']['client_id'] = array(
    '#type' => 'value',
    '#value' => $client_id,
  );
  $form['client']['uid'] = array(
    '#type' => 'value',
    '#value' => isset($client) ? $client['uid'] : $account->uid,
  );
  $form['client']['status'] = array(
    '#type' => 'value',
    '#value' => isset($client) ? $client['status'] : PIFR_SERVER_CLIENT_STATUS_DISABLED,
  );
  $form['client']['test_id'] = array(
    '#type' => 'value',
    '#value' => isset($client) ? $client['test_id'] : 0,
  );

  // Only allow editting of client key if user has permission, otherwise display current key or generate one.
  if ($client_id && user_access('pifr manage')) {
    $form['client']['client_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Client key'),
      '#default_value' => (isset($client) ? $client['client_key'] : ''),
      '#required' => TRUE
    );
  }
  else {
    // Display client key, either generated or from db.
    $key = (isset($form_state['values']['client_key']) ? $form_state['values']['client_key'] : md5(uniqid()));
    $key = (isset($client) ? $client['client_key'] : $key);
    $form['client']['client_key'] = array(
      '#type' => 'hidden',
      '#default_value' => $key
    );
    $form['client']['client_key_display'] = array(
      '#type' => 'item',
      '#title' => t('Client key'),
      '#value' => $key
    );
  }

  $form['client']['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#description' => t('The base URL of the client, include ending forward slash.'),
    '#default_value' => (isset($client) ? $client['url'] : ''),
    '#required' => TRUE
  );

  $form['client']['type'] = array(
    '#type' => 'radios',
    '#title' => t('Type'),
    '#description' => t('Project clients will send test requests for uploaded patches, and test ' .
                        'clients request patches from the queue, perform review, and report results.'),
    '#options' => array(
      PIFR_SERVER_CLIENT_TYPE_PROJECT,
      PIFR_SERVER_CLIENT_TYPE_TEST
    ),
    '#default_value' => (isset($client) ? $client['type'] : PIFR_SERVER_CLIENT_TYPE_TEST),
    '#required' => TRUE
  );

  // Associate constant values with textual representation and remove any types that are not avialable to user.
  $form['client']['type']['#options'] = drupal_map_assoc($form['client']['type']['#options'], 'pifr_server_client_type');
  if (!user_access('add pifr project client')) {
    unset($form['client']['type']['#options'][PIFR_SERVER_CLIENT_TYPE_PROJECT]);
  }
  if (!user_access('add pifr test client')) {
    unset($form['client']['type']['#options'][PIFR_SERVER_CLIENT_TYPE_TEST]);
  }

  // If only one option available, default to it.
  if (count($form['client']['type']['#options']) == 1) {
    $form['client']['type']['#default_value'] = key($form['client']['type']['#options']);
  }

  // No need to display list of environments if user cannot create a test client.
  $form['client']['environment'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Environment'),
    '#description' => t('The environment(s) the client will be testing.'),
    '#options' => array(),
    '#default_value' => (isset($client) ? $client['environment'] : array()),
    '#access' => user_access('add pifr test client'),
  );
  $environments = pifr_server_environment_get_all();
  foreach ($environments as $environment) {
    $form['client']['environment']['#options'][$environment['environment_id']] = $environment['title'];
  }

  $form['client']['public'] = array(
    '#type' => 'checkbox',
    '#title' => t('Public'),
    '#description' => t('Allow anonymous access to all tests from this client.'),
    '#default_value' => isset($client) ? $client['public'] : FALSE,
  );

  if (user_access('manage pifr clients')) {
    $owner = user_load($form['client']['uid']['#value']);
    $form['client']['owner'] = array(
      '#type' => 'textfield',
      '#title' => t('Owner'),
      '#autocomplete_path' => 'user/autocomplete',
      '#default_value' => $owner->name,
    );
  }

  $form['op'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  $form['cancel'] = array(
    '#type' => 'markup',
    '#value' => l(t('Cancel'), 'user/' . $GLOBALS['user']->uid . '/pifr'),
  );

  return $form;
}

/**
 * Validate client information.
 */
function pifr_server_manage_client_form_validate($form, &$form_state) {
  if (drupal_strlen($form_state['values']['client_key']) != 32) {
    form_set_error('client_key', t('Client key must be 32 characters long.'));
  }
  if (!$form_state['values']['url'] || !@parse_url($form_state['values']['url'])) {
    form_set_error('url', t('Client URL must be a complete URL, schema included.'));
  }
  else if (drupal_substr($form_state['values']['url'], drupal_strlen($form_state['values']['url']) - 1) != '/') {
    form_set_error('url', t('Client URL must end with a slash.'));
  }

  if ($form_state['values']['type'] == PIFR_SERVER_CLIENT_TYPE_TEST) {
    $found = FALSE;
    foreach ($form_state['values']['environment'] as $environment => $enabled) {
      if ($enabled) {
        $found = TRUE;
        break;
      }
    }

    if (!$found) {
      form_set_error('environment', t('At least one environment must be selected.'));
    }
  }

  if (!empty($form_state['values']['owner']) && !user_load(array('name' => $form_state['values']['owner']))) {
    form_set_error('owner', t('Invalid username.'));
  }
}


/**
 * Save client information.
 */
function pifr_server_manage_client_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['owner'])) {
    $owner = user_load(array('name' => $form_state['values']['owner']));
    $form_state['values']['uid'] = $owner->uid;
  }

  pifr_server_client_save($form_state['values']);
  drupal_set_message(t('Changes saved successfully.'));
}

/**
 * Confirm request for client to be enabled.
 */
function pifr_server_client_enable_confirm_form(&$form_state, $client_id, $account = NULL) {
  $form = array();
  $form['#redirect'] = pifr_server_manage_base_url($account);
  $form['client_id'] = array(
    '#type' => 'value',
    '#value' => $client_id
  );

  return confirm_form($form,
                       t('Initiate the client test process for client #@client_id?', array('@client_id' => $client_id)),
                       $form['#redirect'],
                       t('The results of the testing will determine if the client is enabled.'),
                       t('Start testing'));
}

/**
 * Init client testing process.
 */
function pifr_server_client_enable_confirm_form_submit($form, &$form_state) {
  $client = pifr_server_client_get($form_state['values']['client_id']);
  pifr_server_client_confirmation_init($client);

  if ($client['type'] == PIFR_SERVER_CLIENT_TYPE_PROJECT) {
    drupal_set_message(t('Enabled client @client_id.', array('@client_id' => $form_state['values']['client_id'])));
  }
  else {
    drupal_set_message(t('Initiated testing of client @client_id.', array('@client_id' => $client['client_id'])));
  }
}

/**
 * Confirm disabling of client.
 */
function pifr_server_client_disable_confirm_form(&$form_state, $client_id, $account = NULL) {
  $form = array();
  $form['#redirect'] = pifr_server_manage_base_url($account);
  $form['client_id'] = array(
    '#type' => 'value',
    '#value' => $client_id
  );

  return confirm_form($form,
                       t('Disable client #@client_id?', array('@client_id' => $client_id)),
                       $form['#redirect'],
                       t('Once disabled the client will no longer be allowed to test files.'),
                       t('Disable'));
}

/**
 * Disable client.
 */
function pifr_server_client_disable_confirm_form_submit($form, &$form_state) {
  // Set client status to disabled.
  $client = pifr_server_client_get($form_state['values']['client_id']);
  $client['status'] = PIFR_SERVER_CLIENT_STATUS_DISABLED;
  pifr_server_client_save($client);

  drupal_set_message(t('Disabled client @client_id.',
                       array('@client_id' => $form_state['values']['client_id'])));
}

/**
 * Confirm removal of client.
 */
function pifr_server_client_remove_confirm_form(&$form_state, $client_id, $account = NULL) {
  $form = array();
  $form['#redirect'] = pifr_server_manage_base_url($account);
  $form['client_id'] = array(
    '#type' => 'value',
    '#value' => $client_id
  );

  return confirm_form($form,
    t('Are you sure you want to delete client #@client_id?', array('@client_id' => $client_id)),
    $form['#redirect'], t('All data related to this client will be removed without any way to restore it.'));
}

/**
 * Remove client.
 */
function pifr_server_client_remove_confirm_form_submit($form, &$form_state) {
  $client = pifr_server_client_get($form_state['values']['client_id']);
  pifr_server_client_delete($client);

  drupal_set_message(t('Client removed.'));
}

/**
 * Get the base URL for management operations, either user based or general.
 *
 * @param object $account User object.
 * @return string Base URL.
 */
function pifr_server_manage_base_url($account) {
  if ($account) {
    return "user/$account->uid/pifr";
  }
  else {
    return 'admin/pifr/server';
  }
}
