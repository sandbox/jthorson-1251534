<?php

/**
 * @file
 * Provide review handling functions.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

function pifr_server_review_get_all() {
  return module_invoke_all('pifr_info');
}

function pifr_server_review_argument_get(array $environment, array $dependency_argument = array(), array $branch = array()) {
  $plugin = pifr_server_review_plugin_load($environment['plugin']);

  if (!$branch) {
    $branch['plugin_argument'] = array();
  }
  return $plugin->argument($environment['plugin_argument'], $dependency_argument, $branch['plugin_argument']);
}

function pifr_server_review_confirmation_test(array $client, array $environment, $stage) {
  $plugin = pifr_server_review_plugin_load($environment['plugin']);

  return $plugin->confirmation_test_get($client, $environment, $stage);
}

function pifr_server_review_confirmation_test_check(array $client, array $result, array $status) {
  $plugin = pifr_server_review_plugin_load_environment($result['environment_id']);

  return $plugin->confirmation_check($client, $result, $status);
}

/**
 * Check a result.
 *
 * @param array $result Result information.
 * @return boolean TRUE if pass, otherwise FALSE.
 */
function pifr_server_review_check(array $result) {
  $plugin = pifr_server_review_plugin_load_environment($result['environment_id']);

  return $plugin->check($result);
}

/**
 * Check a result.
 *
 * @param integer $environment Environmnet from which code originated.
 * @param integer $code Result code to check.
 * @return boolean TRUE if pass, otherwise FALSE.
 */
function pifr_server_review_check_code($environment, $code) {
  $plugin = pifr_server_review_plugin_load_environment($environment['environment_id']);

  return $plugin->check(array('code' => $code));
}

/**
 * Generate summary message.
 *
 * @param array $result Result information.
 * @return string Translated summary message.
 */
function pifr_server_review_summary(array $result) {
  $plugin = pifr_server_review_plugin_load_environment($result['environment_id']);

  return $plugin->summary($result);
}


/**
 * Format summary details.
 *
 * @param array $result Result information.
 * @return array Formatted summary details.
 */
function pifr_server_review_summary_format(array $result) {
  $plugin = pifr_server_review_plugin_load_environment($result['environment_id']);

  return $plugin->summary_format($result['details']);
}

/**
 * Generate review details.
 *
 * @param array $test Test information.
 * @param array $result Test results.
 * @return string HTML output.
 */
function pifr_server_review_detail(array $test, array $result) {
  $plugin = pifr_server_review_plugin_load_environment($result['environment_id']);

  return $plugin->detail($test, $result);
}

/**
 * Generate review details summary.
 *
 * @param array $test Test information.
 * @param array $result Test results.
 * @return string HTML output.
 */
function pifr_server_review_detail_summary(array $test, array $result) {
  $plugin = pifr_server_review_plugin_load_environment($result['environment_id']);

  return $plugin->detail_summary($test, $result);
}

function pifr_server_review_code_title($environment_id, $code) {
  $plugin = pifr_server_review_plugin_load_environment($environment_id);

  return $plugin->code_title($code);
}

function pifr_server_review_load(array $result) {
  $plugin = pifr_server_review_plugin_load_environment($result['environment_id']);

  $result['data'] = $plugin->load($result['result_id']);
  return $result;
}

function pifr_server_review_record(array $result) {
  $plugin = pifr_server_review_plugin_load_environment($result['environment_id']);

  $plugin->record($result['result_id'], $result['data']);
}

function pifr_server_review_delete(array $result) {
  $plugin = pifr_server_review_plugin_load_environment($result['environment_id']);

  $plugin->delete($result['result_id'], $result['data']);
}

/**
 * Ensure that the plugin server include is loaded.
 *
 * @param string $plugin PIFR review plugin module name.
 */
function pifr_server_review_plugin_load($plugin) {
  static $loaded = array();

  if (!isset($loaded[$plugin])) {
    if (!module_exists($plugin)) {
      trigger_error('Plugin [' . $plugin . '] does not exist.', E_USER_ERROR);
    }

    require_once drupal_get_path('module', 'pifr') . '/review/server.inc';
    require_once drupal_get_path('module', $plugin) . '/' . $plugin . '.server.inc';

    $class = 'pifr_server_review_' . $plugin;
    if (class_exists($class) && in_array('pifr_server_review', class_parents($class))) {
      $loaded[$plugin] = new $class();
      $loaded[$plugin]->init();
    }
    else {
      trigger_error('Plugin [' . $plugin . '] must extend [pifr_server_review] ' .
        'with a class named [pifr_server_review_' . $plugin . '].', E_USER_ERROR);
    }
  }
  return $loaded[$plugin];
}

function pifr_server_review_plugin_load_environment($environment_id) {
  $environment = pifr_server_environment_get($environment_id);
  return pifr_server_review_plugin_load($environment['plugin']);
}
