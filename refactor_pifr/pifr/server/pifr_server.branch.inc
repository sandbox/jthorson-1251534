<?php

/**
 * @file
 * Provide branch functions.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Load a branch.
 *
 * @param integer $branch_id Branch ID.
 * @return array Branch information, or FALSE.
 */
function pifr_server_branch_get($branch_id) {
  $result = db_query('SELECT *
                      FROM {pifr_branch}
                      WHERE branch_id = %d', $branch_id);
  return pifr_server_branch_load(db_fetch_array($result));
}

/**
 * Load branch(es) assiciated with a project.
 *
 * @param integer $project_id Project ID.
 * @return array List of branches.
 */
function pifr_server_branch_get_all($project_id) {
  $result = db_query('SELECT *
                      FROM {pifr_branch}
                      WHERE project_id = %d', $project_id);

  $branches = array();
  while ($branch = db_fetch_array($result)) {
    $branches[] = pifr_server_branch_load($branch);
  }
  return $branches;
}

/**
 * Load branch assiciated with a test.
 *
 * @param integer $test_id Test ID.
 * @return array Branch information, or FALSE.
 */
function pifr_server_branch_get_test($test_id) {
  $result = db_query('SELECT *
                      FROM {pifr_branch}
                      WHERE test_id = %d', $test_id);
  return pifr_server_branch_load(db_fetch_array($result));
}

/**
 * Find a branch if it exists.
 *
 * @param array $branch Branch information.
 * @return array Complete branch information if found, or FALSE.
 */
function pifr_server_branch_find(array $branch) {
  $result = db_query("SELECT *
                      FROM {pifr_branch}
                      WHERE client_identifier = '%s'
                      AND project_id = %d", $branch['client_identifier'], $branch['project_id']);
  return pifr_server_branch_load(db_fetch_array($result));
}

/**
 * Load branch information by preforming necessary data cleanups.
 *
 * @param array $branch Branch information.
 * @return array Cleaned up branch information.
 */
function pifr_server_branch_load($branch) {
  if ($branch) {
    $branch['plugin_argument'] = unserialize($branch['plugin_argument']);
  }
  return $branch;
}

/**
 * Save branch information.
 *
 * @param array $branch Branch information.
 * @param boolean $check Check for existing branch and update if found.
 * @return array Updated branch information.
 */
function pifr_server_branch_save(array $branch, $check = TRUE) {
  if ($check && empty($branch['branch_id'])) {
    // Check if branch already exists.
    if ($branch_record = pifr_server_branch_find($branch)) {
      $branch['branch_id'] = $branch_record['branch_id'];
      $branch['test_id'] = $branch_record['test_id'];
    }
  }

  if (!empty($branch['branch_id'])) {
    // Existing project, update record.
    drupal_write_record('pifr_branch', $branch, 'branch_id');
    pifr_server_test_title_update($branch);
  }
  else {
    // Insert test record.
    $test = array(
      'type' => PIFR_SERVER_TEST_TYPE_BRANCH,
      'status' => PIFR_SERVER_TEST_STATUS_STUB,
      'title' => pifr_server_test_title(PIFR_SERVER_TEST_TYPE_BRANCH, $branch),
      'last_received' => time(),
      'last_requested' => 0,
      'last_tested' => 0,
      'test_count' => 0
    );
    $test = pifr_server_test_save($test);
    $branch['test_id'] = $test['test_id'];

    // New branch, insert record.
    drupal_write_record('pifr_branch', $branch);
  }
  return $branch;
}

/**
 * Delete a branch, its test, and files associated with it.
 *
 * @param array $branch Branch information.
 */
function pifr_server_branch_delete(array $branch) {
  // Delete test and results.
  pifr_server_test_delete($branch['test_id']);

  // Delete all files that are appart of branch.
  $files = pifr_server_file_get_all($branch['branch_id']);
  foreach ($files as $file) {
    pifr_server_file_delete($file);
  }

  // Delete branch record.
  db_query('DELETE FROM {pifr_branch} WHERE branch_id = %d', $branch['branch_id']);
}

class PIFRTestBranch extends PIFRTest {

  protected $branch;

  public function __construct(array $test) {
    parent::__construct($test);
    $this->branch = pifr_server_branch_get_test($test['test_id']);
  }

  public function getType() {
    return t('Branch');
  }

  public function getInfo() {
    return $this->branch;
  }

  public function getInfoTable() {
    $rows = array();

    $rows['identifier'] = array(t('Identifier'), check_plain($this->branch['vcs_identifier']));

    $project = pifr_server_project_get($this->branch['project_id']);
    $client = pifr_server_client_get($project['client_id']);
    $rows['project'] = array(t('Project'), $project['link'] ? l($project['name'], $project['link']) : $project['name']);

    $link = $this->branch['link'];
    $rows['link'] = array(t('Link'), $link ? l(t('Project information'), $link) : '<em>' . t('none') . '</em>');

    return $this->formatTable($rows);
  }
}
