<?php

/**
 * @file
 * Provide XML-RPC callbacks and invoke functions.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/*
 * Maximum number of items to send in a batch.
 */
define('PIFR_SERVER_BATCH_MAX_COUNT', 100);

/**
 * Queue a the batch of files for testing.
 *
 * @param string $client_key Client key requesting batch to be queued.
 * @param array $batch Array of file and project data used to queue files.
 * <code>
 *   $batch = array(
 *     'branches' => array( [referenced branches (may also be tested)] ),
 *     'files' => array( [files to be tested] ),
 *     'projects' => array( [referenced projects] ),
 *   );
 *
 *   $branch = array(
 *     'project_identifier', // Reference to project by client identifier.
 *     'client_identifier', // Identifier that will be used to reference the branch.
 *     'vcs_identifier', // Version control system identifier.
 *     'dependency', // (Optional) Comma separated list of branch identifiers that this branch depends on.
 *     'plugin_argument', // (Optional) Associative array of arguments keyed by plugin.
 *     'test', // (Optional) (Default: false) Request branch to be tested.
 *     'link', // (Optional) Web accessible link representing branch.
 *   );
 *
 *   $file = array(
 *     'branch_identifier', // Reference to branch by client identifier.
 *     'client_identifier', // Identifier that will be used to reference the file.
 *     'file_url', // Absolute URL to file.
 *     'link', // (Optional) Web accessible link representing file.
 *   );
 *
 *   $project = array(
 *     'client_identifier', // Identifier that will be used to reference the project.
 *     'name', // Name of project.
 *     'repository_type', // Repository type.
 *     'repository_url', // Repository URL, git://git.drupal.org/project/drupal.git.
 *     'link', // (Optional) Web accessible link representing project.
 *   );
 * </code>
 * @return array Assigned test IDs, or array with response code, PIFR_RESPONSE_*.
 * <code>
 *   $response = array(
 *     'branches' => array( [Test IDs of branches keyed by client_identifier] ),
 *     'files' => array( [Test IDs of files keyed by client_identifier] ),
 *   );
 * </code>
 *
 * OR
 *
 * <code>
 *   $test = array(
 *     'response', // Response code: PIFR_RESPONSE_*.
 *     'errors', // (Optional) List of errors related to response code.
 *   );
 * </code>
 */
function pifr_server_xmlrpc_queue($client_key, $batch) {

  // Check if valid project client.
  $client = pifr_server_client_check_key($client_key, PIFR_SERVER_CLIENT_TYPE_PROJECT);

  pifr_debug('pifr_server_xmlrpc_queue: received batch from client=<pre>%client</pre>. Batch=<pre>%batch</pre>', array('%client' => print_r($client, TRUE), '%batch' => print_r($batch, TRUE)));

  if ($client) {
    if (!PIFR_ACTIVE) {
      return array(
        'response' => PIFR_RESPONSE_DENIED,
        'errors' => array(t('Server is not active.')),
      );
    }

    // Validate and clean the incoming batch.
    if (!($batch = pifr_server_xmlrpc_validate('queue', $batch))) {
      watchdog('pifr_client', 'Failed validation of incoming batch');
      return array(
        'response' => PIFR_RESPONSE_DENIED,
        'errors' => pifr_server_xmlrpc_error(),
      );
    }

    // Store project data and retrieve completed project information.
    $projects = pifr_server_xmlrpc_project_save($batch['projects'], $client['client_id']);

    // Store branch data and retrieve completed branch information.
    $branches = pifr_server_xmlrpc_branch_save($batch['branches'], $projects, $client);

    // Store branch data and retrieve completed file information.
    $files = pifr_server_xmlrpc_file_save($batch['files'], $branches, $client);

    // Postpone tests if branch is currently failing.
    foreach ($branches as $branch) {
      if (!pifr_server_result_summary_status($branch['test_id'])) {
        // Postpone all tests that have been queued.
        pifr_server_file_branch_update($branch['branch_id'], PIFR_SERVER_TEST_STATUS_QUEUED, PIFR_SERVER_TEST_STATUS_POSTPONED);
      }
    }

    // Send back test IDs.
    $response = array(
      'branches' => array(),
      'files' => array(),
    );
    foreach (array_keys($response) as $key) {
      foreach ($$key as $item) {
        $response[$key][$item['client_identifier']] = (int) $item['test_id'];
      }
    }

    return $response;
  }
  else {
    return array(
      'response' => PIFR_RESPONSE_INVALID_SERVER,
      'errors' => array(t('Invalid project client, check key and ensure that client is enabled.')),
    );
  }
}

/**
 * Save specified projects.
 *
 * @param array $projects Projects to save.
 * @param integer $client_id Client ID that send projects.
 * @return array Saved projects.
 */
function pifr_server_xmlrpc_project_save(array $projects, $client_id) {
  $saved = array();
  foreach ($projects as $project) {
    // Add client ID to complete project information.
    $project['client_id'] = $client_id;

    // Save the project and add complete project information to saved array.
    $saved[$project['client_identifier']] = pifr_server_project_save($project);
  }
  return $saved;
}

/**
 * Save specified branches.
 *
 * @param array $branches Branches to save.
 * @param array $projects Projects that branches relate to.
 * @param array $client Client information.
 * @return array Saved branches.
 */
function pifr_server_xmlrpc_branch_save(array $branches, array $projects, array $client) {
  $saved = array();
  foreach ($branches as $branch) {
    if (!isset($projects[$branch['project_identifier']])) {
      pifr_debug('XML-RPC batch(): missing project data for branch: (b: @identifier; p: @project_identifier).',
                   array('@identifier' => $branch['client_identifier'], '@project_identifier' => $branch['client_identifier']));
      continue;
    }

    // Get project ID from saved project information.
    $branch['project_id'] = $projects[$branch['project_identifier']]['project_id'];

    // Save the branch and add complete branch information to saved array.
    $saved[$branch['client_identifier']] = pifr_server_branch_save($branch);

    // If branch is to be tested then update test record.
    if ($branch['test']) {
      $test = pifr_server_test_get($saved[$branch['client_identifier']]['test_id']);
      pifr_server_xmlrpc_test_queue($test, $client);
    }
  }

  // Once all branches have been saved the branch dependencies can be safely
  // converted to branch ID references.
  foreach ($saved as $branch) {
    if (!empty($branch['dependency'])) {
      $dependencies = explode(',', $branch['dependency']);
      $dependency_ids = array();
      foreach ($dependencies as $dependency) {
        if (!empty($saved[$dependency])) {
          $dependency_ids[] = $saved[$dependency]['branch_id'];
        }
      }

      $branch['dependency'] = implode(',', $dependency_ids);
      pifr_server_branch_save($branch);
    }
  }

  return $saved;
}

/**
 * Save specified files.
 *
 * @param array $files Files to save.
 * @param array $branches Branches that files relate to.
 * @param array $client Client information.
 * @return array Saved files.
 */
function pifr_server_xmlrpc_file_save(array $files, array $branches, array $client) {
  $saved = array();
  pifr_debug("Received %count files", array('%count' => count($files)));
  foreach ($files as $file) {
    pifr_debug("pifr_server_xmlrpc_file_save: Received file <pre>%file</pre>", array('%file' => print_r($file, TRUE)));
    if (!isset($branches[$file['branch_identifier']])) {
      pifr_debug('XML-RPC batch(): missing branch data for file: (u: @file_url; n: @issue_nid).',
                   array('@file_url' => $file['file_url'], '@issue_nid' => !empty($file['issue_nid']) ? $file['issue_nid'] : 'not provided'));
      continue;
    }

    // Get branch ID from saved branch information.
    $file['branch_id'] = $branches[$file['branch_identifier']]['branch_id'];

    // Generate file name from file URL.
    $file['file_name'] = basename($file['file_url']);

    // Save the file and add complete file information to saved array.
    $saved[$file['client_identifier']] = pifr_server_file_save($file);

    // Update test record.
    $test = pifr_server_test_get($saved[$file['client_identifier']]['test_id']);
    pifr_server_xmlrpc_test_queue($test, $client);
  }
  return $saved;
}

/**
 * Queue test for testing.
 *
 * If test is already queued or currently being tested the queue request will
 * be ignored and an appropriate log message will be made.
 *
 * @param array $test Test information.
 * @param array $client Client information.
 */
function pifr_server_xmlrpc_test_queue(array $test, array $client) {
  if ($test['status'] == PIFR_SERVER_TEST_STATUS_QUEUED) {
    pifr_server_log($test['test_id'], $client['client_id'], PIFR_SERVER_LOG_TEST_REQUEST_IGNORE);
  }
  else {
    pifr_server_test_queue($test);
    pifr_server_log($test['test_id'], $client['client_id'], PIFR_SERVER_LOG_TEST_REQUEST);
  }
}

/**
 * Request the next file for testing. (Called by PIFR client)
 *
 * @param string $client_key Client key requesting next test.
 * @return array Test information, empty array, or array with response code, PIFR_RESPONSE_*.
 * <code>
 *   $test = array(
 *     'test_id', // Unique test ID.
 *     'review' => array(
 *       'plugin', // Module name of the review plugin to use.
 *       'argument', // Associative array of arguments for the review backend.
 *     ),
 *     'vcs' => array(
 *       'main' => array(
 *         'repository' => array(
 *            'type', // Textual representation of repository type.
 *            'url', // CVS will only require URL.
 *         ),
 *         'vcs_identifier', // Version control system identifier.
 *       ),
 *     ),
 *     'dependencies' => array(), // Dependencies following the same format as 'main'.
 *     'files' => array(), // List of absolute URL(s) to file(s) necessary for the review.
 *   );
 * </code>
 *
 * OR
 *
 * <code>
 *   $test = array(
 *     'response', // Response code: PIFR_RESPONSE_*.
 *     'errors', // (Optional) List of errors related to response code.
 *   );
 * </code>
 */
function pifr_server_xmlrpc_next($client_key) {
  // Check if valid test client.
  $client = pifr_server_client_check_key($client_key, PIFR_SERVER_CLIENT_TYPE_TEST);

  if ($client) {
    if (!PIFR_ACTIVE) {
      return array(
        'response' => PIFR_RESPONSE_DENIED,
        'errors' => array(t('Server is not active.')),
      );
    }

    return pifr_server_test_get_next($client);
  }
  else {
    return array(
      'response' => PIFR_RESPONSE_INVALID_SERVER,
      'errors' => array(t('Invalid testing client, check key and ensure that client is enabled.')),
    );
  }
  return array(
    'response' => PIFR_RESPONSE_ACCEPTED,
  );
}

/**
 * Report test results. (Called by PIFR client)
 *
 * @param string $client_key Client key sending test results.
 * @param array $result Test result.
 * <code>
 *   $result = array(
 *     'test_id', // Unique test ID.
 *     'code', // Summary result code as defined by the plugin steps.
 *     'details', // Array of details (parameters for t()).
 *     'pass', // Total number of pass assertions.
 *     'fail', // Total number of fail assertions.
 *     'exception', // Total number of exception assertions.
 *     'result_details' => array( [summary of test and non-pass assertions] ),
 *   );
 *
 *   $result_detail = array(
 *     'test_name', // Name of test.
 *     'pass', // Total number of pass assertions.
 *     'fail', // Total number of fail assertions.
 *     'exception', // Total number of exception assertions.
 *     'assertions' => array( [non-pass assertions] ),
 *   );
 *
 *   $assertion = array(
 *     'status', // Assertion status: 'fail' or 'exception'.
 *     'message', // Descriptive assertion message.
 *     'message_group', // Message group.
 *     'function', // Function in which the assertion was triggered.
 *     'line', // Line at which the assertion was triggered.
 *     'file', // File in which the assertion was triggered.
 *   );
 * </code>
 * @return Response code: PIFR_RESPONSE_*.
 */
function pifr_server_xmlrpc_result($client_key, array $result) {
  pifr_debug('Received result from testbot client: <pre>%result</pre>', array('%result' => print_r($result, TRUE)));
  // Check if valid test client.
  $client = pifr_server_client_check_key($client_key, PIFR_SERVER_CLIENT_TYPE_TEST);

  if ($client) {
    // Ensure that test was sent to the client returning the result.
    if ($test = pifr_server_test_get($result['test_id'])) {
      if (pifr_server_environment_status_get_client($test, $client)) {
        pifr_server_test_result($test, $result, $client);
      }
      else {
        watchdog('pifr_server', 'Test not assigned to client: (t: @test_id, c: @client_id)',
          array('@test_id' => $test['test_id'], '@client_id' => $client['client_id']), WATCHDOG_ALERT);
        return PIFR_RESPONSE_DENIED;
      }
    }
    else {
      watchdog('pifr_server', 'Invalid test ID: (t: @test_id, c: @client_id)',
        array('@test_id' => $test['test_id'], '@client_id' => $client['client_id']), WATCHDOG_ALERT);
      return PIFR_RESPONSE_DENIED;
    }
  }
  else {
    return PIFR_RESPONSE_INVALID_SERVER;
  }
  return PIFR_RESPONSE_ACCEPTED;
}

/**
 * Retrieve test results. (Called by PIFT)
 *
 * @param string $client_key Client key requesting batch to be queued.
 * @param integer $since GMT Unix timestamp to query test results after.
 * @return array Test results, empty array, or array with response code, PIFR_RESPONSE_*.
 * <code>
 *   $batch = array(
 *     'results' => array( [results of tests] ),
 *     'next', // Boolean flag signifying that more results are available.
 *     'last', // GMT timestamp when the last result was tested. For use in
 *             // subsiquent queries as the $since argument.
 *   );
 *
 *   $result = array(
 *     'test_id', // Unique ID representing the test.
 *     'pass', // TRUE or FALSE.
 *     'message', // Message describing results of test.
 *   );
 * </code>
 *
 * OR
 *
 * <code>
 *   $test = array(
 *     'response', // Response code: PIFR_RESPONSE_*.
 *     'errors', // (Optional) List of errors related to response code.
 *   );
 * </code>
 */
function pifr_server_xmlrpc_retrieve($client_key, $since) {
  // Check if valid test client.
  $client = pifr_server_client_check_key($client_key, PIFR_SERVER_CLIENT_TYPE_PROJECT);

  if ($client) {
    if (!PIFR_SERVER_REPORT) {
      return array(
        'response' => PIFR_RESPONSE_DENIED,
        'errors' => array(t('Reporting is not enabled.')),
      );
    }

    $batch = array(
      'results' => array(),
      'next' => FALSE,
      'last' => $since,
    );

    $since = pifr_server_xmlrpc_time_local($since);
    $tests = pifr_server_test_get_since($since, PIFR_SERVER_BATCH_MAX_COUNT + 1);
    foreach ($tests as $test) {
      $batch['results'][] = array(
        'test_id' => $test['test_id'],
        'pass' => pifr_server_result_summary_status($test['test_id']),
        'message' => pifr_server_result_summary_message($test['test_id']),
      );
      pifr_server_log($test['test_id'], $client['client_id'], PIFR_SERVER_LOG_CLIENT_RETRIEVE);
    }

    // If there are more then the maximum number of results to be sent in one
    // batch then remove the extra one and set the 'next' flag to TRUE.
    if (isset($batch['results'][PIFR_SERVER_BATCH_MAX_COUNT])) {
      unset($batch['results'][PIFR_SERVER_BATCH_MAX_COUNT]);
      $batch['next'] = TRUE;
    }

    if ($tests) {
      // Get the last result and send back its 'last_tested' timestamp.
      $batch['last'] = $tests[count($batch['results']) - 1]['last_tested'];
    }

    return $batch;
  }
  else {
    return array(
      'response' => PIFR_RESPONSE_INVALID_SERVER,
      'errors' => array(t('Invalid project client, check key and ensure that client is enabled.')),
    );
  }
  return array(
    'response' => PIFR_RESPONSE_ACCEPTED,
  );
}

/**
 * Convert a GMT Unix timestamp to a local Unix timestamp.
 *
 * @param integer $gmt_timestamp GMT Unix timestamp.
 * @return integer Local timestamp.
 */
function pifr_server_xmlrpc_time_local($gmt_timestamp) {
  $gmt_now = gmmktime();
  $offset = $gmt_now - $gmt_timestamp;
  return time() - $offset;
}

/**
 * Convert a local Unix timestamp to a GMT Unix timestamp.
 *
 * @param integer $timestamp Local Unix timestamp.
 * @return integer GMT Unix timestamp
 */
function pifr_server_xmlrpc_time_gmt($timestamp) {
  $now = time();
  $offset = $now - $timestamp;
  return gmmktime() - $offset;
}

/**
 * Validate the arguments for a given XML-RPC method.
 *
 * @param string $method XML-RPC method without the 'pifr.' prefix.
 * @param ... Additional arguments to validate.
 * @return mixed Validated and cleaned data.
 */
function pifr_server_xmlrpc_validate($method) {
  // Get all the arguments after the first argument ($method).
  $args = func_get_args();
  array_shift($args);

  // Call the related validation function and return the cleaned data.
  $return = call_user_func_array('pifr_server_xmlrpc_validate_' . $method, $args);

  // If an error occurred return FALSE, execution should stop and the errors
  // should be returned, as reported by pifr_server_xmlrpc_error().
  $errors = pifr_server_xmlrpc_error();
  if (!empty($errors)) {
    watchdog('pifr_client', 'Failed validation of xmlrpc batch. Errors=<pre>%errors</pre>', array('%errors' => print_r($errors, TRUE)));
    return FALSE;
  }
  return $return;
}

/**
 * Validate pifr.queue() batch.
 *
 * @param array $batch Queue batch to validate.
 * @return array Cleaned data.
 */
function pifr_server_xmlrpc_validate_queue(array $batch) {
  // Ensure every base key is present.
  $default = array(
    'branches' => array(),
    'files' => array(),
    'projects' => array(),
  );
  $batch += $default;

  if (count(array_keys($batch)) > 3) {
    pifr_server_xmlrpc_error(t('Only keys [branches, files, projects] are allowed.'));
  }

  // Get a list of supported vcs backends.
  $includes = file_scan_directory(drupal_get_path('module', 'pifr_client') . '/review/vcs', '.*');
  $backends = array();
  foreach ($includes as $include) {
    $backend = str_replace('.inc', '', $include->basename);
    if ($backend != 'vcs') {
      $backends[] = $backend;
    }
  }

  // Validate each project.
  $required_keys = array('client_identifier', 'name', 'repository_type', 'repository_url');
  $default = array(
    'link' => '',
  );
  $i = 0;
  $project_identifiers = array();
  foreach ($batch['projects'] as &$project) {
    // Provide default values for optional keys.
    $project += $default;
    $i++;

    // Ensure that all required keys are found and that all keys are valid.
    if ($errors = pifr_server_xmlrpc_required_check($project, $required_keys)) {
      pifr_server_xmlrpc_error_list('project', $i, $errors);
      continue;
    }
    elseif (count(array_keys($project)) > 5) {
      pifr_server_xmlrpc_error_list('project', $i, t('Only keys [client_identifier, name, ' .
        'repository_type, repository_url, link] are allowed'));
      continue;
    }

    if (!pifr_server_xmlrpc_is_int($project['client_identifier'])) {
      $errors[] = t('[client_identifier] must be an integer');
    }
    elseif (in_array($project['client_identifier'], $project_identifiers)) {
      $errors[] = t('[client_identifier] must be unique');
    }
    if (!in_array($project['repository_type'], $backends)) {
      $errors[] = t('[repository_type] must be one of [@list]', array('@list' => implode(', ', $backends)));
    }

    // Keep a list of project identifiers for validation.
    $project_identifiers[] = $project['client_identifier'];

    // If any errors where found then register an error for the file.
    pifr_server_xmlrpc_error_list('project', $i, $errors);
  }

  // Keep a list of branch identifiers for dependency validation.
  $branch_identifiers = array();
  foreach ($batch['branches'] as &$branch) {
    $branch_identifiers[] = $branch['client_identifier'];
  }

  // Validate each branch.
  $required_keys = array('project_identifier', 'client_identifier', 'vcs_identifier');
  $default = array(
    'dependency' => '',
    'plugin_argument' => array(),
    'test' => FALSE,
    'link' => '',
  );
  $unique = array();
  $i = 0;
  foreach ($batch['branches'] as &$branch) {
    // Provide default values for optional keys.
    $branch += $default;
    $i++;

    // Ensure that all required keys are found and that all keys are valid.
    if ($errors = pifr_server_xmlrpc_required_check($branch, $required_keys)) {
      pifr_server_xmlrpc_error_list('branch', $i, $errors);
      continue;
    }
    elseif (count(array_keys($branch)) > 7) {
      pifr_server_xmlrpc_error_list('branch', $i, t('Only keys [project_identifier, client_identifier, ' .
        'vcs_identifier, dependency, plugin_argument, test, link] are allowed'));
      continue;
    }

    // Validate fields.
    if (!pifr_server_xmlrpc_is_int($branch['project_identifier'])) {
      $errors[] = t('[project_identifier] must be an integer');
    }
    if (!pifr_server_xmlrpc_is_int($branch['client_identifier'])) {
      $errors[] = t('[client_identifier] must be an integer');
    }
    elseif (in_array($branch['client_identifier'], $unique)) {
      $errors[] = t('[client_identifier] must be unique');
    }
    if (!empty($branch['dependency'])) {
      $dependencies = explode(',', $branch['dependency']);
      foreach ($dependencies as $dependency) {
        if (empty($dependency)) {
          continue;
        }
        if (!pifr_server_xmlrpc_is_int($dependency)) {
          $errors[] = t('all dependencies must be integers; received "%dependency" instead', array('%dependency' => $dependency));
          break;
        }
        elseif (!in_array($dependency, $branch_identifiers)) {
          $errors[] = t('invalid dependency [' . $dependency . ']');
          break;
        }
      }
    }
    if (!is_array($branch['plugin_argument'])) { // TODO Validate key/value pairs.
      $errors[] = t('[plugin_argument] must be an array');
    }

    // Add the client identifiers during loop for unique validation.
    $unique[] = $branch['client_identifier'];

    // If any errors where found then register an error for the branch.
    pifr_server_xmlrpc_error_list('branch', $i, $errors);
  }

  // Validate each file.
  $required_keys = array('branch_identifier', 'client_identifier', 'file_url');
  $default = array(
    'link' => '',
  );
  $i = 0;
  foreach ($batch['files'] as &$file) {
    // Provide default values for optional keys.
    $file += $default;
    $i++;

    // Ensure that all required keys are found and that all keys are valid.
    if ($errors = pifr_server_xmlrpc_required_check($file, $required_keys)) {
      pifr_server_xmlrpc_error_list('file', $i, $errors);
      continue;
    }
    elseif (count(array_keys($file)) > 4) {
      pifr_server_xmlrpc_error_list('file', $i, t('Only keys [branch_identifier, client_identifier, file_url, link] are allowed'));
      continue;
    }

    if (!pifr_server_xmlrpc_is_int($file['branch_identifier'])) {
      $errors[] = t('[branch_identifier] must be an integer');
    }
    elseif (!in_array($file['branch_identifier'], $branch_identifiers)) {
      $errors[] = t('[branch_identifier] not found in branch list');
    }
    if (!pifr_server_xmlrpc_is_int($file['client_identifier'])) {
      $errors[] = t('[client_identifier] must be an integer');
    }

    // If any errors where found then register an error for the file.
    pifr_server_xmlrpc_error_list('file', $i, $errors);
  }

  return $batch;
}

/**
 * Store and retrieve XML-RPC errors.
 *
 * @param string $error (Optional) XML-RPC error, or NULL to simply retrieve.
 * @return array List of errors.
 */
function pifr_server_xmlrpc_error($error = NULL) {
  static $errors = array();

  if ($error) {
    $errors[] = $error;
  }
  return $errors;
}

/**
 * Generate a string from a list of errors for a particular field.
 *
 * @param string $key Field key.
 * @param integer $number Entry number of the field that triggered the errors.
 * @param mixed $error List of errors, or single error message.
 */
function pifr_server_xmlrpc_error_list($key, $number, $error) {
  if ($error) {
    pifr_server_xmlrpc_error(t('Error(s) found in @key #@number: !list.', array(
      '@key' => $key,
      '@number' => $number,
      '!list' => implode(', ', is_array($error) ? $error : array($error)),
    )));
  }
}

/**
 * Generate errors for if required keys are not found.
 *
 * @param array Data to check for required keys in.
 * @param array $required_keys List required keys.
 * @return array List of errors generated by missing keys.
 */
function pifr_server_xmlrpc_required_check(array $data, array $required_keys) {
  $errors = array();
  foreach ($required_keys as $required_key) {
    if (empty($data[$required_key])) {
      $errors[] = t('required key [@key] not found', array('@key' => $required_key));
    }
  }
  return $errors;
}

/**
 * Check if a value is an integer.
 *
 * @param mixed $value Value to check.
 * @return boolean TRUE if value is an integer, otherwise FALSE.
 */
function pifr_server_xmlrpc_is_int($value) {
  return is_numeric($value) && (int) $value == (float) $value;
}
