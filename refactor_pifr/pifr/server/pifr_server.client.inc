<?php

/**
 * @file
 * Provide client functions.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/*
 * Client types.
 */
define('PIFR_SERVER_CLIENT_TYPE_PROJECT', 1);
define('PIFR_SERVER_CLIENT_TYPE_TEST', 2);

/*
 * Client status codes.
 */
define('PIFR_SERVER_CLIENT_STATUS_DISABLED', 1);
define('PIFR_SERVER_CLIENT_STATUS_TESTING', 2);
define('PIFR_SERVER_CLIENT_STATUS_FAIL', 3);
define('PIFR_SERVER_CLIENT_STATUS_ENABLED', 4);

/**
 * Access check for viewing information related to client.
 *
 * @param integer $client_id Client ID.
 * @param object $account (Optional) The account to check, if not given use
 *   currently logged in user.
 * @return boolean TRUE if user has access, FALSE if not.
 */
function pifr_server_client_access($client_id, $account = NULL) {
  // Determine the user to evaluate against.
  if (!$account) {
    $account = $GLOBALS['user'];
  }

  // Allow if client has public viewing enabled or user owns client.
  $client = pifr_server_client_get($client_id);
  return $client['public'] || $client['uid'] == $account->uid;
}

/**
 * Access check for viewing activity logs related to client.
 *
 * @param integer $client_id Client ID.
 * @param object $account (Optional) The account to check, if not given use
 *   currently logged in user.
 * @return boolean TRUE if user has access, FALSE if not.
 */
function pifr_server_client_access_log($client_id, $account = NULL) {
  // Determine the user to evaluate against.
  if (!$account) {
    $account = $GLOBALS['user'];
  }

  // If user has access to all client tests then skip rest.
  if (user_access('view all pifr client tests', $account)) {
    return TRUE;
  }
  return pifr_server_client_access($client_id, $account);
}


/**
 * Load a client.
 *
 * @param integer $client_id Client ID.
 * @return array Client information, or FALSE.
 */
function pifr_server_client_get($client_id) {
  $result = db_query('SELECT *
                      FROM {pifr_client}
                      WHERE client_id = %d', $client_id);
  return pifr_server_client_load(db_fetch_array($result));
}

/**
 * Load all clients.
 *
 * @param interger $uid (Optional) Client owner user ID.
 * @return array List of clients.
 */
function pifr_server_client_get_all($uid = NULL) {
  if ($uid) {
    $result = db_query('SELECT *
                        FROM {pifr_client}
                        WHERE uid = %d', $uid);
  }
  else {
    $result = db_query('SELECT *
                        FROM {pifr_client}');
  }

  $clients = array();
  while ($client = db_fetch_array($result)) {
    $clients[] = pifr_server_client_load($client);
  }
  return $clients;
}

/**
 * Load a client by key.
 *
 * @param string $key Client key.
 * @return array Client information, or FALSE.
 */
function pifr_server_client_get_key($key) {
  $result = db_query("SELECT *
                      FROM {pifr_client}
                      WHERE client_key = '%s'", $key);
  return pifr_server_client_load(db_fetch_array($result));
}

/**
 * Load client assiciated with test.
 *
 * @param integer $test_id Test ID.
 * @return array Client information, or FALSE.
 */
function pifr_server_client_get_test($test_id) {
  $result = db_query("SELECT *
                      FROM {pifr_client}
                      WHERE test_id = %d", $test_id);
  return pifr_server_client_load(db_fetch_array($result));
}

/**
 * Load clients by type.
 *
 * @param interger $type Client type.
 * @return array List of clients.
 */
function pifr_server_client_get_type($type) {
  $result = db_query('SELECT *
                      FROM {pifr_client}
                      WHERE type = %d
                      ORDER BY client_id', $type);

  $clients = array();
  while ($client = db_fetch_array($result)) {
    $clients[] = pifr_server_client_load($client);
  }
  return $clients;
}

/**
 * Determine the last time a client was active.
 *
 * @param array $client Client information.
 * @return array Log information, or FALSE.
 */
function pifr_server_client_get_last_active(array $client) {
  $result = db_query('SELECT *
                      FROM {pifr_log}
                      WHERE client_id = %d
                      ORDER BY log_id DESC
                      LIMIT 1', $client['client_id']);
  return db_fetch_array($result);
}

/**
 * Load client by key and type (client must be enabled or testing).
 *
 * @param string $client_key Client key.
 * @param integer $type Client type ID.
 * @return array Client information, or FALSE.
 */
function pifr_server_client_check_key($client_key, $type) {
  $client = pifr_server_client_get_key($client_key);
  if ($client && $client['type'] == $type &&
      ($client['status'] == PIFR_SERVER_CLIENT_STATUS_ENABLED || $client['status'] == PIFR_SERVER_CLIENT_STATUS_TESTING)) {
    return $client;
  }
  return FALSE;
}

/**
 * Load client information by preforming necessary data cleanups.
 *
 * @param array $client Client information.
 * @return array Cleaned up branch information.
 */
function pifr_server_client_load($client) {
  if ($client) {
    $client['environment'] = unserialize($client['environment']);
  }
  return $client;
}

/**
 * Check if client confirmation tests need to be rerun and if so queue them.
 *
 * If the client confirmation tests were last tested longer then the the client
 * test interval then initiate the client testing process.
 *
 * @param array $client Client information.
 * @return array Client information which may be updated if client testing
 *   process was started.
 * @see pifr_server_client_confirmation_init()
 */
function pifr_server_client_confirmation_necessary(array $client) {
  if (PIFR_SERVER_CLIENT_TEST_INTERVAL != -1) {
    $test = pifr_server_test_get($client['test_id']);
    if (time() - $test['last_tested'] >= (PIFR_SERVER_CLIENT_TEST_INTERVAL * 60 * 60)) {
      pifr_server_client_confirmation_init($client);
      $client = pifr_server_client_get($client['client_id']);
    }
  }
  return $client;
}

/**
 * Initialize the client confirmation process.
 *
 * If the client is a project client it is simply enabled, but if the client
 * is a testing client then it is marked as testing and the related client test
 * is queued.
 *
 * @param array $client Client.
 */
function pifr_server_client_confirmation_init(array $client) {
  // Currently there is no testing of project clients, so just enable.
  if ($client['type'] == PIFR_SERVER_CLIENT_TYPE_PROJECT) {
    $client['status'] = PIFR_SERVER_CLIENT_STATUS_ENABLED;
    pifr_server_client_save($client);
    return;
  }

  // Mark client as testing.
  $client['status'] = PIFR_SERVER_CLIENT_STATUS_TESTING;
  pifr_server_client_save($client);

  // Queue client test.
  $test = pifr_server_test_get($client['test_id']);
  pifr_server_test_queue($test);

  // Reset client test stage.
  pifr_server_client_confirmation_status_clear($client);

  // Log event.
  pifr_server_log($test['test_id'], $client['client_id'], PIFR_SERVER_LOG_CONFIRMATION_START);
}

/**
 * Get the next stage of the client confirmation testing process.
 *
 * @param array $client Client information.
 * @return array XML-RPC reponse containing test definition and the environment.
 */
function pifr_server_client_confirmation_next($client) {
  // Determine the current status of the client confirmation test process.
  $environments = pifr_server_environment_test_get_all($client['test_id']);
  $status = pifr_server_client_confirmation_status($client);
  if (!$status) {
    // Client confirmation process has not started, pick the first environment.
    $environment = array_shift($environments);
    $status = array(
      'client_id' => $client['client_id'],
      'environment_id' => $environment['environment_id'],
      'stage' => 1,
    );
  }
  elseif ($status['stage'] == 0) {
    // Confirmation testing of the current environment has been completed.
    // Begin testing of next environment, since the check of results should
    // have marked testing as complete if final environment passed.
    $next = FALSE;
    foreach ($environments as $environment) {
      if ($next) {
        $status['environment_id'] = $environment['environment_id'];
        $status['stage'] = 1;
        break;
      }

      // Once the previously tested environment has been found the next
      // environment will be used.
      if ($environment['environment_id'] == $status['environment_id']) {
        $next = TRUE;
      }
    }
  }
  else {
    // Incrament stage and continue testing on current environment.
    $status['stage']++;
    $environment = pifr_server_environment_get($status['environment_id']);
  }

  // Save update status information.
  pifr_server_client_confirmation_status_save($status, $client);

  // Get the confirmation test definition for the current status.
  return array(pifr_server_review_confirmation_test($client, $environment, $status['stage']), $environment);
}

/**
 * Get the current status of a client confirmation test.
 *
 * @param array $client Client information.
 * @return array Status information including: 'client_id', 'environment_id',
 *   and 'stage'.
 */
function pifr_server_client_confirmation_status(array $client) {
  $result = db_query('SELECT *
                      FROM {pifr_client_test}
                      WHERE client_id = %d', $client['client_id']);
  return db_fetch_array($result);
}

/**
 * Save the status of a client confirmation test.
 *
 * Checks if the status information already exists and if so updates it,
 * otherwise the status information is inserted.
 *
 * @param array $status Status information.
 * @return array Status information.
 */
function pifr_server_client_confirmation_status_save(array $status, array $client) {
  if ($current = pifr_server_client_confirmation_status($client)) {
    drupal_write_record('pifr_client_test', $status, 'client_id');
  }
  else {
    drupal_write_record('pifr_client_test', $status);
  }
  return $status;
}

/**
 * Clear the confirmation test status.
 *
 * @param array $client Client information.
 */
function pifr_server_client_confirmation_status_clear(array $client) {
  db_query('DELETE FROM {pifr_client_test} WHERE client_id = %d', $client['client_id']);
}

/**
 * Check the results of a client confirmation test.
 *
 * The results of a client confirmation test need to be checked against the
 * stage of the test. If the results demonstrate a failure then the client will
 * be failed and the test will be halted, otherwise if the test demonstrates a
 * pass then the test will continue to the next stage. If there are no more
 * stages then the test will be marked as a pass.
 *
 * @param array $client Client information.
 * @param array $result Result information.
 */
function pifr_server_client_confirmation_check(array $client, array $result) {
  $status = pifr_server_client_confirmation_status($client);

  $check = pifr_server_review_confirmation_test_check($client, $result, $status);
  if ($check == 'pass') {
    $environments = pifr_server_environment_test_get_all($client['test_id']);
    $last = array_pop($environments);
    if ($status['environment_id'] == $last['environment_id']) {
      // All environments have passed, mark confirmation as pass.
      pifr_server_client_confirmation_pass($client);
      pifr_server_client_confirmation_status_clear($client);
    }
    else {
      // More environments left to confirm the client on.
      $status['stage'] = 0;
      pifr_server_client_confirmation_status_save($status, $client);
    }
  }
  elseif ($check == 'fail') {
    pifr_server_client_confirmation_fail($client);

    // Since the client confirmation status is only stored for the last
    // environment the client must be failed once an environment fails.
    $test = pifr_server_test_get($result['test_id']);
    $test['status'] = PIFR_SERVER_TEST_STATUS_RESULT;
    pifr_server_test_save($test);
  }
  else {
    // Result is fine and the test is not complete so continue to next stage.
    $test = pifr_server_test_get($result['test_id']);
    $test['status'] = PIFR_SERVER_TEST_STATUS_QUEUED;
    pifr_server_test_save($test);

    // Remove the previous test result since it will cause logic errors to have
    // more then one test result with the same environment ID, and we only
    // need to know that all the previous stages passed.
    $result_old = pifr_server_result_get_environment($test['test_id'], $result['environment_id']);
    pifr_server_result_delete($result_old);
  }
}

/**
 * Mark the client as having passed confirmation tests.
 *
 * @param array $client Client information.
 */
function pifr_server_client_confirmation_pass(array $client) {
  $client['status'] = PIFR_SERVER_CLIENT_STATUS_ENABLED;
  pifr_server_client_save($client);

  pifr_server_log($client['test_id'], $client['client_id'], PIFR_SERVER_LOG_CONFIRMATION_PASS);
}

/**
 * Mark the client as having failed confirmation tests.
 *
 * @param array $client Client information.
 */
function pifr_server_client_confirmation_fail(array $client) {
  $client['status'] = PIFR_SERVER_CLIENT_STATUS_FAIL;
  pifr_server_client_save($client);

  pifr_server_log($client['test_id'], $client['client_id'], PIFR_SERVER_LOG_CONFIRMATION_FAIL);
}

/**
 * Save client information.
 *
 * @param array $client Client information.
 * @return array Updated client information.
 */
function pifr_server_client_save(array $client) {
  if (isset($client['client_id'])) {
    // Existing client, update record.
    drupal_write_record('pifr_client', $client, 'client_id');
  }
  else {
    // Insert test record.
    $test = array(
      'type' => PIFR_SERVER_TEST_TYPE_CLIENT,
      'status' => $client['type'] == PIFR_SERVER_CLIENT_TYPE_PROJECT ? PIFR_SERVER_TEST_STATUS_STUB : PIFR_SERVER_TEST_STATUS_QUEUED,
      'title' => 'Client', // Place holder until a client_id has been assigned.
      'last_received' => 0,
      'last_requested' => 0,
      'last_tested' => 0,
      'test_count' => 0
    );
    $test = pifr_server_test_save($test);
    $client['test_id'] = $test['test_id'];

    // New client, insert record.
    drupal_write_record('pifr_client', $client);

    // Update test title now that client_id has been assigned and update
    // environment information.
    $test['title'] = pifr_server_test_title(PIFR_SERVER_TEST_TYPE_CLIENT, $client);
    $test = pifr_server_test_save($test);
  }
  return $client;
}

/**
 * Delete a client, its test, and all review data related to it.
 *
 * @param array $client Client information.
 */
function pifr_server_client_delete(array $client) {
  // Delete test and results.
  pifr_server_test_delete($client['test_id']);

  // Delete all projects that are related to the client.
  $projects = pifr_server_project_get_client($client['client_id']);
  foreach ($projects as $project) {
    pifr_server_project_delete($project);
  }

  // Delete branch record.
  db_query('DELETE FROM {pifr_client} WHERE client_id = %d', $client['client_id']);
}

/**
 * Render the environments a client can review.
 *
 * @param array $client Client information.
 * @return string HTML output.
 */
function pifr_server_client_environment_render(array $client) {
  $environments = array();
  foreach ($client['environment'] as $environment_id => $enabled) {
    if ($enabled) {
      $environment = pifr_server_environment_get($environment_id);
      $environments[] = $environment['title'];
    }
  }

  if ($environments) {
    return count($environments) > 1 ? theme('item_list', $environments) : $environments[0];
  }
  return '<em>' . t('None') . '</em>';
}

/**
 * Get the client type from the code.
 *
 * @param integer $type Client type code.
 * @return string Representation of client type.
 */
function pifr_server_client_type($type) {
  switch ($type) {
    case PIFR_SERVER_CLIENT_TYPE_PROJECT:
      return t('Project');
    case PIFR_SERVER_CLIENT_TYPE_TEST:
      return t('Test');
    default:
      return 'Unknown';
  }
}

/**
 * Get the client status from the code.
 *
 * @param integer $status Client status code.
 * @return string Representation of client status.
 */
function pifr_server_client_status($status) {
  switch ($status) {
    case PIFR_SERVER_CLIENT_STATUS_DISABLED:
      return t('Disabled');
    case PIFR_SERVER_CLIENT_STATUS_TESTING:
      return t('Testing client');
    case PIFR_SERVER_CLIENT_STATUS_FAIL:
      return t('Failed client check');
    case PIFR_SERVER_CLIENT_STATUS_ENABLED:
      return t('Enabled');
    default:
      return 'Unknown';
  }
}

class PIFRTestClient extends PIFRTest {

  protected $client;

  public function __construct(array $test) {
    parent::__construct($test);
    $this->client = pifr_server_client_get_test($test['test_id']);
  }

  public function getType() {
    return t('Client');
  }

  public function getInfo() {
    return $this->client;
  }

  public function getInfoTable() {
    $rows = array();

    $rows['environment'] = array(t('Environment'), pifr_server_client_environment_render($this->client));
    $rows['status'] = array(t('Status'), pifr_server_client_status($this->client['status']));

    return $this->formatTable($rows);
  }
}
