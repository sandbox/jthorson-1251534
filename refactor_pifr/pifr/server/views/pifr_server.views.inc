<?php
/**
 * @file
 * Provide views data definition.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Implementation of hook_views_data()
 */
function pifr_server_views_data() {
  $data = array();

  // ----------------------------------------------------------------
  // pifr_branch table -- basic table information.

  $data['pifr_branch']['table']['group']  = t('PIFR branch');

  $data['pifr_branch']['table']['base'] = array(
    'field' => 'branch_id',
    'title' => t('PIFR branch'),
    'help' => t('Repesents a version control branch.'),
    'weight' => -10,
  );

  $data['pifr_branch']['table']['join'] = array(
    'pifr_project' => array(
      'left_field' => 'project_id',
      'field' => 'project_id',
    ),
    'pifr_test' => array(
      'left_field' => 'test_id',
      'field' => 'test_id',
    ),
  );

  $data['pifr_branch']['branch_id'] = array(
    'title' => t('Branch ID'),
    'help' => t('Unique indentifier given to each branch.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'vcs_identifier',
      'numeric' => TRUE,
    ),
  );

  $data['pifr_branch']['project_id'] = array(
    'title' => t('Project'),
    'help' => t('Project to which branch is associated'),
    'relationship' => array(
      'base' => 'pifr_project',
      'field' => 'project_id',
      'handler' => 'views_handler_relationship',
      'label' => t('project'),
    ),
  );

  $data['pifr_branch']['test_id'] = array(
    'title' => t('Test ID'),
    'help' => t('Property of a test.'),
    'relationship' => array(
      'base' => 'pifr_test',
      'field' => 'test_id',
      'handler' => 'views_handler_relationship',
      'label' => t('PIFR test'),
    ),
  );

  $data['pifr_branch']['client_identifier'] = array(
    'title' => t('Client Identifier'),
    'help' => t('Unique client identifier.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pifr_branch']['vcs_identifier'] = array(
    'title' => t('VCS Identifier'),
    'help' => t('Verson control system identifier.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );


  // ----------------------------------------------------------------
  // pifr_client table -- basic table information.

  $data['pifr_client']['table']['group']  = t('PIFR client');

  $data['pifr_client']['table']['base'] = array(
    'field' => 'client_id',
    'title' => t('PIFR client'),
    'help' => t('Repesents a test client.'),
    'weight' => -10,
  );

  $data['pifr_client']['table']['join'] = array(
    'pifr_test' => array(
      'left_field' => 'test_id',
      'field' => 'test_id',
    ),
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
      'type' => 'INNER',
    ),
  );

  $data['pifr_client']['client_id'] = array(
    'title' => t('Client ID'),
    'help' => t('Unique indentifier given to each client.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pifr_client']['client_key'] = array(
    'title' => t('Client key'),
    'help' => t('The unique client key used for authentication between servers.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['pifr_client']['type'] = array(
    'title' => t('Type'),
    'help' => t('The type of client.'),
    'field' => array(
      'handler' => 'views_handler_field_pifr_client_type',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pifr_client']['environment_id'] = array(
    'title' => t('Environment'),
    'help' => t('The type of client environment.'),
    'field' => array(
      'handler' => 'views_handler_field_pifr_client_environment',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pifr_client']['uid'] = array(
    'title' => t('User'),
    'help' => t('Relate a client to the user who created the client.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'field' => 'uid',
      'label' => t('user'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_user_uid',
    ),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pifr_client']['url'] = array(
    'title' => t('URL'),
    'help' => t('Base URL of client.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['pifr_client']['status'] = array(
    'title' => t('Status'),
    'help' => t('The status of client.'),
    'field' => array(
      'handler' => 'views_handler_field_pifr_client_status',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pifr_client']['test_id'] = array(
    'title' => t('Test ID'),
    'help' => t('Property of a test.'),
    'relationship' => array(
      'base' => 'pifr_test',
      'field' => 'test_id',
      'handler' => 'views_handler_relationship',
      'label' => t('PIFR test'),
    ),
  );

  $data['pifr_client']['operations_admin'] = array(
    'title' => t('Admin operations'),
    'help' => t('Field handler to present admin client operations.'),
    'field' => array(
      'handler' => 'views_handler_field_pifr_client_operations_admin',
    ),
  );

  $data['pifr_client']['operations_user'] = array(
    'title' => t('User operations'),
    'help' => t('Field handler to present user client operations.'),
    'field' => array(
      'handler' => 'views_handler_field_pifr_client_operations_user',
    ),
  );

  // ----------------------------------------------------------------
  // pifr_environment table -- basic table information.

  $data['pifr_environment']['table']['group']  = t('PIFR environment');

  $data['pifr_environment']['table']['base'] = array(
    'field' => 'environment_id',
    'title' => t('PIFR environment'),
    'help' => t('Represents a client environment.'),
    'weight' => -10,
  );

  $data['pifr_environment']['table']['join'] = array(
    'pifr_client' => array(
      'left_field' => 'environment_id',
      'field' => 'environment_id',
    ),
    'pifr_test' => array(
      'left_field' => 'environment_id',
      'field' => 'environment_id',
    ),
  );

  $data['pifr_environment']['environment_id'] = array(
    'title' => t('Environment ID'),
    'help' => t('Unique indentifier given to each environment.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pifr_environment']['title'] = array(
    'title' => t('Title'),
    'help' => t('Environment title.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['pifr_environment']['description'] = array(
    'title' => t('Description'),
    'help' => t('A clear description of the environment.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // ----------------------------------------------------------------
  // pifr_file table -- basic table information.

  $data['pifr_file']['table']['group']  = t('PIFR file');

  $data['pifr_file']['table']['base'] = array(
    'field' => 'file_id',
    'title' => t('PIFR file'),
    'help' => t('Repesents a patch file.'),
    'weight' => -10,
  );

  $data['pifr_file']['table']['join'] = array(
    'pifr_test' => array(
      'left_field' => 'test_id',
      'field' => 'test_id',
    ),
  );

  $data['pifr_file']['file_id'] = array(
    'title' => t('File ID'),
    'help' => t('Unique indentifier given to each file.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pifr_file']['branch_id'] = array(
    'title' => t('Branch ID'),
    'help' => t('Property of a branch.'),
    'relationship' => array(
      'base' => 'pifr_branch',
      'field' => 'branch_id',
      'handler' => 'views_handler_relationship',
      'label' => t('Branch ID'),
    ),
  );

  $data['pifr_file']['issue_nid'] = array(
    'title' => t('Issue NID'),
    'help' => t('The NID in which the file was posted.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pifr_file']['issue_cid'] = array(
    'title' => t('Issue CID'),
    'help' => t('The CID in which the file was posted.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pifr_file']['file_url'] = array(
    'title' => t('File URL'),
    'help' => t('The URL of the file on the project server.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['pifr_file']['file_name'] = array(
    'title' => t('File name'),
    'help' => t('The name of the file on the project server.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['pifr_file']['test_id'] = array(
    'title' => t('Test ID'),
    'help' => t('Property of a test.'),
    'relationship' => array(
      'base' => 'pifr_test',
      'field' => 'test_id',
      'handler' => 'views_handler_relationship',
      'label' => t('PIFR test'),
    ),
  );

  // ----------------------------------------------------------------
  // pifr_log table -- basic table information.

  $data['pifr_log']['table']['group']  = t('PIFR log');

  $data['pifr_log']['table']['base'] = array(
    'field' => 'log_id',
    'title' => t('PIFR log'),
    'help' => t('Repesents a log entry.'),
    'weight' => -10,
  );

  $data['pifr_log']['table']['join'] = array(
    'pifr_client' => array(
      'left_field' => 'client_id',
      'field' => 'client_id',
      'type' => 'INNER',
    ),
    'pifr_test' => array(
      'left_field' => 'test_id',
      'field' => 'test_id',
      'type' => 'INNER',
    ),
  );

  $data['pifr_log']['log_id'] = array(
    'title' => t('Log ID'),
    'help' => t('Unique indentifier given to each log entry.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pifr_log']['test_id'] = array(
    'title' => t('Test'),
    'help' => t('Property of a test.'),
    'relationship' => array(
      'base' => 'pifr_test',
      'field' => 'test_id',
      'handler' => 'views_handler_relationship',
      'label' => t('test'),
    ),
  );

  $data['pifr_log']['client_id'] = array(
    'title' => t('Client'),
    'help' => t('Reference to client.'),
    'relationship' => array(
      'base' => 'pifr_client',
      'field' => 'client_id',
      'handler' => 'views_handler_relationship',
      'label' => t('client'),
    ),
  );

  $data['pifr_log']['code'] = array(
    'title' => t('Code'),
    'help' => t('Event code.'),
    'field' => array(
      'handler' => 'views_handler_field_pifr_log_code',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pifr_log']['timestamp'] = array(
    'title' => t('Timestamp'),
    'help' => t('Date/time when log entry occured.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // ----------------------------------------------------------------
  // pifr_project table -- basic table information.

  $data['pifr_project']['table']['group']  = t('PIFR project');

  $data['pifr_project']['table']['base'] = array(
    'field' => 'project_id',
    'title' => t('PIFR project'),
    'help' => t('Repesents a project.'),
    'weight' => -10,
  );

  $data['pifr_project']['table']['join'] = array(
    'pifr_client' => array(
      'left_field' => 'client_id',
      'field' => 'client_id',
      'type' => 'INNER',
    ),
  );

  $data['pifr_project']['project_id'] = array(
    'title' => t('Project ID'),
    'help' => t('Unique indentifier given to each project.'),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'name',
      'numeric' => TRUE,
    ),
  );

  $data['pifr_project']['client_id'] = array(
    'title' => t('Client ID'),
    'help' => t('Reference to client.'),
    'relationship' => array(
      'base' => 'pifr_client',
      'field' => 'client_id',
      'handler' => 'views_handler_relationship',
      'label' => t('client'),
    ),
  );

  $data['pifr_project']['client_identifier'] = array(
    'title' => t('Client identifier'),
    'help' => t('Unique identifier used by client to reference project.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pifr_project']['name'] = array(
    'title' => t('Name'),
    'help' => t('Project name.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['pifr_project']['repository_type'] = array(
    'title' => t('Repository type'),
    'help' => t('Type of repository.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pifr_project']['repository_url'] = array(
    'title' => t('Repository URL'),
    'help' => t('URL of repository.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // ----------------------------------------------------------------
  // pifr_test table -- basic table information.

  $data['pifr_test']['table']['group']  = t('PIFR test');

  $data['pifr_test']['table']['base'] = array(
    'field' => 'test_id',
    'title' => t('PIFR test'),
    'help' => t('PIFR test is the basis for the three test types.'),
    'weight' => -10,
  );

  $data['pifr_test']['table']['join'] = array(
    'pifr_client' => array(
      'handler' => 'views_join',
      'left_table' => 'pifr_client',
      'left_field' => 'test_id',
      'field' => 'test_id',
    ),
    'pifr_branch' => array(
      'handler' => 'views_join',
      'left_table' => 'pifr_branch',
      'left_field' => 'test_id',
      'field' => 'test_id',
    ),
    'pifr_file' => array(
      'handler' => 'views_join',
      'left_table' => 'pifr_file',
      'left_field' => 'test_id',
      'field' => 'test_id',
    ),
  );

  $data['pifr_test']['test_id'] = array(
    'title' => t('Test ID'),
    'help' => t('Unique indentifier given to each test.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pifr_test']['type'] = array(
    'title' => t('Type'),
    'help' => t('The type of test.'),
    'field' => array(
      'handler' => 'views_handler_field_pifr_test_type',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pifr_test']['status'] = array(
    'title' => t('Status'),
    'help' => t('The status of test.'),
    'field' => array(
      'handler' => 'views_handler_field_pifr_test_status',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pifr_test']['title'] = array(
    'title' => t('Title'),
    'help' => t('The summarized title of the test.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'arguments' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['pifr_test']['client_id'] = array(
    'title' => t('Client ID'),
    'help' => t('The last client to review test.'),
    'relationship' => array(
      'base' => 'pifr_client',
      'field' => 'client_id',
      'handler' => 'views_handler_relationship',
      'label' => t('Client ID'),
    ),
  );

  $data['pifr_test']['last_received'] = array(
    'title' => t('Last received'),
    'help' => t('When test request was last received.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['pifr_test']['last_requested'] = array(
    'title' => t('Last requested'),
    'help' => t('When test was last requested by client.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['pifr_test']['last_tested'] = array(
    'title' => t('Last tested'),
    'help' => t('When test last finished.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['pifr_test']['test_count'] = array(
    'title' => t('Test count'),
    'help' => t('Total number of times the test has been performed.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function pifr_server_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'pifr_server') . '/views/handlers',
    ),
    'handlers' => array(
      'views_handler_field_pifr_client_environment' => array(
        'parent' => 'views_handler_field',
      ),
      'views_handler_field_pifr_client_status' => array(
        'parent' => 'views_handler_field',
      ),
      'views_handler_field_pifr_client_type' => array(
        'parent' => 'views_handler_field',
      ),
      'views_handler_field_pifr_client_operations_admin' => array(
        'parent' => 'views_handler_field',
      ),
      'views_handler_field_pifr_client_operations_user' => array(
        'parent' => 'views_handler_field',
      ),
      'views_handler_field_pifr_log_code' => array(
        'parent' => 'views_handler_field',
      ),
      'views_handler_field_pifr_test_type' => array(
        'parent' => 'views_handler_field',
      ),
      'views_handler_field_pifr_test_status' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_plugins
 */
function pifr_server_views_plugins() {
  return array(
    'module' => 'views',
    'row' => array(
      'pifr_log_rss' => array(
        'title' => t('PIFR log'),
        'help' => t('Includes standard PIFR log fields.'),
        'handler' => 'views_plugin_row_pifr_log_rss',
        'path' => drupal_get_path('module', 'pifr_server') . '/views/plugins',
        'theme' => 'views_view_row_rss',
        'base' => array('pifr_log'),
        'uses options' => FALSE,
        'type' => 'feed',
      ),
    ),
  );
}
