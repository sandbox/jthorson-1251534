<?php

/**
 * @file
 * Provides PIFR log RSS row style plugin.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * PIFR log RSS row style plugin.
 */
class views_plugin_row_pifr_log_rss extends views_plugin_row {

  function render($row) {
    global $base_url;

    $log = pifr_server_log_get($row->log_id);
    $test = pifr_server_test_get($log['test_id']);

    $item = new stdClass();
    $item->title = $test['title'] . ': ' . pifr_server_log_code($log['code'], $log['client_id']);
    $item->link = url('pifr/test/' . $log['test_id'], array('absolute' => TRUE));
    $item->description = '';
    $item->elements = array(
      array(
        'key' => 'pubDate',
        'value' => gmdate('r', $log['timestamp']),
      ),
      array(
        'key' => 'guid',
        'value' => 'log ' .  $log['log_id'] . ' at ' . $base_url,
        'attributes' => array('isPermaLink' => 'true'),
        'namespace' => array('xmlns:dc' => 'http://purl.org/dc/elements/1.1/'),
      ),
      array(
        'key' => 'testId',
        'value' => $test['test_id'],
      ),
      array(
        'key' => 'testTitle',
        'value' => $test['title'],
      ),
    );

    foreach ($item->elements as $element) {
      if (isset($element['namespace'])) {
        $this->view->style_plugin->namespaces = array_merge($this->view->style_plugin->namespaces, $element['namespace']);
      }
    }

    return theme($this->theme_functions(), $this->view, $this->options, $item);
  }
}
