<?php
/**
 * Field handler to present test status as string.
 */
class views_handler_field_pifr_test_status extends views_handler_field {
  function render($values) {
    return pifr_server_test_status($values->{$this->field_alias});
  }
}
