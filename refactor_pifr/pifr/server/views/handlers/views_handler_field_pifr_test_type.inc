<?php
/**
 * Field handler to present test type as string.
 */
class views_handler_field_pifr_test_type extends views_handler_field {
  function render($values) {
    return pifr_server_test_type($values->{$this->field_alias});
  }
}
