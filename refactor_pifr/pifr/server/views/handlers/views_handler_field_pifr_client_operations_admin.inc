<?php

/**
 * Field handler to present admin client operations.
 */
class views_handler_field_pifr_client_operations_admin extends views_handler_field {
  function query() {
    // Do nothing -- to override the parent query.
  }

  function render($values) {
    module_load_include('manage.inc', 'pifr_server');

    $client = array(
      'client_id' => $values->client_id,
      'test_id' => $values->pifr_test_test_id,
      'status' => $values->pifr_client_status,
    );
    $operations = pifr_server_manage_client_list_operations($client);
    return pifr_server_manage_render_operations($operations);
  }
}
