<?php
/**
 * Field handler to present client status as string.
 */
class views_handler_field_pifr_client_status extends views_handler_field {
  function render($values) {
    return pifr_server_client_status($values->{$this->field_alias});
  }
}
