<?php
/**
 * Field handler to present client environment as string.
 */
class views_handler_field_pifr_log_code extends views_handler_field {
  function render($values) {
    return pifr_server_log_code($values->{$this->field_alias}, $values->pifr_client_pifr_log_client_id);
  }
}
