<?php
/**
 * @file
 * Automatically review patches attached to issues.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Dispaly report value on general dashboard.
 */
function pifr_server_admin_dashboard_form(&$form, $form_state) {
  $form['status']['report'] = array(
    '#type' => 'item',
    '#title' => t('Report'),
    '#value' => theme('pifr_state', PIFR_SERVER_REPORT)
  );
  $form['status']['toggle_report'] = array(
    '#type' => 'submit',
    '#value' => t('Toggle report'),
    '#submit' => array('pifr_serveradmin_dashboard_form_toggle_submit'),
    '#weight' => 1001
  );

  $form['tools'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tools'),
    '#description' => t('Tools useful during development/debug.'),
    '#access' => PIFR_DEBUG,
    '#weight' => 0,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['tools']['queue'] = array(
    '#type' => 'fieldset',
    '#title' => t('Queue'),
    '#attributes' => array('class' => 'container-inline'),
  );
  $form['tools']['queue']['client_id'] = array(
    '#type' => 'select',
    '#title' => t('Project client'),
    '#options' => array(),
  );

  $clients = pifr_server_client_get_type(PIFR_SERVER_CLIENT_TYPE_PROJECT);
  foreach ($clients as $client) {
    $form['tools']['queue']['client_id']['#options'][$client['client_id']] = t('Client @client_id', array('@client_id' => $client['client_id']));
  }

  $form['tools']['queue']['op'] = array(
    '#type' => 'submit',
    '#value' => t('Queue tests'),
    '#submit' => array('pifr_serveradmin_dashboard_form_queue_submit'),
  );

  $form['tools']['clear'] = array(
    '#type' => 'submit',
    '#value' => t('Clear all tests'),
    '#submit' => array('pifr_serveradmin_dashboard_form_clear_submit'),
  );
}

/**
 * Toggle report variable.
 */
function pifr_serveradmin_dashboard_form_toggle_submit($form, &$form_state) {
  variable_set('pifr_server_report', !PIFR_SERVER_REPORT);
}

/**
 * Queue a set of generic tests.
 */
function pifr_serveradmin_dashboard_form_queue_submit($form, &$form_state) {
  $batch = array(
    'branches' => array(),
    'files' => array(),
    'projects' => array(),
  );

  $batch['branches'][] = array(
    'project_identifier' => '3060',
    'client_identifier' => 156281, # this could be a random number
    'vcs_identifier' => '7.4',   // GITMIGRATION fragile
    'dependency' => '',
    'plugin_argument' => array(),
    'test' => TRUE,
  );

  $batch['files'][] = array(
    'branch_identifier' => $batch['branches'][0]['client_identifier'],
    'client_identifier' => 17,  # this could be a random number
    'file_url' => url(file_directory_path() . '/pifr_simpletest/pass.patch', array('absolute' => TRUE)),
  );
  $batch['files'][] = array(
    'branch_identifier' => $batch['branches'][0]['client_identifier'],
    'client_identifier' => 1337,
    'file_url' => url('', array('absolute' => TRUE)) . drupal_get_path('module', 'pifr_simpletest') . '/confirmation/fail.patch',
  );

  $batch['projects'][] = array(
    'client_identifier' => 3060, # this could be a random number
    'name' => 'Drupal',
    'repository_type' => 'git',   // GITMIGRATION fragile
    'repository_url' => variable_get('pifr_drupal_repository', 'git://git.drupal.org/project/drupal.git'),
  );

  module_load_include('xmlrpc.inc', 'pifr_server');
  $client = pifr_server_client_get($form_state['values']['client_id']);
  $response = pifr_server_xmlrpc_queue($client['client_key'], $batch);

  if (empty($response['response'])) {
    // Print out links to each of the queued tests.
    foreach (array_merge($response['branches'], $response['files']) as $test) {
      drupal_set_message('Queued test: ' . l('#' . $test, 'pifr/test/' . $test));
    }
  }
  else {
    // Display error information.
    drupal_set_message(pifr_response_code($response['response']), 'warning');
    if (!empty($response['errors'])) {
      foreach ($response['errors'] as $error) {
        drupal_set_message($error, 'error');
      }
    }
  }
}

/**
 * Clear all test information.
 */
function pifr_serveradmin_dashboard_form_clear_submit($form, &$form_state) {
  foreach (array('branch', 'file', 'log', 'project', 'result', 'result_detail', 'result_detail_assertion') as $table) {
    db_query('TRUNCATE {pifr_' . $table . '}');
  }
  db_query('DELETE FROM {pifr_test} WHERE type != %d', PIFR_SERVER_TEST_TYPE_CLIENT);
  drupal_set_message('Cleared test(s) and result(s).');
}

/**
 * Display server key.
 */
function pifr_server_admin_configuration_form(&$form, $form_state) {
  $form['server'] = array(
    '#type' => 'fieldset',
    '#title' => t('Server'),
    '#weight' => -9
  );

  $form['server']['pifr_server_client_test_interval'] = array(
    '#type' => 'select',
    '#title' => t('Test client interval'),
    '#description' => t('The interval, in hours, after which a test client will be re-confirmed.'),
    '#options' => drupal_map_assoc(array(
      -1, 6, 12, 24, 48, 72,
    )),
    '#default_value' => PIFR_SERVER_CLIENT_TEST_INTERVAL
  );
  $form['server']['pifr_server_client_test_interval']['#options'][-1] = t('Never');

  $form['server']['pifr_drupal_repository'] = array(
    '#type' => 'textfield',
    '#title' => t('Drupal core repository URL'),
    '#default_value' => variable_get('pifr_drupal_repository', 'git://git.drupal.org/project/drupal.git'),
    '#required' => TRUE,
  );
  $form['server']['pifr_confirmation_vcs_id'] = array(
    '#type' => 'textfield',
    '#title' => t('VCS ID to be used for testbot configuration'),
    '#description' => t('The VCS branch or tag to be used for testbot (client) confirmation, as in "7.2"'),
    '#default_value' => variable_get('pifr_confirmation_vcs_id', '7.2'),
  );

}
