
/**
 * Provide the show/hide functionality when assertions are displayed beneith
 * the summary results for a test.
 */
Drupal.behaviors.pifrResultAssertions = function() {

  // Cycle through each assertion wrapper and hide them and use the summary
  // row above them as a toggle.
  $('.pifr-assertion-wrapper').each(function() {
    $(this).hide();
    $(this).prev().addClass('pifr-assertion-header');

    $(this).prev().click(function() {
      var index = ($(this).next().css('display') == 'none' ? 1 : 0);
      $(this).find('img').replaceWith(Drupal.settings.pifr_assertion[index]);
      $(this).next().toggle();
    });
  });
}

/**
 * Provides show/hide functionality when selecting a client type on the client
 * form. When type of 'project' then the environment field should be hidden,
 * otherwise it should be displayed.
 */
Drupal.behaviors.pifrClientForm = function() {

  // Set initial state.
  if ($('input[name="type"]:checked').length != 0) {
    setState($('input[name="type"]:checked').val());
  }

  // React to changes when clicked.
  $('input[name="type"]').change(function() {
    setState($(this).val());
  });

  function setState(value) {
    $environment = $('div.form-item:has(label:contains("Environment"))');
    $public = $('div.form-item:has(label:contains("Public"))');
    if (value == '1') {
      $environment.hide();
      $public.show();
    }
    else {
      $environment.show();
      $public.hide();
    }
  }
}

/**
 * Konami code implementation.
 *
 * Make a test result appear as having no failures of any kind.
 *
 * @link http://en.wikipedia.org/wiki/List_of_Konami_code_websites
 */
Drupal.behaviors.pifrKonami = function() {

  var kkeys = [];
  var konami = "38,38,40,40,37,39,37,39,66,65";

  $(window).keydown(function(event) {
    kkeys.push(event.keyCode);
    if (kkeys.toString().indexOf(konami) >= 0) {
      // Environment status display.
      $('#pifr-environment td:contains("fail")').html('pass');

      // Overall summary mesasge.
      $('#pifr-result-summary-message').html('Passed on all environments.');

      // Steps display.
      $('li.pifr-fail div').hide();
      $('li').removeClass('pifr-fail').addClass('pifr-pass');

      // Individual environment summary.
      $('.pifr-assertion-summary').html('1,000,000,000,000 pass(es)');

      // Both summary messages.
      $('.pifr-error').removeClass('pifr-error').addClass('pifr-ok');

      // Assertion details.
      $('tr.pifr-fail, tr.pifr-exception')
        .removeClass('pifr-fail')
        .removeClass('pifr-exception')
        .removeClass('pifr-assertion-header')
        .addClass('pifr-pass');
    }
  });
}
