<?php
/**
 * @file
 * Provide event related funtions such as trigger and action definitions.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Implementation of hook_hook_info().
 */
function pifr_server_hook_info() {
  return array(
    'pifr_server' => array(
      'pifr_server' => array(
        'result' => array(
          'runs when' => t('After result has been received from test client'),
        ),
        'client_result' => array(
          'runs when' => t('After client test result has been received from test client'),
        ),
      ),
    ),
  );
}

/**
 * Implementation of hook_action_info().
 */
function pifr_server_action_info() {
  return array(
    'pifr_server_notification_email_action' => array(
      'description' => t('Send e-mail about test results received'),
      'type' => 'pifr_test',
      'configurable' => TRUE,
      'hooks' => array(
        'pifr_server' => array('result', 'client_result'),
      ),
    ),
  );
}

/**
 * Implementation of hook_mail().
 */
function pifr_server_mail($key, &$message, $params) {
  $context = $params['context'];
  $variables = array(
    '%recipient' => $context['recipient'],
  );

  if (!empty($params['test'])) {
    $test = $params['test'];
    $pass = pifr_server_result_summary_status($test['test_id']);

    // Generate a summary of the details for each result.
    $plugin_info = pifr_server_review_get_all();
    $results = pifr_server_result_get_all($test['test_id']);
    $detail_summaries = array();
    foreach ($results as $result) {
      $environment = pifr_server_environment_get($result['environment_id']);
      $detail_summary = '-- [[' . $plugin_info[$environment['plugin']] . ']]: [' . $environment['title'] . "] --\n\n";
      $detail_summary .= pifr_server_review_detail_summary($test, $result);
      $detail_summaries[] = $detail_summary;
    }

    $variables = array(
      '%link' => url('pifr/test/' . $test['test_id'], array('absolute' => TRUE)),
      '%test_id' => $test['test_id'],
      '%type' => pifr_server_test_type($test['type']),
      '%status' => pifr_server_test_status($test['status']),
      '%title' => $test['title'],
      '%last_received' => format_date($test['last_received']),
      '%last_requested' => format_date($test['last_requested']),
      '%last_tested' => format_date($test['last_tested']),
      '%test_count' => number_format($test['test_count']),
      '%result_word' => t($pass ? 'passed' : 'failed', array()),
      '%result_summary' => pifr_server_result_summary_message($test['test_id']),
      '%result_detail_summary' => implode("\n\n", $detail_summaries),
    );
  }

  if ($key == 'pifr_server_notification_email_action') {
    $message['headers']['Content-Type'] = 'text/html; charset=UTF-8;';
    $message['subject'] = strip_tags(strtr($context['subject'], $variables));

    $body = strip_tags(strtr($context['body'], $variables));
    $message['body'][] = str_replace(array("\n", '  '), array("<br>\n", '&nbsp;&nbsp;'), $body);
  }
}

/**
 * Implementation of hook_pifr_server_result().
 */
function pifr_server_pifr_server_result($test, $result) {
  // Only trigger event if trigger module is enabled.
  if (!module_exists('trigger')) {
    return;
  }

  // Ensure that testing is complete.
  if ($test['status'] == PIFR_SERVER_TEST_STATUS_RESULT && $test['type'] != PIFR_SERVER_TEST_TYPE_CLIENT) {
    $context = array(
      'hook' => 'pifr_server',
      'op' => 'result',
    );
    $aids = _trigger_get_hook_aids($context['hook'], $context['op']);
    foreach ($aids as $aid => $action_info) {
      actions_do($aid, $test, $context);
    }
  }
}

/**
 * Implementation of hook_pifr_server_client_result().
 */
function pifr_server_pifr_server_client_result($test, $result, $client) {
  // Only trigger event if trigger module is enabled.
  if (!module_exists('trigger')) {
    return;
  }

  // Ensure that testing is complete.
  if ($test['status'] == PIFR_SERVER_TEST_STATUS_RESULT) {
    $context = array(
      'hook' => 'pifr_server',
      'op' => 'client_result',
      'client' => $client,
    );
    $aids = _trigger_get_hook_aids($context['hook'], $context['op']);
    foreach ($aids as $aid => $action_info) {
      actions_do($aid, $test, $context);
    }
  }
}

/**
 * Send e-mail about test results received.
 */
function pifr_server_notification_email_action($test, $context) {
  global $user;

  // Check conditions.
  if (!empty($context['test_id'])) {
    $test_ids = explode(',', $context['test_id']);
    if (!in_array($test['test_id'], $test_ids)) {
      return;
    }
  }

  if ($context['result'] != 'any') {
    $result = pifr_server_result_summary_status($test['test_id']) ? 'pass' : 'fail';
    if ($context['result'] != $result) {
      return;
    }
  }

  $params = array(
    'test' => $test,
    'context' => $context,
  );

  $language = user_preferred_language($user);

  // If client test result then add owner to list of recipients.
  $recipients = explode(',', $params['context']['recipient']);
  if (!empty($context['client'])) {
    $user = user_load($context['client']['uid']);
    $recipients[] = $user->mail;
  }
  foreach ($recipients as $recipient) {
    if ($recipient = trim($recipient)) {
      drupal_mail('pifr_server', 'pifr_server_notification_email_action', $recipient, $language, $params);
    }
  }
}

/**
 * E-mail notification settings form.
 */
function pifr_server_notification_email_action_form($context) {
  $form = array();

  // Default subject and body of notification e-mail.
  $subject = '[%result_word] %title on %last_tested';
  $body = '
Overall Summary
==============================================

%result_summary

Individual Environment Summaries
==============================================

%result_detail_summary

For more information please visit the test detail page [%link].

-- ' . '
Your friendly automated testing system';

  // Notification e-mail settings.
  $form['mail'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mail'),
    '#description' => t('Notification e-mail settings. Avaialable placeholders for subject and body: %recipient, %link, %test_id, %type, %status, %title, %last_received, %last_requested, %last_tested, %test_count, %result_word, %result_summary, %result_detail_summary.'),
  );
  $form['mail']['recipient'] = array(
    '#type' => 'textfield',
    '#title' => t('Recipient'),
    '#description' => t('Comma separated list of recipient e-mail addresses. When sending client confirmation results the client owner will automatically be added.'),
    '#default_value' => isset($context['recipient']) ? $context['recipient'] : '',
  );
  $form['mail']['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#description' => t('Subject of notification e-mail.'),
    '#default_value' => isset($context['subject']) ? $context['subject'] : $subject,
    '#required' => TRUE,
  );
  $form['mail']['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#description' => t('Body of notification e-mail.'),
    '#rows' => 15,
    '#default_value' => isset($context['body']) ? $context['body'] : $body,
    '#required' => TRUE,
  );

  $form['condition'] = array(
    '#type' => 'fieldset',
    '#title' => t('Conditions'),
    '#description' => t('Conditions that must be met for notification to be sent.'),
  );
  $form['condition']['test_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Test'),
    '#description' => t('Comma separated list of test IDs to which the results must belong.'),
    '#default_value' => isset($context['test_id']) ? $context['test_id'] : '',
  );
  $form['condition']['result'] = array(
    '#type' => 'radios',
    '#title' => t('Result'),
    '#description' => t('The test outcome that the result must represent.'),
    '#options' => array(
      'pass' => 'Pass',
      'fail' => 'Fail',
      'any' => 'Any',
    ),
    '#default_value' => isset($context['result']) ? $context['result'] : 'both',
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Save e-mail notifications settings.
 */
function pifr_server_notification_email_action_submit($from, &$form_state) {
  return array(
    'recipient' => $form_state['values']['recipient'],
    'subject' => $form_state['values']['subject'],
    'body' => $form_state['values']['body'],
    'test_id' => $form_state['values']['test_id'],
    'result' => $form_state['values']['result'],
  );
}
