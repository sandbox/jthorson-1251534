<?php

/**
 * @file
 * Provide event logging functions.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/*
 * Log event codes.
 */
define('PIFR_SERVER_LOG_CONFIRMATION_START', 1);
define('PIFR_SERVER_LOG_CONFIRMATION_PASS', 2);
define('PIFR_SERVER_LOG_CONFIRMATION_FAIL', 3);

define('PIFR_SERVER_LOG_TEST_REQUEST', 4);
define('PIFR_SERVER_LOG_TEST_REQUEST_MANAGER', 5);
define('PIFR_SERVER_LOG_TEST_REQUEST_IGNORE', 6);

define('PIFR_SERVER_LOG_CLIENT_REQUEST', 7);
define('PIFR_SERVER_LOG_CLIENT_RESET', 8);
define('PIFR_SERVER_LOG_CLIENT_RESULT', 9);
define('PIFR_SERVER_LOG_CLIENT_RETRIEVE', 10);

/**
 * Log an event that occurred to a test.
 *
 * @param interger $test_id Test ID.
 * @param interger $client_id Client ID.
 * @param integer $code Event code, PIFR_SERVER_LOG_*.
 * @return array Update log event information.
 */
function pifr_server_log($test_id, $client_id, $code) {
  // Check for an invalid event code.
  if ($code < 1 || $code > 10) {
    trigger_error('Invalid log event code.', E_USER_ERROR);
  }

  // Save event to log.
  $log = array(
    'test_id' => $test_id,
    'client_id' => $client_id,
    'code' => $code,
    'timestamp' => time(),
  );
  drupal_write_record('pifr_log', $log);

  return $log;
}

/**
 * Load log entry.
 *
 * @param integer $log_id Log ID.
 * @return array Log information.
 */
function pifr_server_log_get($log_id) {
  $result = db_query('SELECT *
                      FROM {pifr_log}
                      WHERE log_id = %d', $log_id);
  return db_fetch_array($result);
}

/**
 * Delete a log entries associated with a test.
 *
 * @param integer $test_id Test ID.
 */
function pifr_server_log_delete($test_id) {
  db_query('DELETE FROM {pifr_log} WHERE test_id = %d', $test_id);
}

/**
 * Get the event message from the code.
 *
 * @param integer $code Event code.
 * @param integer $client_id (Optional) Client ID.
 * @return string Representation of the event code.
 */
function pifr_server_log_code($code, $client_id = 0) {
  switch ($code) {
    case PIFR_SERVER_LOG_CONFIRMATION_START:
      return t('Client confirmation process started.');
    case PIFR_SERVER_LOG_CONFIRMATION_PASS:
      return t('Passed confirmation tests.');
    case PIFR_SERVER_LOG_CONFIRMATION_FAIL:
      return t('Failed confirmation tests.');
    case PIFR_SERVER_LOG_TEST_REQUEST:
      return t('Test request received.');
    case PIFR_SERVER_LOG_TEST_REQUEST_MANAGER:
      return t('Re-test requested by manager.');
    case PIFR_SERVER_LOG_TEST_REQUEST_IGNORE:
      return t('Re-test request ignored because test is already queued.');
    case PIFR_SERVER_LOG_CLIENT_REQUEST:
      return t('Requested by test client #@client_id.', array('@client_id' => $client_id));
    case PIFR_SERVER_LOG_CLIENT_RESET:
      return t('Test reset by client request.');
    case PIFR_SERVER_LOG_CLIENT_RESULT:
      return t('Result received from test client #@client_id.', array('@client_id' => $client_id));
    case PIFR_SERVER_LOG_CLIENT_RETRIEVE:
      return t('Result retrieved by project client.');
    default:
      return t('Unknown');
  }
}
