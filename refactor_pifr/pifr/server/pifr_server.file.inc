<?php

/**
 * @file
 * Provide file functions.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Load a file.
 *
 * @param integer $file_id File ID.
 * @return array File information, or FALSE.
 */
function pifr_server_file_get($file_id) {
  $result = db_query('SELECT *
                      FROM {pifr_file}
                      WHERE file_id = %d', $file_id);
  return db_fetch_array($result);
}

/**
 * Load all files associated with a branch.
 *
 * @param integer $branch_id Branch ID.
 * @return array List of files.
 */
function pifr_server_file_get_all($branch_id) {
  $result = db_query('SELECT *
                      FROM {pifr_file}
                      WHERE branch_id = %d', $branch_id);
  $files = array();
  while ($file = db_fetch_array($result)) {
    $files[] = $file;
  }
  return $files;
}

/**
 * Load file assiciated with test.
 *
 * @param integer $test_id Test ID.
 * @return array Test information, or FALSE.
 */
function pifr_server_file_get_test($test_id) {
  $result = db_query('SELECT *
                      FROM {pifr_file}
                      WHERE test_id = %d', $test_id);
  return db_fetch_array($result);
}

/**
 * Find a file if it exists.
 *
 * @param array $file File information.
 * @return array Complete file information if found, otherwise FALSE.
 */
function pifr_server_file_find(array $file) {
  $result = db_query("SELECT *
                      FROM {pifr_file}
                      WHERE client_identifier = '%s'
                      AND branch_id = %d", $file['client_identifier'], $file['branch_id']);
  return db_fetch_array($result);
}

/**
 * Update the status of all file tests related to a branch.
 *
 * @param integer $branch_id Branch ID.
 * @param integer $status_from Status to update from.
 * @param integer $status_to Status to change to.
 */
function pifr_server_file_branch_update($branch_id, $status_from, $status_to) {
  db_query('UPDATE {pifr_test}
            SET status = %d
            WHERE test_id IN (
              SELECT test_id
              FROM {pifr_file}
              WHERE branch_id = %d
            )
            AND status = %d', $status_to, $branch_id, $status_from);
}

/**
 * Save file information.
 *
 * @param array $file File information.
 * @param boolean $check Check for existing file and update if found.
 * @return array Updated file information.
 */
function pifr_server_file_save(array $file, $check = TRUE) {
  if ($check && empty($file['file_id'])) {
    // Check if file already exists.
    if ($file_record = pifr_server_file_find($file)) {
      $file['file_id'] = $file_record['file_id'];
      $file['test_id'] = $file_record['test_id'];
    }
  }

  if (!empty($file['file_id'])) {
    // Existing project, update record.
    drupal_write_record('pifr_file', $file, 'file_id');
    pifr_server_test_title_update($file);
  }
  else {
    // Insert test record.
    $test = array(
      'type' => PIFR_SERVER_TEST_TYPE_FILE,
      'status' => PIFR_SERVER_TEST_STATUS_STUB,
      'title' => pifr_server_test_title(PIFR_SERVER_TEST_TYPE_FILE, $file),
      'last_received' => time(),
      'last_requested' => 0,
      'last_tested' => 0,
      'test_count' => 0
    );
    $test = pifr_server_test_save($test);
    $file['test_id'] = $test['test_id'];

    // New project, insert record.
    drupal_write_record('pifr_file', $file);
  }
  return $file;
}

/**
 * Delete a file.
 *
 * @param array $file File information.
 */
function pifr_server_file_delete(array $file) {
  // Delete test and results.
  pifr_server_test_delete($file['test_id']);

  // Delete file record.
  db_query('DELETE FROM {pifr_file} WHERE file_id = %d', $file['file_id']);
}

class PIFRTestFile extends PIFRTest {

  protected $file;

  public function __construct(array $test) {
    parent::__construct($test);
    $this->file = pifr_server_file_get_test($test['test_id']);
  }

  public function getType() {
    return t('File');
  }

  public function getInfo() {
    return $this->file;
  }

  public function getInfoTable() {
    $rows = array();

    $branch = PIFRTest::getInstanceById(PIFR_SERVER_TEST_TYPE_BRANCH, $this->file['branch_id']);

    $rows['file'] = array(t('File'), l($this->file['file_name'], $this->file['file_url']));

    $branch_test = $branch->getTest();
    $rows['branch'] = array(t('Branch'), l($branch_test['title'], 'pifr/test/' . $branch_test['test_id']));

    $link = $this->file['link'];
    $rows['link'] = array(t('Link'), $link ? l(t('Issue Link'), $link) : '<em>' . t('none') . '</em>');

    return $this->formatTable($rows);
  }
}
