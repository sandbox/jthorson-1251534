<?php

/**
 * @file
 * Provide installation related functionality.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Implementation of hook_install().
 */
function pifr_server_install() {
  // Install schema.
  drupal_install_schema('pifr_server');

  // Set chart global background to transparent.
  variable_set('chart_global_bg', 'FFFFFF00');

  // Set tab module effect speed to 'immediate'.
  variable_set('tabs_speed', 1);

  // Ensure that pifr_server is weighted behind views for hook_menu_alter().
  if ($weight = db_result(db_query("SELECT weight FROM {system} WHERE name='%s'", 'views'))) {
    db_query("UPDATE {system} SET weight = %d WHERE name = '%s'", $weight + 1, 'pifr_server');
  }
}

/**
 * Implementation of hook_uninstall().
 */
function pifr_server_uninstall() {
  // Remove tables.
  drupal_uninstall_schema('pifr_server');

  // Remove variables.
  variable_del('pifr_server_report');
  variable_del('pifr_server_client_test_interval');
}

/**
 * Implementation of hook_schema().
 */
function pifr_server_schema() {
  $schema = array();

  $schema['pifr_branch'] = array(
    'description' => t('Store branch information.'),
    'fields' => array(
      'branch_id' => array(
        'description' => t('Unique branch ID.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'project_id' => array(
        'description' => t('Reference to project.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'test_id' => array(
        'description' => t('Reference to test.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'client_identifier' => array(
        'description' => t('Client identifier used to reference the branch.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
      ),
      'vcs_identifier' => array(
        'description' => t('Version control system identifier.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
      ),
      'dependency' => array(
        'description' => t('List of branches the branch is dependent on.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
      ),
      'plugin_argument' => array(
        'description' => t('Serialized plugin arguments.'),
        'type' => 'text',
        'not null' => TRUE,
        'default' => '',
        'serialize' => TRUE,
      ),
      'link' => array(
        'description' => t('Link to relevant information determined by project client.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
      ),
    ),
    'indexes' => array(
      'test_id' => array('test_id'),
      'client_identifier' => array('client_identifier'),
    ),
    'primary key' => array('branch_id'),
  );
  $schema['pifr_client'] = array(
    'description' => t('Store clients: either project or testing.'),
    'fields' => array(
      'client_id' => array(
        'description' => t('Unique client ID.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'client_key' => array(
        'description' => t('Unique client key used for authentication between servers.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 32,
        'default' => '',
      ),
      'type' => array(
        'description' => t('Type of client, PIFR_SERVER_CLIENT_*.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'environment' => array(
        'description' => t('Environment ID(s) which the client can test (does not apply to project client).'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
        'serialize' => TRUE,
      ),
      'uid' => array(
        'description' => t('Reference to user that controls client.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'url' => array(
        'description' => t('Base URL of client.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
      ),
      'status' => array(
        'description' => t('Client status, PIFR_SERVER_CLIENT_STATUS_*.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'public' => array(
        'description' => t('Flag to allow anonymous access to all tests from this client.'),
        'type' => 'int',
        'size' => 'tiny',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'test_id' => array(
        'description' => t('Reference test.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'indexes' => array(
      'client_key' => array('client_key'),
    ),
    'primary key' => array('client_id'),
  );
  $schema['pifr_client_test'] = array(
    'description' => t('Keep track of client confirmation testing stage.'),
    'fields' => array(
      'client_id' => array(
        'description' => t('Reference to client ID.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'environment_id' => array(
        'description' => t('Current environment in which the testing is taking place.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'stage' => array(
        'description' => t('Arbitrary stage in the client confirmation process.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('client_id'),
  );
  $schema['pifr_environment'] = array(
    'description' => t('Store client environment information.'),
    'fields' => array(
      'environment_id' => array(
        'description' => t('Unique environment ID.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'title' => array(
        'description' => t('Environment title.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
      ),
      'description' => array(
        'description' => t('Description of environment.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
      ),
      'client' => array(
        'description' => t('The client ID(s) to restrict environment to.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
        'serialize' => TRUE,
      ),
      'project' => array(
        'description' => t('The project ID(s) to restrict environment to.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
        'serialize' => TRUE,
      ),
      'branch' => array(
        'description' => t('The branch ID(s) to restrict environment to.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
        'serialize' => TRUE,
      ),
      'type' => array(
        'description' => t('The type(es) of tests(s) to restrict environment to.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
        'serialize' => TRUE,
      ),
      'plugin' => array(
        'description' => t('The review plugin that perform the review.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
      ),
      'plugin_argument' => array(
        'description' => t('Serialized plugin arguments.'),
        'type' => 'text',
        'not null' => TRUE,
        'default' => '',
        'serialize' => TRUE,
      ),
    ),
    'primary key' => array('environment_id'),
  );
  $schema['pifr_environment_status'] = array(
    'description' => t('Store test status in relation to each environment.'),
    'fields' => array(
      'test_id' => array(
        'description' => t('Test ID related to status information.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'environment_id' => array(
        'description' => t('Environment ID related to status information.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'client_id' => array(
        'description' => t('Client ID that test was sent to.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'indexes' => array(
      'test_id' => array('test_id'),
      'client_id' => array('client_id'),
    ),
    'primary key' => array('test_id', 'environment_id'),
  );
  $schema['pifr_file'] = array(
    'description' => t('Store file test results.'),
    'fields' => array(
      'file_id' => array(
        'description' => t('Unique file ID.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'branch_id' => array(
        'description' => t('Reference to branch.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'test_id' => array(
        'description' => t('Reference to test.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'client_identifier' => array(
        'description' => t('Client identifier used to reference the file.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'file_url' => array(
        'description' => t('Absolute URL to file.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
      ),
      'file_name' => array(
        'description' => t('Name of file.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
      ),
      'link' => array(
        'description' => t('Link to relevant information determined by project client.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
      ),
    ),
    'indexes' => array(
      'test_id' => array('test_id'),
      'client_identifier' => array('client_identifier'),
    ),
    'primary key' => array('file_id'),
  );
  $schema['pifr_log'] = array(
    'description' => t('Stores log of all events that take place in a test cycle.'),
    'fields' => array(
      'log_id' => array(
        'description' => t('Unique log ID.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'test_id' => array(
        'description' => t('Reference to test event occured to.'),
        'type' => 'int',
        'not null' => TRUE,
      ),
      'client_id' => array(
        'description' => t('Reference to client (if any) that was involved in the event.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'code' => array(
        'description' => t('Event code, PIFR_SERVER_LOG_*.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'timestamp' => array(
        'description' => t('Timestamp when the event occured.'),
        'type' => 'int',
        'unsigned' => FALSE,
        'not null' => TRUE,
      ),
    ),
    'indexes' => array(
      'test_id' => array('test_id'),
      'client_id' => array('client_id'),
    ),
    'primary key' => array('log_id'),
  );
  $schema['pifr_project'] = array(
    'description' => t('Store project information.'),
    'fields' => array(
      'project_id' => array(
        'description' => t('Unique project ID.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'client_id' => array(
        'description' => t('Reference to client.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'client_identifier' => array(
        'description' => t('Client identifier used to reference the project.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
      ),
      'name' => array(
        'description' => t('Name of project.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
      ),
      'repository_type' => array(
        'description' => t('Repository type.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 32,
        'default' => '',
      ),
      'repository_url' => array(
        'description' => t('Repository URL, git://git.drupal.org/project/drupal.git'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
      ),
      'link' => array(
        'description' => t('Link to relevant information determined by project client.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
      ),
    ),
    'indexes' => array(
      'client_identifier' => array('client_identifier'),
    ),
    'primary key' => array('project_id'),
  );
  $schema['pifr_result'] = array(
    'description' => t('Stores summarized result of testing for each type.'),
    'fields' => array(
      'result_id' => array(
        'description' => t('Unique result ID.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'test_id' => array(
        'description' => t('Test ID results belong to.'),
        'type' => 'int',
        'not null' => TRUE,
      ),
      'environment_id' => array(
        'description' => t('Environment ID (from pifr_environment table) for which the result was obtained in.'),
        'type' => 'int',
        'not null' => TRUE,
      ),
      'code' => array(
        'description' => t('Result code PIFR_SERVER_CLIENT_RESULT_*.'),
        'type' => 'int',
        'not null' => TRUE,
      ),
      'details' => array(
        'description' => t('Serialized array of details (parameters for t()).'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
        'serialize' => TRUE,
      ),
      'log' => array(
        'description' => t('Contents of log file from test client.'),
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'medium',
        'default' => '',
      ),
    ),
    'indexes' => array(
      'test_id' => array('test_id'),
    ),
    'primary key' => array('result_id'),
  );
  $schema['pifr_test'] = array(
    'description' => t('Store base test record.'),
    'fields' => array(
      'test_id' => array(
        'description' => t('Unique test ID.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'type' => array(
        'description' => t('Type of test, PIFR_SERVER_TEST_TYPE_*.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'status' => array(
        'description' => t('Status of test, PIFR_SERVER_TEST_STATUS_*.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'title' => array(
        'description' => t('Title representing the test.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 256,
        'default' => '',
      ),
      'last_received' => array(
        'description' => t('Timestamp when test request was last received.'),
        'type' => 'int',
        'unsigned' => FALSE,
        'not null' => TRUE,
      ),
      'last_requested' => array(
        'description' => t('Timestamp when test was last requested by client.'),
        'type' => 'int',
        'unsigned' => FALSE,
        'not null' => TRUE,
      ),
      'last_tested' => array(
        'description' => t('Timestamp when test last finished.'),
        'type' => 'int',
        'unsigned' => FALSE,
        'not null' => TRUE,
      ),
      'test_count' => array(
        'description' => t('Total number of times the test has been performed.'),
        'type' => 'int',
        'unsigned' => FALSE,
        'not null' => TRUE,
      ),
    ),
    'indexes' => array(
      'status' => array('status'),
    ),
    'primary key' => array('test_id'),
  );
  $schema['pifr_test_environment'] = array(
    'description' => t('Store base test record.'),
    'fields' => array(
      'test_id' => array(
        'description' => t('Reference to test.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'environment_id' => array(
        'description' => t('Reference to environment.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('test_id', 'environment_id'),
  );

  return $schema;
}

/**
 * Remove 'require_pass' field from 'pifr_environment'.
 */
function pifr_server_update_6200() {
  $ret = array();

  db_drop_field($ret, 'pifr_environment', 'require_pass');

  return $ret;
}

/**
 * Remove variable 'pifr_server_client_project_interval'.
 */
function pifr_server_update_6201() {
  $ret = array();

  variable_del('pifr_server_client_project_interval');

  return $ret;
}

/**
 * Create 'pifr_test_environment' table.
 */
function pifr_server_update_6202() {
  $ret = array();

  $table = array(
    'description' => t('Store base test record.'),
    'fields' => array(
      'test_id' => array(
        'description' => t('Reference to test.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'environment_id' => array(
        'description' => t('Reference to environment.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('test_id', 'environment_id'),
  );
  db_create_table($ret, 'pifr_test_environment', $table);

  return $ret;
}

/**
 * Insert 'pifr_test_environment' records for all existing test data.
 */
function pifr_server_update_6203(&$sandbox = NULL) {
  $ret = array();

  if (!isset($sandbox['progress'])) {
    $sandbox['progress'] = 0;
    $sandbox['current_test_id'] = 0;
    $sandbox['max'] = db_result(db_query('SELECT COUNT(test_id) FROM {pifr_test}'));
  }

  $result = db_query_range('SELECT *
                            FROM {pifr_test}
                            WHERE test_id > %d
                            ORDER BY test_id ASC', $sandbox['current_test_id'], 0, 2500);
  while ($test = db_fetch_array($result)) {
    // Evaluate environment coditions and save relevant IDs.
    pifr_server_test_environment_save($test);

    $sandbox['progress']++;
    $sandbox['current_test_id'] = $test['test_id'];
  }

  $ret['#finished'] = empty($sandbox['max']) ? 1 : ($sandbox['progress'] / $sandbox['max']);

  return $ret;
}

/**
 * Update role permissions, using map.
 */
function pifr_server_update_6204() {
  $ret = array();

  $map = array(
    'pifr add project client' => 'add pifr project client',
    'pifr add test client' => 'add pifr test client',
    'pifr manage own client' => 'manage own pifr client',
    'pifr manage clients' => 'manage pifr clients',
    'pifr manage environments' => 'manage pifr environments',
    'pifr manage tests' => 'manage pifr tests',
  );
  foreach ($map as $old => $new) {
    update_sql("UPDATE permission SET perm = REPLACE(perm, '$old', '$new')");
  }

  return $ret;
}

/**
 * Change 'plugin_argument' column to type of 'text'.
 */
function pifr_server_update_6205() {
  $ret = array();

  $spec = array(
    'description' => t('Serialized plugin arguments.'),
    'type' => 'text',
    'not null' => TRUE,
    'default' => '',
    'serialize' => TRUE,
  );
  db_change_field($ret, 'pifr_branch', 'plugin_argument', 'plugin_argument', $spec);
  db_change_field($ret, 'pifr_environment', 'plugin_argument', 'plugin_argument', $spec);

  return $ret;
}

/**
 * Update plugin argument keys.
 */
function pifr_server_update_6206() {
  $result = db_query('SELECT branch_id FROM {pifr_branch}');
  while ($branch_id = db_result($result)) {
    $branch = pifr_server_branch_get($branch_id);
    $branch['plugin_argument'] = pifr_server_update_6206_map($branch['plugin_argument']);
    pifr_server_branch_save($branch, FALSE);
  }

  $result = db_query('SELECT environment_id FROM {pifr_environment}');
  while ($environment_id = db_result($result)) {
    $environment = pifr_server_environment_get($environment_id);
    $environment['plugin_argument'] = pifr_server_update_6206_map($environment['plugin_argument']);
    pifr_server_environment_save($environment, FALSE);
  }

  return array();
}

/**
 * Map old plugin argument keys to new keys.
 *
 * @param array $array Old plugin argument array.
 * @return array Updated plugin argument array.
 */
function pifr_server_update_6206_map(array $array) {
  static $map = array(
    'directory' => 'test.directory',
    'files' => 'test.files',
    'core' => 'drupal.core.version',
    'core-url' => 'drupal.core.url',
    'modules' => 'drupal.modules',
    'pifr_coder.annotate' => 'coder.annotate',
    'tests' => 'simpletest.tests',
    'clone-db' => 'simpletest.db',
    'clone-db-user' => 'drupal.user',
    'clone-db-pass' => 'drupal.pass',
  );

  $new = array();
  foreach ($array as $key => $value) {
    if (isset($map[$key])) {
      $new[$map[$key]] = $value;
    }
    else {
      $new[$key] = $value;
    }
  }
  return $new;
}

/**
 * Add public field to pifr_client table.
 */
function pifr_server_update_6207() {
  $ret = array();

  $spec = array(
    'description' => t('Flag to allow anonymous access to all tests from this client.'),
    'type' => 'int',
    'size' => 'tiny',
    'unsigned' => TRUE,
    'not null' => TRUE,
  );
  db_add_field($ret, 'pifr_client', 'public', $spec);

  return $ret;
}

/**
 * Ensure that pifr_server is weighted behind views for hook_menu_alter().
 */
function pifr_server_update_6208() {
  if ($weight = db_result(db_query("SELECT weight FROM {system} WHERE name='%s'", 'views'))) {
    db_query("UPDATE {system} SET weight = %d WHERE name = '%s'", $weight + 1, 'pifr_server');
  }

  return array();
}

/**
 * Set all existing project clients to allow public viewing.
 */
function pifr_server_update_6209() {
  db_query('UPDATE {pifr_client} SET public = %d WHERE type = %d', TRUE, PIFR_SERVER_CLIENT_TYPE_PROJECT);

  return array();
}

/**
 * Update plugin argument 'test.directory.review'.
 */
function pifr_server_update_6210() {
  $result = db_query('SELECT branch_id FROM {pifr_branch}');
  while ($branch_id = db_result($result)) {
    $branch = pifr_server_branch_get($branch_id);
    if (!empty($branch['plugin_argument']['test.directory.review'])) {
      $branch['plugin_argument']['test.directory.review'] = array($branch['plugin_argument']['test.directory.review']);
      pifr_server_branch_save($branch, FALSE);
    }
  }

  $result = db_query('SELECT environment_id FROM {pifr_environment}');
  while ($environment_id = db_result($result)) {
    $environment = pifr_server_environment_get($environment_id);
    if (!empty($environment['plugin_argument']['test.directory.review'])) {
      $environment['plugin_argument']['test.directory.review'] = array($environment['plugin_argument']['test.directory.review']);
      pifr_server_environment_save($environment, FALSE);
    }
  }

  return array();
}
