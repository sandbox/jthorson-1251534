<?php

/**
 * @file
 * Provide statistic functions.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Determine the number of active tests.
 *
 * @return integer Number of active tests.
 */
function pifr_server_statistic_active_count() {
  static $active_count;

  if (!isset($active_count)) {
    $result = db_query('SELECT COUNT(test_id)
                        FROM (
                          SELECT s.test_id
                          FROM {pifr_test} t
                          JOIN {pifr_environment_status} s
                            ON s.test_id = t.test_id
                          WHERE t.type != %d
                          GROUP BY test_id
                        ) x', PIFR_SERVER_TEST_TYPE_CLIENT);
    $active_count = db_result($result);
  }
  return $active_count;
}

/**
 * Determine the number of queued tests.
 *
 * @return integer Number of queued tests.
 */
function pifr_server_statistic_queue_count() {
  static $queue_count;

  if (!isset($queue_count)) {
    $result = db_query('SELECT COUNT(test_id)
                        FROM {pifr_test}
                        WHERE type != %d
                        AND status = %d', PIFR_SERVER_TEST_TYPE_CLIENT, PIFR_SERVER_TEST_STATUS_QUEUED);
    $queue_count = db_result($result);
  }
  return $queue_count;
}

/**
 * Determine the number of enabled test clients.
 *
 * @return integer Number of enabled test clients.
 */
function pifr_server_statistic_enabled_count() {
  static $enabled_count;

  if (!isset($enabled_count)) {
    $clients = pifr_server_client_get_type(PIFR_SERVER_CLIENT_TYPE_TEST);
    $enabled_count = 0;
    foreach ($clients as $client) {
      if ($client['status'] == PIFR_SERVER_CLIENT_STATUS_ENABLED) {
        $enabled_count++;
      }
    }
  }
  return $enabled_count;
}

/**
 * Generate a number of general statistics.
 *
 * @return array Associative array of general statistics with the following
 *   keys: test_count, review_count, review_average.
 */
function pifr_server_statistic_general() {
  $general = array(
    'test_count' => 'SELECT COUNT(test_id) FROM {pifr_test} WHERE test_count > 0 AND type != %d',
    'review_count' => 'SELECT SUM(test_count) FROM {pifr_test} WHERE type != %d',
    'review_average' => 'SELECT AVG(test_count) FROM {pifr_test} WHERE type != %d',
  );

  // Execute each query to determine the value of the statistic.
  foreach ($general as $key => $query) {
    $general[$key] = db_result(db_query($query, PIFR_SERVER_TEST_TYPE_CLIENT));
  }
  return $general;
}

/**
 * Determine the result breakdown.
 *
 * @param integer $environment_id (Optional) Environment ID.
 * @return array If an environment is specified reakdown information in the
 *   form $code => $count, otherwise wrapped by an associative array of
 *   environemnt IDs.
 */
function pifr_server_statistic_breakdown($environment_id = NULL) {
  $sql = 'SELECT r.environment_id, r.code, COUNT(r.result_id) AS count
          FROM {pifr_result} r
          JOIN {pifr_test} t
            ON r.test_id = t.test_id
          WHERE t.type != %d
          ' . ($environment_id ? 'AND r.environment_id = %d' : '') . '
          GROUP BY r.environment_id, r.code
          ORDER BY count';
  $results = db_query($sql, PIFR_SERVER_TEST_TYPE_CLIENT, $environment_id);

  $breakdown = array();
  while ($result = db_fetch_array($results)) {
    if (!isset($breakdown[$result['environment_id']])) {
      $breakdown[$result['environment_id']] = array();
    }

    $breakdown[$result['environment_id']][$result['code']] = $result['count'];
  }

  if ($environment_id) {
    return isset($breakdown[$environment_id]) ? $breakdown[$environment_id] : array();
  }
  return $breakdown;
}
