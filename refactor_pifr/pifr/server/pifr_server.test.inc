<?php

/**
 * @file
 * Provide test functions.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/*
 * Test type codes.
 */
define('PIFR_SERVER_TEST_TYPE_CLIENT', 1);
define('PIFR_SERVER_TEST_TYPE_BRANCH', 2);
define('PIFR_SERVER_TEST_TYPE_FILE', 3);

/*
 * Test status codes.
 */
define('PIFR_SERVER_TEST_STATUS_STUB', 1);
define('PIFR_SERVER_TEST_STATUS_QUEUED', 2);
define('PIFR_SERVER_TEST_STATUS_POSTPONED', 3);
define('PIFR_SERVER_TEST_STATUS_RESULT', 4);

/**
 * Access check for viewing test information.
 *
 * @param integer $test_id Test ID.
 * @param object $account (Optional) The account to check, if not given use
 *   currently logged in user.
 * @return boolean TRUE if user has access, FALSE if not.
 */
function pifr_server_test_access($test_id, $account = NULL) {
  // If user has access to all test then skip rest.
  if (user_access('view all pifr tests', $account)) {
    return TRUE;
  }

  // Only check permissions for valid test.
  if ($test = pifr_server_test_get($test_id)) {
    // Determine the user to evaluate against.
    if (!$account) {
      $account = $GLOBALS['user'];
    }

    // Determine the project client related to the test.
    if ($test['type'] == PIFR_SERVER_TEST_TYPE_CLIENT) {
      // If user has access to all client tests then skip rest.
      if (user_access('view all pifr client tests', $account)) {
        return TRUE;
      }

      $client = pifr_server_client_get_test($test['test_id']);
      $client_id = $client['client_id'];
    }
    else {
      if ($test['type'] == PIFR_SERVER_TEST_TYPE_BRANCH) {
        $branch = pifr_server_branch_get_test($test['test_id']);
      }
      else {
        $file = pifr_server_file_get_test($test['test_id']);
        $branch = pifr_server_branch_get($file['branch_id']);
      }

      // Branch of file test grant permission based on their project client.
      $project = pifr_server_project_get($branch['project_id']);
      $client_id = $project['client_id'];
    }

    // Allow if client has public viewing enabled or user owns client.
    return pifr_server_client_access($client_id, $account);
  }

  // Invalid test so allow user to see error page.
  return TRUE;
}

/**
 * Load test.
 *
 * @param integer $test_id Test ID.
 * @return array Test information, or FALSE.
 */
function pifr_server_test_get($test_id) {
  $result = db_query('SELECT *
                      FROM {pifr_test}
                      WHERE test_id = %d', $test_id);
  return db_fetch_array($result);
}

/**
 * Load the current test being reviewed by a client.
 *
 * @param array $client Client information.
 * @return mixed Test information, or FALSE.
 */
function pifr_server_test_get_client(array $client) {
  $result = db_query('SELECT t.*
                      FROM {pifr_test} t
                      JOIN {pifr_environment_status} s
                        ON t.test_id = s.test_id
                      WHERE s.client_id = %d', $client['client_id']);
  return db_fetch_array($result);
}

/**
 * Get the next eligible test for a client.
 *
 * @param array $client Client information.
 * @return array Test information, or an empty array.
 */
function pifr_server_test_get_next(array $client) {
  // Check if client tests need to be re-run.
  $client = pifr_server_client_confirmation_necessary($client);

  if ($client['status'] == PIFR_SERVER_CLIENT_STATUS_TESTING) {
    // Return the next stage of the client testing process.
    $test = pifr_server_test_get($client['test_id']);
    list($next, $environment) = pifr_server_client_confirmation_next($client);
    pifr_server_test_sent($test, $client, $environment['environment_id']);
    return $next;
  }
  else if ($client['status'] == PIFR_SERVER_CLIENT_STATUS_ENABLED) {
    // If the client currently has a test out, reset the test.
    if ($test = pifr_server_test_get_client($client)) {
      pifr_server_test_reset($test, $client);
    }

    // Return the next eligible branch or file test.
    if ($next = pifr_server_test_get_next_eligible($client)) {
      pifr_debug('Next eligible=%next', array('%next' => print_r($next, TRUE)));
      $test = $next[0];
      $environment = $next[1];
      pifr_server_test_sent($test, $client, $environment['environment_id']);

      // Prepare test XML-RPC response.
      return pifr_server_test_xmlrpc($test, $environment);
    }
  }

  // No eligible tests for the client at this time.
  return array();
}

/**
 * Get the next eligible branch or file test.
 *
 * @param array $client Client information.
 * @return array Test information and environment or FALSE.
 */
function pifr_server_test_get_next_eligible(array $client) {
  $result = db_query('SELECT z.*
                      FROM
                      (
                        SELECT t.test_id, e.environment_id
                        FROM {pifr_test} t
                        JOIN {pifr_test_environment} te
                          ON t.test_id = te.test_id
                        JOIN {pifr_environment} e
                          ON (e.environment_id = te.environment_id AND e.environment_id IN (' . db_placeholders($client['environment'], 'int') . '))
                        WHERE t.type != %d
                        AND t.status = %d
                        ORDER BY t.type, t.test_count, t.last_tested, t.test_id
                      ) z
                      LEFT JOIN {pifr_environment_status} s
                        ON (z.test_id = s.test_id AND z.environment_id = s.environment_id)
                      LEFT JOIN {pifr_result} r
                        ON (z.test_id = r.test_id AND z.environment_id = r.environment_id)
                      WHERE s.client_id IS NULL
                      AND r.result_id IS NULL
                      LIMIT 1',
    array_merge($client['environment'], array(PIFR_SERVER_TEST_TYPE_CLIENT, PIFR_SERVER_TEST_STATUS_QUEUED)));

  // If result found then load test and environment.
  if ($info = db_fetch_array($result)) {
    $test = pifr_server_test_get($info['test_id']);
    $environment = pifr_server_environment_get($info['environment_id']);
    return array($test, $environment);
  }
  return FALSE;
}

/**
 * Load tests that have been tested since the timestamp.
 *
 * @param integer $timestamp Local timestamp.
 * @param integer $max Maximum number of tests to return.
 * @return array List of tests.
 */
function pifr_server_test_get_since($timestamp, $max) {
  $result = db_query('SELECT *
                      FROM {pifr_test}
                      WHERE status = %d
                      AND last_tested > %d
                      AND type != %d
                      ORDER BY last_tested
                      LIMIT %d', PIFR_SERVER_TEST_STATUS_RESULT, $timestamp, PIFR_SERVER_TEST_TYPE_CLIENT, $max);
  $tests = array();
  while ($test = db_fetch_array($result)) {
    $tests[] = $test;
  }
  return $tests;
}

/**
 * Build XML-RPC reponse for pifr.next() containing test definition.
 *
 * @param array $test Test information.
 * @param array $environment Environment information.
 * @return array Test definition as defined by pifr_server_xmlrpc_next().
 * @see pifr_server_xmlrpc_next()
 */
function pifr_server_test_xmlrpc(array $test, array $environment) {
  // Initiate reponse with test ID.
  $response = array(
    'test_id' => (int) $test['test_id'],
    'review' => array(
      'plugin' => $environment['plugin'],
      'argument' => array(),
    ),
    'vcs' => array(
      'main' => array(),
      'dependencies' => array(),
    ),
    'files' => array(),
  );

  // Load related branch and add test type specific information.
  if ($test['type'] == PIFR_SERVER_TEST_TYPE_FILE) {
    // File test, load file information and related branch.
    $file = pifr_server_file_get_test($test['test_id']);
    $response['files'][] = $file['file_url'];

    $branch = pifr_server_branch_get($file['branch_id']);
  }
  else {
    // Branch test, load related branch.
    $branch = pifr_server_branch_get_test($test['test_id']);
  }

  // Get the project related to the branch.
  $project = pifr_server_project_get($branch['project_id']);

  // Check if the branch has dependencies.
  $dependency_argument = array();
  if ($branch['dependency']) {
    $dependencies = explode(',', $branch['dependency']);
    foreach ($dependencies as $dependency) {
      if ($dependency_branch = pifr_server_branch_get($dependency)) {
        $dependency_project = pifr_server_project_get($dependency_branch['project_id']);

        $response['vcs']['dependencies'][] = array(
          'repository' => array(
            'type' => $dependency_project['repository_type'],
            'url' => $dependency_project['repository_url'],
          ),
          'vcs_identifier' => $dependency_branch['vcs_identifier'],
        );

        $dependency_argument[] = $dependency_branch['plugin_argument'];
      }
    }
  }

  // Get the plugin arguments.
  $response['review']['argument'] = pifr_server_review_argument_get($environment, $dependency_argument, $branch);

  // Add core branch information to response.
  $response['vcs']['main'] = array(
    'repository' => array(
      'type' => $project['repository_type'],
      'url' => $project['repository_url'],
    ),
    'vcs_identifier' => $branch['vcs_identifier'],
  );
  return $response;
}

/**
 * Queue test for testing.
 *
 * Update test status and last received, clear previous result, and clear
 * environment status.
 *
 * @param array $test Test information.
 */
function pifr_server_test_queue(array $test) {
  // Update test status and last received timestamp.
  $test['status'] = PIFR_SERVER_TEST_STATUS_QUEUED;
  $test['last_received'] = time();
  pifr_server_test_save($test);

  // Clear the previous results for the test.
  pifr_server_result_clear($test['test_id']);

  // Reset the environment status so that all environments will be tested.
  pifr_server_environment_status_clear_all($test['test_id']);
}

/**
 * Perform test sent event operations.
 *
 * @param array $test Test information.
 * @param array $client Client information.
 * @param interger $environment_id Environment ID.
 */
function pifr_server_test_sent(array $test, array $client, $environment_id) {
  // Reserver the client's environment for the test.
  pifr_server_environment_status_reserve($environment_id, $test['test_id'], $client['client_id']);

  // Update the last requested timestamp.
  $test['last_requested'] = time();
  pifr_server_test_save($test);

  // Log the event.
  pifr_server_log($test['test_id'], $client['client_id'], PIFR_SERVER_LOG_CLIENT_REQUEST);
}

/**
 * Perform test reset event operations.
 *
 * @param array $test Test information.
 * @param array $client Client information.
 */
function pifr_server_test_reset(array $test, array $client) {
  pifr_server_test_queue($test);

  // Log the event.
  pifr_server_log($test['test_id'], $client['client_id'], PIFR_SERVER_LOG_CLIENT_RESET);
}

/**
 * Record the result of a test.
 *
 * @param array $test Test information.
 * @param array $result Result information, see pifr_server_xmlrpc_result().
 * @param array $client Client information.
 * @see pifr_server_xmlrpc_result()
 */
function pifr_server_test_result(array $test, array $result, array $client) {
  $results = pifr_server_result_get_all($test['test_id']);
  $environments = pifr_server_environment_test_get_all($test['test_id']);

  // Determine the environment ID that the client is supposed to be testing.
  $result['environment_id'] = pifr_server_environment_status_get_client($test, $client);
  $results[$result['environment_id']] = $result;

  // If test is currently marked as queued then evaluate the result to
  // determine what the status of the tests should be set to.
  if ($test['status'] == PIFR_SERVER_TEST_STATUS_QUEUED) {
    // If this result constitutes the final result (all environments reviewed)
    // then mark the test as complete.
    if (count($results) == count($environments)) {
      $test['status'] = PIFR_SERVER_TEST_STATUS_RESULT;
    }
    else {
      // Get a list of all the plugins being used by the environments.
      $plugins = pifr_server_environment_plugins($test);

      // Determine the number of "complete" plugins. Complete is defined by a
      // plugin that has had all of its environments tested or has an
      // environment that has failed testing.
      $complete = 0;
      foreach ($plugins as $plugin) {
        $all = TRUE;
        $fail = FALSE;
        foreach ($environments as $environment) {
          // Process the environment if it uses the plugin in question.
          if ($environment['plugin'] == $plugin) {
            // If the environment has a result then check to see if it failed,
            // otherwise the plugin does not have all environments complete.
            if (isset($results[$environment['environment_id']])) {
              if (!pifr_server_review_check($result)) {
                $fail = TRUE;
                break;
              }
            }
            else {
              $all = FALSE;
            }
          }
        }

        // Check the two decisive flags for the plugin.
        if ($all || $fail) {
          $complete++;
        }
      }

      // If all plugins are complete then the test is complete.
      if ($complete == count($plugins)) {
        $test['status'] = PIFR_SERVER_TEST_STATUS_RESULT;
      }
    }
  }

  $test['last_tested'] = time();
  $test['test_count']++;
  pifr_server_test_save($test);

  // Since the client has returned the result, clear its environment status record.
  pifr_server_environment_status_clear_client($client['client_id']);

  // Save the prepared result.
  $result = pifr_server_result_save($result);

  // Log the test result event.
  pifr_server_log($test['test_id'], $client['client_id'], PIFR_SERVER_LOG_CLIENT_RESULT);

  // If client is undergoing testing then allow the results to be checked based
  // on the client confirmation testing stage. If testing is incomplete then
  // the test may be re-enabled.
  if ($test['type'] == PIFR_SERVER_TEST_TYPE_CLIENT) {
    pifr_server_client_confirmation_check($client, $result);

    // Reload data to ensure that any changes made above are included.
    $test = pifr_server_test_get($test['test_id']);
    $client = pifr_server_client_get($client['client_id']);

    // Invoke hook_pifr_server_client_result().
    module_invoke_all('pifr_server_client_result', $test, $result, $client);
  }
  elseif ($test['type'] == PIFR_SERVER_TEST_TYPE_BRANCH) {
    // Branch result may effect related files.
    $branch = pifr_server_branch_get_test($test['test_id']);
    if (pifr_server_review_check($result)) {
      // Queue all tests that have been postponed.
      pifr_server_file_branch_update($branch['branch_id'], PIFR_SERVER_TEST_STATUS_POSTPONED, PIFR_SERVER_TEST_STATUS_QUEUED);
    }
    else {
      // Postpone all tests that have been queued.
      pifr_server_file_branch_update($branch['branch_id'], PIFR_SERVER_TEST_STATUS_QUEUED, PIFR_SERVER_TEST_STATUS_POSTPONED);
    }
  }

  // Invoke hook_pifr_server_result().
  module_invoke_all('pifr_server_result', $test, $result);
}

/**
 * Save test information.
 *
 * @param array $test Test information.
 * @return array Updated test information.
 */
function pifr_server_test_save(array $test) {
  if (isset($test['test_id'])) {
    // Existing test, update record.
    drupal_write_record('pifr_test', $test, 'test_id');
  }
  else {
    // New test, insert record.
    drupal_write_record('pifr_test', $test);
  }
  pifr_server_test_environment_save($test);
  return $test;
}

/**
 * Evaluate and store the relevant environments for a test.
 *
 * @param array $test Test information.
 */
function pifr_server_test_environment_save(array $test) {
  db_query('DELETE FROM {pifr_test_environment} WHERE test_id = %d', $test['test_id']);

  $relevant = pifr_server_environment_test_evaluate($test);
  foreach ($relevant as $environment) {
    $relation = array(
      'test_id' => $test['test_id'],
      'environment_id' => $environment['environment_id'],
    );
    drupal_write_record('pifr_test_environment', $relation);
  }
}

/**
 * Delete a test and its results.
 *
 * @param integer $test_id Test ID.
 */
function pifr_server_test_delete($test_id) {
  // Delete all test results.
  $results = pifr_server_result_get_all($test_id);
  foreach ($results as $result) {
    pifr_server_result_delete($result);
  }

  // Delete all log entries associated with the test.
  pifr_server_log_delete($test_id);

  // Delete test record.
  db_query('DELETE FROM {pifr_test} WHERE test_id = %d', $test_id);
}

/**
 * View a test result.
 *
 * @param integer $test_id Test ID.
 * @return string HTML output.
 */
function pifr_server_test_view($test_id) {
  // Determine if the specified test ID is a valid test.
  if ($test = pifr_server_test_get($test_id)) {
    // Include style sheets and javascript.
    drupal_add_css(drupal_get_path('module', 'pifr') . '/pifr.css');
    drupal_add_css(drupal_get_path('module', 'pifr_server') . '/pifr_server.css');
    drupal_add_js(drupal_get_path('module', 'pifr_server') . '/pifr_server.js');

    // Set page title to test title.
    drupal_set_title($test['title']);

    // Load the object that will generate the proper test information.
    $object = PIFRTest::getInstanceByTest($test_id);

    // Use a renderable form array to generate page.
    $form = array();

    // Always provide general information.
    pifr_server_test_view_general($form, $test, $object);

    // Only display result related information if the test has results.
    if ($test['status'] == PIFR_SERVER_TEST_STATUS_RESULT) {
      pifr_server_test_view_result($form, $test);
    }

    // Allow modification of the test view form array.
    drupal_alter('pifr_server_test_view', $form, $test);

    return drupal_render($form);
  }

  drupal_set_message(t('Invalid test.'), 'error');
  return '';
}

/**
 * Generate general test information.
 *
 * @param array $form Reference to test view form.
 * @param array $test Test information.
 * @param object $object Specific test type object.
 */
function pifr_server_test_view_general(array &$form, array $test, $object) {
  $form['status'] = array(
    '#type' => 'fieldset',
    '#title' => t('Test status'),
    '#attributes' => array('id' => 'pifr-status', 'class' => 'pifr-test'),
    '#weight' => -10,
  );
  $form['status']['table'] = array(
    '#type' => 'markup',
    '#value' => theme('table', array(), $object->getTestTable()),
  );

  $form['environment'] = array(
    '#type' => 'fieldset',
    '#title' => t('Environment status'),
    '#attributes' => array('id' => 'pifr-environment', 'class' => 'pifr-test'),
    '#weight' => -9,
  );
  $rows = $object->getEnvironmentTable();
  $form['environment']['table'] = array(
    '#type' => 'markup',
    '#value' => $rows ? theme('table', array(), $rows) : '<em>' . t('No applicable environments.') . '</em>',
  );

  $form['info'] = array(
    '#type' => 'fieldset',
    '#title' => t('@type information', array('@type' => $object->getType())),
    '#attributes' => array('id' => 'pifr-info', 'class' => 'pifr-test'),
    '#weight' => -8,
  );
  $form['info']['table'] = array(
    '#type' => 'markup',
    '#value' => theme('table', array(), $object->getInfoTable()),
  );

  // If no test result then show log expanded.
  $form['log'] = array(
    '#type' => 'fieldset',
    '#title' => t('Event log'),
    '#attributes' => array('id' => 'pifr-log'),
    '#collapsible' => TRUE,
    '#collapsed' => $test['status'] == PIFR_SERVER_TEST_STATUS_RESULT,
    '#weight' => -7,
  );
  $form['log']['table'] = array(
    '#type' => 'markup',
    '#value' => l(t('View entire log'), 'pifr/log/' . $test['test_id']) .
      views_embed_view('pifr_server_log', 'page_4', $test['test_id']),
  );

  // If test type is a client and the client test has a result then display a
  // log of client review activity.
  if ($test['type'] == PIFR_SERVER_TEST_TYPE_CLIENT && $test['status'] == PIFR_SERVER_TEST_STATUS_RESULT) {
    $client = pifr_server_client_get_test($test['test_id']);

    $form['client_log'] = array(
      '#type' => 'fieldset',
      '#title' => t('Client log'),
      '#attributes' => array('id' => 'pifr-log'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => -6.5,
    );
    $form['client_log']['table'] = array(
      '#type' => 'markup',
      '#value' => l(t('View entire log'), 'pifr/log/client/' . $client['client_id']) .
        views_embed_view('pifr_server_log', 'page_6', $client['client_id']),
    );
  }
}

/**
 * Generate test result information.
 *
 * @param array $form Reference to test view form.
 * @param array $test Test information.
 */
function pifr_server_test_view_result(array &$form, array $test) {
  $pass = pifr_server_result_summary_status($test['test_id']);

  $form['summary'] = array(
    '#type' => 'fieldset',
    '#title' => t('Summary'),
    '#attributes' => array('id' => 'pifr-result-summary', 'class' => ($pass ? 'pifr-ok' : 'pifr-error')),
    '#weight' => -6,
    '#value' => '<span id="pifr-result-summary-message">' . pifr_server_result_summary_message($test['test_id']) . '</span>',
  );

  $form['detail'] = array(
    '#type' => 'fieldset',
    '#title' => t('Details'),
    '#weight' => -5,
  );
  $form['detail']['environment'] = array(
    '#type' => 'tabset',
    '#attributes' => array('id' => 'pifr-result-details'),
    '#weight' => -5,
  );

  // Generate the results for each environment.
  $environments = pifr_server_environment_test_get_all($test['test_id']);
  $results = pifr_server_result_get_all($test['test_id']);
  foreach ($environments as $environment) {
    $form['detail']['environment'][$environment['environment_id']] = array(
      '#type' => 'tabpage',
      '#title' => $environment['title'],
    );
    $tab = &$form['detail']['environment'][$environment['environment_id']];

    // Only generate result details if there is a result for this environment.
    if (!empty($results[$environment['environment_id']])) {
      $result = $results[$environment['environment_id']];

      // Add the review log in a fieldset at the end of the results. If the
      // result is not a pass and there is no data to display then default to
      // showing the review log.
      $review_log = array(
        '#type' => 'fieldset',
        '#title' => t('Review log'),
        '#collapsible' => TRUE,
        '#collapsed' => !(!pifr_server_review_check($result) && empty($result['data'])),
        '#value' => $result['log'] ? '<pre>' . $result['log'] . '</pre>' : '<em>' . t('No log available.') . '</em>',
      );
      $tab['#content'] = pifr_server_review_detail($test, $result) . drupal_render($review_log);
    }
    else {
      $tab['#content'] = '<em>' . t('No results for this environment.') . '</em>';
    }
  }
}

/**
 * Test tools form.
 */
function pifr_server_test_tools_form(&$form_state, $test_id) {
  // Determine if the specified test ID is a valid test.
  if ($test = pifr_server_test_get($test_id)) {
    $form = array();

    $form['#redirect'] = 'pifr/test/' . $test_id;

    $form['test_id'] = array(
      '#type' => 'value',
      '#value' => $test_id,
    );

    $form['tools'] = array(
      '#type' => 'tabset',
    );

    $form['tools']['retest'] = array(
      '#type' => 'tabpage',
      '#title' => t('Re-test'),
    );
    $form['tools']['retest']['fieldset'] = array(
      '#type' => 'fieldset',
      '#description' => t('Manually request the test to be retested.'),
    );
    $form['tools']['retest']['fieldset']['op'] = array(
      '#type' => 'submit',
      '#value' => t('Re-test'),
    );

    $form['tools']['xmlrpc'] = array(
      '#type' => 'tabpage',
      '#title' => t('XML-RPC'),
    );
    $form['tools']['xmlrpc']['fieldset'] = array(
      '#type' => 'fieldset',
      '#description' => t('Preview the XML-RPC information to be sent to test client.'),
    );

    // Cannot preview client test information.
    if ($test['type'] != PIFR_SERVER_TEST_TYPE_CLIENT) {
      if ($environments = pifr_server_environment_test_get_all($test['test_id'])) {
        $form['tools']['xmlrpc']['fieldset']['environment'] = array(
          '#type' => 'tabset',
        );

        // Generate the previews for each environment.
        foreach ($environments as $environment) {
          // Export XML-RPC information and format for display.
          $export = var_export(pifr_server_test_xmlrpc($test, $environment), TRUE);
          $export = str_replace('array (', 'array(', $export);
          $export = preg_replace('/=>\s+array\(/', '=> array(', $export);
          $export = preg_replace('/array\(\s+\)/', 'array()', $export);

          $form['tools']['xmlrpc']['fieldset']['environment'][$environment['environment_id']] = array(
            '#type' => 'tabpage',
            '#title' => $environment['title'],
            '#content' => highlight_string('<?php ' . $export, TRUE),
          );
        }
      }
      else {
        unset($form['tools']['xmlrpc']['fieldset']['#description']);
        $form['tools']['xmlrpc']['fieldset']['#value'] = '<em>' . t('No relevant enviroments.') . '</em>';
      }
    }
    else {
      unset($form['tools']['xmlrpc']['fieldset']['#description']);
      $form['tools']['xmlrpc']['fieldset']['#value'] = '<em>' . t('Cannot preview client test infromation.') . '</em>';
    }

    return $form;
  }

  drupal_set_message(t('Invalid test.'), 'error');
  return array();
}

/**
 * Perform requested operation.
 */
function pifr_server_test_tools_form_submit($form, &$form_state) {
  if ($form_state['values']['op'] == t('Re-test')) {
    $test = pifr_server_test_get($form_state['values']['test_id']);

    if ($test['type'] == PIFR_SERVER_TEST_TYPE_CLIENT) {
      $client = pifr_server_client_get_test($test['test_id']);
      pifr_server_client_confirmation_init($client);

      if ($client['type'] == PIFR_SERVER_CLIENT_TYPE_PROJECT) {
        drupal_set_message(t('Enabled client @client_id.', array('@client_id' => $client['client_id'])));
      }
      else {
        drupal_set_message(t('Initiated testing of client @client_id.', array('@client_id' => $client['client_id'])));
      }
    }
    else {
      pifr_server_test_queue($test);
      pifr_server_log($test['test_id'], 0, PIFR_SERVER_LOG_TEST_REQUEST_MANAGER);

      drupal_set_message(t('Removed result and queued %title.', array('%title' => $test['title'])));
    }
  }
}

function pifr_server_test_type($test_type) {
  switch ($test_type) {
    case PIFR_SERVER_TEST_TYPE_CLIENT:
      return t('Client');
    case PIFR_SERVER_TEST_TYPE_BRANCH:
      return t('Branch');
    case PIFR_SERVER_TEST_TYPE_FILE:
      return t('File');
  }
}

function pifr_server_test_status($status) {
  switch ($status) {
    case PIFR_SERVER_TEST_STATUS_STUB:
      return t('Stub');
    case PIFR_SERVER_TEST_STATUS_QUEUED:
      return t('Queued');
    case PIFR_SERVER_TEST_STATUS_POSTPONED:
      return t('Postponed');
    case PIFR_SERVER_TEST_STATUS_RESULT:
      return t('Result');
  }
}

function pifr_server_test_title($test_type, array $object) {
  // Use custom title if provided.
  if (!empty($object['plugin_argument']['test.title'])) {
    return $object['plugin_argument']['test.title'];
  }

  switch ($test_type) {
    case PIFR_SERVER_TEST_TYPE_CLIENT:
      return t('Client #@id', array('@id' => $object['client_id']));
    case PIFR_SERVER_TEST_TYPE_BRANCH:
      $project = pifr_server_project_get($object['project_id']);
      return t('@project - @identifier', array('@project' => $project['name'], '@identifier' => $object['vcs_identifier']));
    case PIFR_SERVER_TEST_TYPE_FILE:
      return t('@file', array('@file' => $object['file_name']));
  }
}

/**
 * Determine if test title needs to be updated and if so update it.
 *
 * @param array $object Object related to a test (ie. file, branch, client).
 */
function pifr_server_test_title_update(array $object) {
  $test = pifr_server_test_get($object['test_id']);
  $new_title = pifr_server_test_title($test['type'], $object);

  // Title needs to be updated.
  if ($new_title != $test['title']) {
    $test['title'] = $new_title;
    pifr_server_test_save($test);
  }
}

abstract class PIFRTest {

  protected $test;

  protected function __construct(array $test) {
    $this->test = $test;
  }

  public static function getInstanceByTest($test_id) {
    $test = pifr_server_test_get($test_id);

    switch ($test['type']) {
      case PIFR_SERVER_TEST_TYPE_CLIENT:
        return new PIFRTestClient($test);
      case PIFR_SERVER_TEST_TYPE_BRANCH:
        return new PIFRTestBranch($test);
      case PIFR_SERVER_TEST_TYPE_FILE:
        return new PIFRTestFile($test);
    }
    return FALSE;
  }

  public static function getInstanceById($type, $object_id) {
    switch ($type) {
      case PIFR_SERVER_TEST_TYPE_CLIENT:
        $object = pifr_server_client_get($object_id);
        break;
      case PIFR_SERVER_TEST_TYPE_BRANCH:
        $object = pifr_server_branch_get($object_id);
        break;
      case PIFR_SERVER_TEST_TYPE_FILE:
        $object = pifr_server_file_get($object_id);
        break;
    }
    if ($object) {
      return self::getInstanceByTest($object['test_id']);
    }
    return FALSE;
  }

  public function getTest() {
    return $this->test;
  }

  public function getTestTable() {
    $rows = array();

    $rows['status'] = array(
      'data' => array(t('Status'), pifr_server_test_status($this->test['status'])),
      'class' => $this->test['status'] == PIFR_SERVER_TEST_STATUS_POSTPONED ? 'pifr-fail' : '',
    );
    $rows['last_received'] = array(t('Last received'), pifr_server_format_date($this->test['last_received']));
    $rows['last_requested'] = array(t('Last requested'), pifr_server_format_date($this->test['last_requested']));
    $rows['last_tested'] = array(t('Last tested'), pifr_server_format_date($this->test['last_tested']));
    $rows['test_count'] = array(t('Test count'), $this->test['test_count']);

    return $this->formatTable($rows);
  }

  /**
   * Get environment status information.
   *
   * @return array Table rows containing environment information.
   */
  public function getEnvironmentTable() {
    $rows = array();

    // Insert a row and status for each relevant environment.
    $environments = pifr_server_environment_test_get_all($this->test['test_id']);
    foreach ($environments as $environment) {
      $status = pifr_server_environment_status_get($environment['environment_id'], $this->test['test_id']);
      $message = t($status['status']);
      if ($status['status'] == 'result') {
        $message = pifr_server_review_check_code($environment, $status['code']) ? 'pass' : 'fail';
      }

      $rows[] = array($environment['title'], $message);
    }

    $rows = $this->formatTable($rows);

    // Color environment rows based on result status, if present.
    foreach ($rows as &$row) {
      if ($row[1] == 'pass' || $row[1] == 'fail') {
        $row = array(
          'class' => $row[1] == 'pass' ? 'pifr-pass' : 'pifr-fail',
          'data' => $row,
        );
      }
    }

    return $rows;
  }

  public abstract function getType();

  public abstract function getInfo();

  public abstract function getInfoTable();

  protected function formatTable(array $rows) {
    foreach ($rows as &$row) {
      if (isset($row['data'])) {
        $row['data'][0] = array('header' => TRUE, 'data' => $row['data'][0]);
      }
      else {
        $row[0] = array('header' => TRUE, 'data' => $row[0]);
      }
    }
    return $rows;
  }
}
