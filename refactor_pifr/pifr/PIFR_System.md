# PIFR Overview

## Basic architecture
![Overview Image](http://dl.dropbox.com/u/7350603/PIFROverview.png)

* QA.drupal.org is the dispatcher (["Project Issue File Review"](http://drupal.org/project/project_issue_file_review) is PIFR; PIFR server == dispatcher)
* Testbot is a testing client (PIFR Client)
* drupal.org is project client (["Project Issue File Test"](http://drupal.org/project/project_issue_file_test), PIFT submits things to do)
* Testbots are provisioned using [Puppet](http://puppetlabs.com)

## Communication

All communication between systems is XML-RPC. All is initiated by the project client or the testbot client. There are no outgoing connections made by the server/dispatcher.

* pifr.queue: Used by PIFT to queue tests on PIFR server. Documented in pifr_server_xmlrpc_queue()
* pifr.retrieve: Used by PIFT to request test results from PIFR server. Documented in pifr_server_xmlrpc_retrieve()
* pifr.next: Used by PIFR testbot to request the next test from PIFR server. Documented in pifr_server_xmlrpc_next()
* pifr.result: Used by PIFR testbot to provide a test result to PIFR server. Documented in pifr_server_xmlrpc_result().

## Life Cycle of a Test

A test starts on drupal.org as a patch submission or a new commit.

The *pift_test* table controls testing.

Column| Usage
------|-----------------------------------------------------------------------
type  |(PIFT_TYPE_RELEASE=1 or PIFT_TYPE_FILE=2, defined in pift.module) determines whether this is a branch/release test or a patch test.
id    |either the fid from the files table (in the case of a patch) or the nid of the release node (in the case of a branch/release test)
status|PIFT_STATUS_QUEUE=1, PIFT_STATUS_SENT=2, PIFT_STATUS_FAIL=3, PIFT_STATUS_PASS=4

It also has the test result message and a last-tested time.

A branch/release test comes into being when a commit is made and hook_versioncontrol_git_refs_updated() is invoked (pift_versioncontrol_git_refs_updated()).  If the project is enabled for testing and there is a release node for the ref updated, the test is either requeued (which just means updating the status of the existing row) or a new test row is added.

A patch/file test comes into being when either

* A node is created (hook_nodeapi is invoked) and the issue meets the criteria configured in pift configuration, the files attached to the node are examined. If they meet the criteria for testing (mostly file naming) the files are queued for testing.
* A comment is created (hook_comment is invoked). The same basic process is used to select files which should be queued.

A patch/file test can also be requeued by

* Use of the requeue link at /pift/retest/% (provided on existing comments that have patches)
* When the issue status changes to a testable status, previous files attached to the node are requeued with pift_test_add_previous_files()

Things start to happen when hook_cron (or drush pift-cron) is run. (Currently drush pift-cron is run every minute by hudson.)

1. pift_cron_queue_batch() causes tests in PIFT_STATUS_QUEUE state to be sent to the PIFR server (qa/dispatcher) using the pifr.queue XMLRPC method
  * pift_cron_queue_batch_build() is used to build a structured $batch array. The batch array is documented there (and also in pifr_server_xmlrpc_queue), but the summary is:
      * $batch['branches'], an array of structured arrays containing branch information for both release/branch tests and file tests. If $batch['branches'][x]['test'] == TRUE, it means that this branch should be tested in its own right (a commit was done).
      * $batch['files'], information on the patchfiles to be tested.
      * $batch['projects'], information on the related projects
2. pift_cron_queue_batch() does the xmlrpc() call to send the batch.
  * A response is received, which either contains mappings of client identifiers (patch file fids or project release node nids) to test ids, or which contains a list of errors. The response array possibilities are documented in the code.
  * pift_cron_retrieve_results() requests results from the PIFR server (qa/dispatcher)
  * For each of the branches and files, pifr_test_sent() is called, which updates the pift_test table with the new status (sent)

Now control moves to qa.drupal.org or whatever PIFR server is configured.

* pifr_server_xmlrpc_queue() is invoked

        foreach item in batch
          foreach project
            Update or insert project information from $batch['projects'] into the pifr_project table
          foreach branch
            Update or insert branch information from $batch['branches'] into the pifr_test table. 
            (Branches are type PIFR_SERVER_TEST_TYPE_BRANCH = 2)
            if $branch['test'] is TRUE, then
              queue the branch for testing 
              (this means setting status=PIFR_SERVER_TEST_STATUS_QUEUED=2 in the pifr_test table
              Delete any previous results for this test from the pifr_result table
              If there are dependencies ($branch['dependency'])
                update or insert the branch information for each of the dependencies into pifr_test.
          foreach file (a file is a patch)
            Update or insert the test/file information into pifr_test table. 
            Files are of type PIFR_SERVER_TEST_TYPE_FILE = 3
            Update or insert the file information into pifr_file table
            Queue the file for testing (set status=PIFR_SERVER_TEST_STATUS_QUEUED=2 in pifr_test table)
          foreach branch
            if the branch has failed testing, 
              find all files queued for testing and set them to PIFR_SERVER_TEST_STATUS_POSTPONED = 3 in pifr_test.
        return the response to PIFT

Control now returns to PIFT.

1. We return in the middle of pift_cron_queue_batch(). The response structure is documented there. It consists of arrays of branch and file mappings to tests.
2. The pift_test table is updated. status on each item for branches and files is updated to PIFT_STATUS_SENT = 2.
3. Now pift_cron_retrieve_results() is called. It
  * Uses the pifr.retrieve XMLRPC method, and in turn calls the pifr_server_xmlrpc_retrieve() method.on the PIFR side and returns an array of results in the form array(test_id, pass (TRUE/FALSE), message).
        Foreach item returned
          update the pift_test table with the status and other information
          Check to see if an automatic test followup is required in pift_cron_check_auto_followup().
          If a followup comment is required (If the test failed and we need to set the test to "Needs Work" then pift_cron_auto_followup().

Now during this time the testbot (PIFR test client) will have needed to do some testing. At every pift-cron (should be every minute) the testbot calls the PIFR server (qa) with the XML-RPC pifr.next. This results in PIFR calling pifr_server_test_get_next(), which calls pifr_server_test_get_next_eligible() to find the next test to be tested.

The testbot takes the test and runs it through a complete series of checks and then returns the response to PIFR server. with XML-RPC pifr.result.

## PIFR Module Directory Structure

Understanding the subdirectories in project_issue_file_review is key to navigating it.

* Root directory: pifr.module, shared code between server and client.
* /client: the pifr_client module, that implements the testbot
* /server: the pifr_server module that implements the server, or dispatcher + UI
* /util: Miscellaneous stuff, not mainline. Example: pifr_shell, a way to insert XMLRPC traffic.
* /review: Implements plugins, classes for both the client and the server side. It's not known why it's called "review"'. In this directory the various plugins' parentage is different for the client and the server sides. Each subdirectory has its own "confirmation" directory, which is where patches for the client (testbot) side confirmation patches are stored.
  * /review/assertion: pifr_assertion.module, which defines a class for assertions, especially for their display. Seems to be used only by the server.
  * /review/coder: pifr_code.module. Provides classes for both testing (client) and UI/storage (server) against coder module.
  * /review/drupal: Provides classes for installing Drupal (client) and UI/storage of results of that activity (server)
  * /review/simpletest: Provides classes for running simpletest on an installed Drupal site (client) and for displaying and storing the results (server)

## PIFR Client (testbot) Testing Strategy

There are eight operations. Since the names of the operations are a bit terse, they can be ambiguous. The basic operations are defined as $this->steps in review/server.inc

Operation|Class|Description
---------|-----|------------------------------------------------------------
setup    |     |Basic preparation.
fetch    |     |Retrieve the patch required for the test
checkout |     |Check out the specified code
check    |     |Perform rudimentary analysis of the patch (CRs?)
apply    |     |Attempt to apply the patch
syntax   |     |Check modified files to make sure they still parse
install  |     |Install drupal
review   |     |Run the actual tests (simpletest, coder, or whatever)


## How to set up a test environment

Although it's complex, it's *not* that hard to set up a complete test environment. You need a clone or partial clone of drupal.org, and then you need a qa site and a testbot site. (It is theoretically possible for qa and testbot to be the same drupal site, although I haven't done that.) All three can be on a single server/workstation. When you start to run real, complete tests on the testbot, though, that will become too onerous and slow and you'll need to set up a specially configured server or VM for the testbot.

1. Set up your drupal.org clone site using either full code extracted from a workarea on scratchvm.drupal.org or code checked out from bzr, etc. Configure with the daily sanitized database. 
2. Set up your qa clone site using either the code from qa or checked out from bzr. Enable the PIFR Server module. It may be easiest to work with a database copy from qa.drupal.org.
3. Set up your testbot clone site. You really only need a D6 site with PIFR; enable the PIFR client module.
4. Create/edit/install the keys and URLs relating the three sites.
5. The testbot *must* be able to look up the hostnames of both the project client (to get patches) and the PIFR server. Otherwise you'll get the mysterious error "You may not be running PIFR 2.0".

## How PIFR testbot Confirmation Works

* For each stage or step or operation, create a test that will fail with that operation
* Walk through those tests, validating failure at that point

## Step-Debugging XML-RPC

http://drupal.org/node/125861#comment-1372632 has a good suggestion about debugging XML-RPC calls on the receiving side. You can just set up a debugging session and grab the magic query string that's added to the bare XMLRPC query string and add that on the client side, and you'll pop into the debugger when needed. I did this with Eclipse + Zend plugin.

In PIFT, the variable pift_xmlrpc_url_query can be set to a query string and this query string will be appended to the xmlrpc.php URL, allowing this with no code hack.

## Miscellaneous

* There is an xmlrpc_log.patch which can be applied against D6 to provide XML-RPC logging.

## Properties/Arguments used in test arrays

The various arrays used for internal commuunication have a place for "arguments" which are free-form but, if they're used, have to be right.

Argument|Description
--------|-----------------------------------------------------------------------------|
test.directory.review|Directory of code we're testing
test.title|This may currently be unused.
test.directory.apply |
test.directory.dependency|Directory into which modules should be checked out.
test.files|
test.extensions|                                                                     |
drupal.core.version|Drupal core version to be used (7)                               |
drupal.core.url|The git url for drupal core (git://git.drupal.org/project/drupal.git)|
drupal.modules|list of modules contained within the release (see [bug](http://drupal.org/node/879662) Some caching issues.|
drupal.user|Name of administrative user
drupal.pass|Password of administrative user
drupal.variables|Array of Drupal variable arrays

## Related Projects on Drupal.org

Project        |Description
---------------|---------------------------------------------------------------|
[Project Issue File Review (PIFR)](http://drupal.org/project/project_issue_file_review)|Provides PIFR client (testbot) and server (qa.drupal.org) functionalities|
[Project Issue File Test (PIFR)](http://drupal.org/project/project_issue_file_test)|Provides test submission capabilities (on drupal.org)|
[drupaltestbot-puppet](http://drupal.org/project/drupaltestbot-puppet)|The puppet script used to deploy a PIFR testbot configuration.|
[drupaltestbot](http://drupal.org/project/drupaltestbot)|The Debian packages that compose a PIFR testbot|


