<?php

/**
 * @file
 * Provide Drupal client review base implementation.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Drupal client review base implementation.
 */
class pifr_client_review_pifr_drupal extends pifr_client_review {

  /**
   * Default Drupal core repository URL.
   * @todo: We will be testing more than one branch of core shortly!
   *
   */
  protected $core_url = 'git://git.drupal.org/project/drupal.git';

  /**
   * Location of default sites module directory.
   */
  protected $module_directory = 'sites/default/modules';

  /**
   * Location of project being tested.
   *
   * @var string
   */
  protected $project_directory;

  /**
   * Analyze review information.
   */
  public function __construct(array $test) {
    parent::__construct($test);

    $this->core_url = variable_get('pifr_drupal_repository', 'git://git.drupal.org/project/drupal.git');
    $this->admin_username = $test['review']['argument']['drupal.user'];
    $this->admin_password = !empty($test['review']['argument']['drupal.pass']) ? $test['review']['argument']['drupal.pass'] : 'drupal';
    // Allow the Drupal core URL to be overridden via an argument.
    if (!empty($this->test['review']['argument']['drupal.core.url'])) {
      $this->core_url = $this->test['review']['argument']['drupal.core.url'];
    }

    pifr_debug('Test(%test_id) request received: <pre>%test</pre>', array('%test_id' => $this->test['test_id'], '%test' => print_r($test, TRUE)));
    $this->log('core_url = [' . $this->core_url . '];  Test repository = [' . $this->test['vcs']['main']['repository']['url'] . ']');



    if ($this->test['review']['plugin'] == 'pifr_simpletest') {
      if ($this->test['vcs']['main']['repository']['url'] != $this->core_url) {
        // Main repository is not core, so this must be a module test. Move the
        // module repository (main) to the dependencies and find the core
        // repository in the dependencies and move it to main.
        $this->test['vcs']['dependencies'][] = $this->test['vcs']['main'];

        // Store the project name 'simpletest' for use when applying patches.
        $this->project_directory = basename($this->test['vcs']['main']['repository']['url']);
        $this->project_directory = preg_replace('/\.git$/', '', $this->project_directory);
        $this->log('Project directory is [' . $this->project_directory . ']');


        foreach ($this->test['vcs']['dependencies'] as $key => $dependency) {
          if ($dependency['repository']['url'] == $this->arguments['drupal.core.url']) {
            $this->test['vcs']['main'] = $dependency;
            unset($this->test['vcs']['dependencies'][$key]);
            break;
          }
        }
      }

      // Add SimpleTest as dependencies, unless it has already been added.
      if ($this->test['review']['argument']['drupal.core.version'] == 6) {
        $simpletest_url = variable_get('pifr_simpletest_repository', 'git://git.drupal.org/project/simpletest.git');
        $found = FALSE;
        foreach ($this->test['vcs']['dependencies'] as $key => $dependency) {
          if ($dependency['repository']['url'] == $simpletest_url) {
            $found = TRUE;
            break;
          }
        }

        if (!$found) {
          // Since SimpleTest is not included in Drupal 6 it needs to be checked
          // out from contrib.
          $this->test['vcs']['dependencies'][] = array(
            'repository' => array(
              'type' => 'git',
              'url' => $simpletest_url,
            ),
            'vcs_identifier' => '6.x-2.x',
          );
        }
      }

      // Set the dependency directory.
      $this->arguments['test.directory.dependency'] = $this->module_directory;

      // If the main project is not Drupal core then all patches should be
      // applied from the root of the primary dependency or project directory.
      if (!empty($this->project_directory)) {
        $this->arguments['test.directory.apply'] = $this->module_directory . '/' . $this->project_directory;
      }
    }
  }


  /**
   * Get the relevant files to check for syntax.
   *
   * If patches and dependencies are being reviewed then make paths relative to modules.
   */
  protected function syntax_files() {
    // If no files to apply then attempt to filter files by arguments.
    if (!$this->apply_files()) {
      if (!empty($this->arguments['drupal.modules'])) {
        $files = array();
        foreach ($this->arguments['drupal.modules'] as $module) {
          $path = $this->module_path($module);
          if ($path !== FALSE) {
            if (!empty($path)) {
              $scan = file_scan_directory($this->checkout_directory . '/' . $path, '.*');
            }
            else {
              $scan = file_scan_directory($this->checkout_directory, '.*');
            }
            foreach ($scan as $file) {
              $files[] = str_replace($this->checkout_directory . '/', '', $file->filename);
            }
          }
        }
        return $files;
      }
      elseif (!empty($this->test['review']['argument']['directory'])) {
        $scan = file_scan_directory($this->checkout_directory . '/' . $this->test['review']['argument']['directory'], '.*');
        $files = array();
        foreach ($scan as $file) {
          $files[] = str_replace($this->checkout_directory . '/', '', $file->filename);
        }
        return $files;
      }
    }

    $files = parent::syntax_files();

    // If there are patch files and dependencies then assume the patch files
    // are relative to the modules.
    if (!empty($this->test['files']) && !empty($this->test['vcs']['dependencies'])) {
      foreach ($files as &$file) {
        // Ensure module directory hasn't already been prepended!
	// Don't apply for coder tests, as they take place in checkout directly
        if (($this->test['review']['plugin'] != 'pifr_coder') && (strpos($file, $this->module_directory) === FALSE)) {
          $file = $this->module_directory . '/' . $file;
        }
      }
    }

    return $files;
  }

  /**
   * Ingore non-PHP files during syntax check.
   */
  protected function syntax_ignore(array $files) {
    $php_extensions = array('php', 'inc', 'install', 'module', 'test');
    foreach ($files as $key => $file) {
      if (!in_array(pathinfo($file, PATHINFO_EXTENSION), $php_extensions)) {
        unset($files[$key]);
      }
    }
    return $files;
  }

  /**
   * Check syntax using PHP parser.
   */
  protected function syntax_check($file) {
    return $this->exec('php -l -f ' . escapeshellarg('./' . $file));
  }

  /**
   * Run the Drupal installer.
   */
  protected function install() {
    global $base_url, $base_path;

    // Make a copy of the settings file to ensure there is no permission issue.
    $default_settings = $this->checkout_directory . '/sites/default/default.settings.php';
    $settings = $this->checkout_directory . '/sites/default/settings.php';
    copy($default_settings, $settings);

    // Use paths relative to checkout directory so that SimpleTest browser will function.
    $base_url .= '/checkout';
    $base_path = '/checkout';
    require_once drupal_get_path('module', 'pifr_client') . '/review/browser.inc';

    // Install Drupal.
    $b = new PIFRBrowser();

    // Step: Select an installation profile.
    // Step: Choose language.
    $profile = $this->test['review']['argument']['drupal.core.version'] == 6 ? 'default' : 'standard';
    $install_path = $base_url . '/install.php?profile=' . $profile . '&locale=en';
    $html = $b->drupalGet($install_path);
    $b->assertText(t('Database name'));
    if (($b->results['#exception'] + $b->results['#fail']) != 0) {
      $this->set_error(array('@reason' => 'Installing: failed to arrive at database configuration page using install path ' . $install_path));
      return;
    }


    // Step: Database configuration.
    $db_info = $this->database->get_information();
    $edit = array();
    if ($element = $b->xpath('//input[@name="driver" and not(@disabled)]')) {
      $edit['driver'] = pifr_client_review_database_type();
      $this->log('Setting driver to ' . $edit['driver']);
      $this->log('Element was ' . print_r($element, TRUE));
    }

    if ($this->arguments['drupal.core.version'] == 6) {
      // Drupal 6.
      $edit['db_path'] = $db_info['name'];
      $edit['db_user'] = $db_info['username'];
      $edit['db_pass'] = $db_info['password'];
      $edit['db_host'] = $db_info['host'];
      $edit['db_prefix'] = '';
    }
    else {
      // Drupal 7 and above.
      $edit['mysql[database]'] = $db_info['name'];
      $edit['mysql[username]'] = $db_info['username'];
      $edit['mysql[password]'] = $db_info['password'];
      $edit['mysql[host]'] = $db_info['host'];
    }

    $b->drupalPost($install_path, $edit, t('Save and continue'));

    if (($b->results['#exception'] + $b->results['#fail']) != 0) {
      $this->log("Error $edit used for database submission to $install_path: " . print_r($edit, TRUE));
      $this->log("Error submitting database configuration: " . print_r($b->results, TRUE));
      $this->set_error(array('@reason' => 'Installing: failed to set database information (database name, user, password were wrong)'));
      return;
    }

    // Step: Site configuration.
    if (empty($this->arguments['drupal.pass'])) {
      $this->arguments['drupal.pass'] = 'drupal';
    }

    $edit = array();
    $edit['site_name'] = 'checkout';
    $edit['site_mail'] = 'admin@example.com';
    $edit['account[name]'] = $this->admin_username;
    $edit['account[mail]'] = 'admin@example.com';
    $edit['account[pass][pass1]'] = $this->admin_password;
    $edit['account[pass][pass2]'] = $this->admin_password;
    $edit['update_status_module[1]'] = FALSE;
    // Call install.php with path again since browser will have messed up URL.
    $b->drupalPost('install.php', $edit, t('Save and continue'), array('query' => 'profile=' . $profile . '&locale=en'));

    // Step: Finished.
    $b->assertText(t('Drupal installation complete'));

    $base_url = $this->original['base_url'];
    $base_path = $this->original['base_path'];

    // Make sure that site installed correctly.
    if (($b->results['#exception'] + $b->results['#fail']) != 0) {
      $this->set_error(array('@reason' => 'Installing: failed to complete installation by setting admin username/password/etc.'));
    }

    // Set Drupal variables if specified.
    if (!empty($this->arguments['drupal.variables'])) {
      $this->install_variables($this->arguments['drupal.variables']);
    }
  }

  /**
   * Set Drupal variables.
   *
   * @param array $variables List of variables in the format key:value.
   */
  protected function install_variables(array $variables) {
    foreach ($this->arguments['drupal.variables'] as $variable) {
      list($key, $value) = explode(':', $variable, 2);
      if (!$this->variable_set($key, $value)) {
        $this->set_error(array('@reason' => 'failed to set variable [' . $key . '] in checkout database'));
        break;
      }
    }
  }

  /**
   * Get the relative path to a module in the checkout directory.
   *
   * @param string $module Name of the module.
   * @param boolean $reset Reset the static list of modules.
   * @return string Relative location of file, or FALSE.
   */
  protected function module_path($module, $reset = FALSE) {
    static $modules = array();

    if (!$modules || $reset) {
      // Ensure file scan directory exists
      $scanpath = $this->checkout_directory . '/' . $this->module_directory;
      if (!file_check_directory($scanpath)) {
        $this->module_directory = '';
        $scanpath = $this->checkout_directory;
      }
      $files = file_scan_directory($scanpath, '\.module$');
      foreach ($files as $file) {
        $modules[basename($file->basename, '.module')] = ltrim(str_replace($this->checkout_directory, '', dirname($file->filename)), '/');
      }
    }
    if (isset($modules[$module])) {
      return strval($modules[$module]);
    }

    $this->log('No such module [' . $module . '].');
    $this->set_error(array('@reason' => 'No such module [' . $module . ']'));
    return FALSE;
  }

  /**
   * Set Drupal variable in {variables} table.
   *
   * @param string $name Name of variable.
   * @param string $value Value of variable.
   * @return boolean TRUE if successfull, otherwise FALSE.
   */
  protected function variable_set($name, $value) {
    // Prepare name and value.
    $name = addslashes($name);
    $value = serialize($value);

    // Clear cache to ensure variables take hold.
    $this->database->query('DELETE FROM {cache}');

    // Remove any existing entry and insert the new entry.
    $this->database->query("DELETE FROM {variable} WHERE name = '$name'");
    return $this->database->query("INSERT INTO {variable} (name, value) VALUES ('$name', '$value')");
  }
}
