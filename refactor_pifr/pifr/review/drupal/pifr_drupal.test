<?php
/**
 * @file
 * Ensure that base Drupal review functionality works.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

module_load_include('module', 'pifr');
module_load_include('test', 'pifr_client');

/**
 * Ensure that base Drupal review functionality works.
 */
class PIFRClientDrupalTestCase extends PIFRClientPluginTestCase {

  /**
   * Add Drupal core repository information.
   */
  public function __construct($test_id = NULL) {
    parent::__construct($test_id);

    $this->test = array_replace_recursive($this->test, array(
      'vcs' => array(
        'main' => array(
          'repository' => array(
            'type' => 'git',
            'url' => variable_get('pifr_drupal_repository', 'git://git.drupal.org/project/drupal.git'),
          ),
          'vcs_identifier' => variable_get('pifr_confirmation_vcs_id', '7.2'),
        ),
      ),
    ));
  }

  /**
   * Ensure that handling of dependencies works properly.
   */
  protected function testDependecies() {
    // Add SimpleTest and subuser dependency and execute review.
    $test = array_replace_recursive($this->test, array(
      'vcs' => array(
        'dependencies' => array(
          array(
            'repository' => array(
              'type' => 'git',
              'url' => variable_get('pifr_simpletest_repository', 'git://git.drupal.org/project/simpletest.git'),
            ),
            'vcs_identifier' => '7.x-2.x',
          ),
          array(
            'repository' => array(
              'type' => 'git',
              'url' => variable_get('pifr_menu_rewrite_repository', 'git://git.drupal.org/project/menu_rewrite.git'),
            ),
            'vcs_identifier' => 'master',
          ),
        ),
      ),
    ));
    $result = $this->review($test);

    // Ensure that only SimpleTest related material was reviewed.
    if (preg_match('/Checked \[(\d+) of (\d+)\] relevant file\(s\) for syntax./', $result['log'], $match)) {
      $this->assertTrue($match[1] < 30, 'Only dependency files were checked for syntax.');
    }
    else {
      $this->fail('Files checked for syntax.');
    }

    // Ensure that the VCS checked out main branch and depdencies.
    $this->assertLogText('Main branch [drupal] checkout [complete].');
    $this->assertLogText('Dependency branch [simpletest] checkout [complete].');
    $this->assertLogText('Dependency branch [menu_rewrite] checkout [complete].');
  }
}
