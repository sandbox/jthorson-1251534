<?php

/**
 * @file
 * Provide base server review class.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Base server review class.
 */
abstract class pifr_server_review {

  /**
   * Associative array of default arguments.
   *
   * @var array
   */
  protected $argument_default = array(
    'test.title' => NULL,
    'test.directory.review' => array(''),
    'test.directory.apply' => '',
    'test.directory.dependency' => '',
    'test.files' => array(),
    'test.extensions' => array(),
  );

  /**
   * The operations which make up a review mapped to integer codes.
   *
   * @var array
   */
  protected $map = array();

  /**
   * The operations which make up a confirmation test mapped to integer codes.
   *
   * @var array
   */
  protected $confirmation_map = array();

  /**
   * The default confirmation test array.
   *
   * @var array
   */
  protected $confirmation_default = array(
    'test_id' => 17,
    'review' => array(
      'plugin' => '',
      'argument' => array(),
    ),
    'vcs' => array(),
    'files' => array(),
  );

  /**
   * Detailed step information.
   *
   * @var array
   */
  protected $steps = array(
    'setup' => array(
      'title' => 'setup environment',
      'active title' => 'setup environment',
      'description' => 'The testbot client is probably malfunctioning.',
      'help' => 'The testbot client failed to setup the testing environment. Check the pifr_client the !install.',
      'summary' => 'Setup environment: @reason',
      'confirmation' => FALSE,
    ),
    'fetch' => array(
      'title' => 'invalid file URL',
      'active title' => 'detect an invalid patch URL',
      'description' => 'Ensure the patch to be tested can be accessed and downloaded.',
      'help' => 'The testbot client failed to detect an invalid patch URL that pointed to example.com.',
      'summary' => 'Fetch test patch: @reason',
      'confirmation' => 'no-patch',
    ),
    'checkout' => array(
      'title' => 'invalid repository information',
      'active title' => 'detect invalid repository information',
      'description' => 'Ensure the version control system is functioning properly.',
      'help' => 'The client failed to detect invalid repository information that pointed to example.com.',
      'summary' => 'Repository checkout: @reason',
      'confirmation' => 'no-patch',
    ),
    'check' => array(
      'title' => 'invalid patch format',
      'active title' => 'detect invalid patch format',
      'description' => 'Ensure the patch only contains unix-style line endings.',
      'help' => 'The client failed to @type which is induced using the !patch.',
      'summary' => 'Invalid patch format in @filename',
      'confirmation' => 'pifr_server',
    ),
    'apply' => array(
      'title' => 'non-applicable patch',
      'active title' => 'detect a non-applicable patch',
      'description' => 'Ensure the patch applies to the tip of the chosen the code-base.',
      'help' => 'The testbot client failed to @type which is induced using the !patch.',
      'summary' => 'Unable to apply patch @filename. @reason',
      'confirmation' => 'pifr_server',
    ),
    'syntax' => array(
      'title' => 'invalid syntax',
      'active title' => 'detect invalid syntax',
      'description' => 'Check the syntax of the patched files.',
      'help' => 'The client failed to @type which is induced using the !patch.',
      'summary' => 'Invalid syntax in @filename',
      'confirmation' => FALSE,
    ),
    'install' => array(
      'title' => 'installation failure',
      'active title' => 'detect an installation failure',
      'description' => 'Ensure that you can perform a fresh installation.',
      'help' => 'The client failed to @type which is induced using the !patch.',
      'summary' => 'Failed to install',
      'confirmation' => FALSE,
    ),
    'review' => array(
      'title' => 'review failed to run',
      'active title' => 'detect a review run failure',
      'description' => 'Ensure that you can perform the review manually.',
      'help' => 'The client failed to @type which is induced using the !patch.',
      'summary' => 'Failed to review: @reason',
      'confirmation' => FALSE,
    ),
    'fail' => array(
      'title' => 'review failed',
      'active title' => 'detect a failing review',
      'description' => 'Look over the review failures below.',
      'help' => 'The testbot client failed to @type which is induced using the !patch.',
      'summary' => 'Failed review',
      'confirmation' => FALSE,
    ),
    'pass' => array(
      'title' => 'review passed',
      'active title' => 'complete test suite with all tests passing',
      'description' => 'Congratulation everything looks good!',
      'help' => 'Testbot client ran the test suite with all tests passing.',
      'summary' => 'Passed review',
      'confirmation' => FALSE,
    ),
  );

  /**
   * Generate step map and confirmation step map.
   */
  public function init() {
    $i = $j = 1;
    foreach ($this->steps as $step => $info) {
      $this->map[$i++] = $step;
      if ($info['confirmation'] !== FALSE) {
        $this->confirmation_map[$j++] = $step;
      }
    }
  }

  /**
   * Get the associative array of default arguments.
   *
   * @return array Associative array of default arguments.
   */
  public function argument_default_get() {
    return $this->argument_default;
  }

  /**
   * Get all the operations.
   *
   * @return array List of operation keys.
   */
  public function operation_get_all() {
    return array_keys($this->steps);
  }

  /**
   * Get the operation related to the code.
   *
   * @param integer $code Operation code.
   * @return string Operation related to the code.
   */
  public function operation_get($code) {
    return $this->map[$code];
  }

  /**
   * Get the operation related to the confirmation stage code.
   *
   * @param integer $code Confirmation stage operation code.
   * @return string Confirmation operation related to the code.
   */
  public function operation_confirmation_get($code) {
    return $this->confirmation_map[$code];
  }

  /**
   * Get all the steps.
   *
   * @return array List of steps.
   * @see self::step_get()
   */
  public function step_get_all() {
    return $this->steps;
  }

  /**
   * Get the step related to the code.
   *
   * @param integer $code Step code.
   * @return array Step information, with keys: key, title, description, help,
   *   and summary, confirmation.
   */
  public function step_get($code) {
    $key = $this->operation_get($code);
    $step = $this->steps[$key];
    $step['key'] = $key;

    return $step;
  }

  /**
   * Get the confirmation step related to the code.
   *
   * @param integer $code Confirmation stage code.
   * @return array Step information.
   * @see self::step_get()
   */
  public function step_confirmation_get($code) {
    $key = $this->operation_confirmation_get($code);
    $step = $this->steps[$key];
    $step['key'] = $key;

    return $step;
  }

  /**
   * Get the code related to the operation.
   *
   * @param string $operation Operation key.
   * @return integer Code related to operation.
   */
  public function code_get($operation) {
    foreach ($this->map as $code => $key) {
      if ($operation == $key) {
        return $code;
      }
    }
    return FALSE;
  }

  /**
   * Merge arguments from the various parts that makeup the test.
   *
   * The default behavior is to merge the dependency arguments in reverse order
   * so the first dependencies have higher priority. The rest of the arguments
   * are then merged in the following order from least to greatest priority:
   * default, environment, dependencies (merged), and branch.
   *
   * @param array $environment_argument Arguments from the environments.
   * @param array $dependency_argument List of arguments from dependency. Each
   *   element of the array is another array of arguments from a dependency.
   * @param array $branch_argument Arguments from the branch.
   * @see self::argument_default()
   */
  public function argument(array $environment_argument, array $dependency_argument, array $branch_argument) {
    // Merge dependency arguments in reverse order so that first dependencies
    // have priority.
    $merged = array();
    foreach ($dependency_argument as $argument) {
      $merged = array_replace_recursive($argument, $merged);
    }

    return array_replace_recursive($this->argument_default, $environment_argument, $merged, $branch_argument);
  }

  /**
   * Get a client confirmation test for the stage.
   *
   * @param array Client information.
   * @param array Environment information.
   * @param integer $stage Confirmation stage.
   * @return array XML-RPC reponse containing test definition.
   * @see pifr_server_xmlrpc_next()
   */
  public function confirmation_test_get(array $client, array $environment, $stage) {
    $step = $this->step_confirmation_get($stage);
    $changes = $this->confirmation_test_change($step['key'], $step['confirmation']);
    return $this->confirmation_test_merge($client, $environment, $changes);
  }

  /**
   * Get the changes to the default client confirmation test array.
   *
   * The changes should then be merged with the default client confirmation test
   * array before being sent as the XML-RPC response.
   *
   * @param string $key Confirmation step key.
   * @param string $module Module providing the confirmation patch, or
   *   'no-patch' if a patch is not required for confirmation stage.
   * @return array Changes to default client confirmation test array.
   */
  protected function confirmation_test_change($key, $module) {
    if ($key == 'fetch') {
      return array(
        'files' => array(
          'http://example.com/pifr_server/some.patch',
        ),
      );
    }
    elseif ($key == 'checkout') {
      return array(
        'vcs' => array(
          'main' => array(
            'repository' => array(
              'type' => 'cvs',
              'url' => 'example.com/cvs',
            ),
          ),
        ),
      );
    }
    elseif ($module != 'no-patch') {
      // The stages require a patch to cause the client to fail in such a way
      // as to cause the desired result code.
      return array(
        'files' => array(
          $this->confirmation_test_patch($key, $module),
        ),
      );
    }
    return array();
  }

  /**
   * Get a confirmation patch related to the stage.
   *
   * @param string $key Confirmation step key.
   * @param string $module Module providing the confirmation patch.
   * @return string Absolute URL to confirmation patch.
   */
  protected function confirmation_test_patch($key, $module) {
    $base = url('', array('absolute' => TRUE));
    return $base . drupal_get_path('module', $module) . '/confirmation/' . $key . '.patch';
  }

  /**
   * Merge an array of changes with the default client confirmation test array.
   *
   * @param array $client Client information.
   * @param array $environment Environment information.
   * @param array $changes Changes to default client confirmation test array.
   * @return array XML-RPC reponse containing test definition.
   */
  protected function confirmation_test_merge($client, array $environment, array $changes) {
    $test = array(
      'test_id' => $client['test_id'],
      'review' => array(
        'plugin' => $environment['plugin'],
        'argument' => pifr_server_review_argument_get($environment),
      ),
    );
    return array_replace_recursive($this->confirmation_default, $test, $changes);
  }

  /**
   * Check the result of a client confirmation test.
   *
   * @param array $client Client information.
   * @param array $result Result information.
   * @param array $status Client confirmation status information.
   * @return string Status of client confirmation, either: pass, fail, or
   *   continue.
   */
  public function confirmation_check(array $client, array $result, array $status) {
    $confirmation_op = $this->operation_confirmation_get($status['stage']);
    $result_op = $this->operation_get($result['code']);

    pifr_debug('confirmation_check(): $status[stage]=%status_stage, result_op=%result_op, confirmation_op=%confirmation_op', array('%status_stage' => $status['stage'], '%result_op' => $result_op, '%confirmation_op' => $confirmation_op));
    if ($status['stage'] == count($this->confirmation_map) && $result_op == $confirmation_op) {
      // Client confirmation has reached the last stage and is now complete.
      return 'pass';
    }
    else {
      // Check the result against the current stage.
      if ($result_op != $confirmation_op) {
        pifr_debug('confirmation_check(): Failing confirmation because confirmation_op(%confirmation_op) != result_op(%result_op)', array('%result_op' => $result_op, '%confirmation_op' => $confirmation_op));
        return 'fail';
      }
      else {
        // Result is fine and the test is not complete so continue to next stage.
        return 'continue';
      }
    }
  }

  /**
   * Check a result.
   *
   * @param array $result Result information.
   * @return boolean TRUE if pass, otherwise FALSE.
   */
  public function check(array $result) {
    return $this->operation_get($result['code']) == 'pass';
  }

  /**
   * Get the unformatted summary message for a review result.
   *
   * @param array $result Result information.
   * @return string Unformatted summary message.
   */
  public function summary(array $result) {
    $step = $this->step_get($result['code']);
    return $step['summary'];
  }

  /**
   * Format the result details.
   *
   * @param array $details Result detail array.
   * @return array Formated result details.
   */
  public function summary_format(array $details) {
    return $details;
  }

  /**
   * Generate a detailed display of test a test result.
   *
   * @param array $test Test information.
   * @param array $result Test result information.
   * @return stirng HTML output.
   */
  public function detail(array $test, array $result) {
    return $this->detail_step($test, $result) . $this->detail_display($test, $result);
  }

  /**
   * Generate a list of steps.
   *
   * The list will contain classes and additional details based on the test,
   * environment, and results.
   *
   * @param array $test Test information.
   * @param array $result Test result information.
   * @return string HTML output.
   */
  protected function detail_step(array $test, array $result) {
    $is_client = $test['type'] == PIFR_SERVER_TEST_TYPE_CLIENT;
    if ($is_client) {
      $client = pifr_server_client_get_test($test['test_id']);
      $status = pifr_server_client_confirmation_status($client);

      // Since there is a result for this environment then it has been tested. If
      // the client is enabled then the confirmation test passed and thus this
      // environment must have passed. If the client is not enabled and the last
      // environment the confirmation test ran in is not this one then it must
      // have passed, otherwise the last environment to run is this one and it
      // should be evaluated based on the stage it got to.
      if ($client['status'] == PIFR_SERVER_CLIENT_STATUS_ENABLED || $status['environment_id'] != $result['environment_id']) {
        $code = $this->code_get('pass');
      }
      else {
        // $status['environment_id'] == $result['environment_id']
        $code = $this->code_get($this->operation_confirmation_get($status['stage']));
      }
    }
    else {
      // Since the test is not a client test it can be evaluated solely on the
      // basis of the result code.
      $code = $result['code'];
    }

    // Generate a list of the steps.
    $steps = array();
    foreach ($this->steps as $operation => $info) {
      $step_code = $this->code_get($operation);

      $step = array(
        'data' => ucfirst($info['active title']),
        'class' => array(),
      );

      // Add classes used to style the first three steps since they are unrelated
      // to the item being tested.
      if ($step_code <= $this->code_get('checkout')) {
        $step['class'][] = 'pifr-init';
        if ($operation == 'checkout') {
          $step['class'][] = 'pifr-last';
        }
      }

      // If the step is less then the code then it has passed. If the step is
      // equal to the code then it must be checked, otherwise ignore the step.
      if ($step_code < $code) {
        $step['class'][] = 'pifr-pass';
      }
      elseif ($step_code == $code) {
        // If the step is the final step and the result has passed then mark as
        // passed, otherwise the step is the one that failed.
        if ($operation == 'pass' && $this->check($result)) {
          $step['class'][] = 'pifr-pass';
        }
        else {
          $step['class'][] = 'pifr-fail';

          if ($is_client) {
            $args = array('@type' => $info['active title']);
            if ($info['confirmation'] !== FALSE && $info['confirmation'] != 'no-patch') {
              $args['!patch'] = l($operation . '.patch', $this->confirmation_test_patch($operation, $info['confirmation']));
            }
            $step['data'] .= '<div>' . t($info['help'], $args) . '</div>';
          }
          else {
            $step['data'] .= '<div>' . t($info['description']) . '</div>';
          }
        }
      }

      $step['class'] = implode(' ', $step['class']);
      $steps[] = $step;
    }
    return theme('item_list', $steps, NULL, 'ol', array('id' => 'pifr-steps'));
  }

  /**
   * Display detailed information about a test result.
   *
   * @param array $test Test information.
   * @param array $result Test result information.
   * @return string HTML output.
   */
  protected abstract function detail_display(array $test, array $result);

  /**
   * Generate detailed summary display of test results.
   *
   * @param array $test Test information.
   * @param array $result Test result information.
   * @return string HTML output.
   */
  public abstract function detail_summary(array $test, array $result);

  /**
   * Get the title for a review code.
   *
   * @param integer $code Review code.
   * @return string Title representing code.
   */
  public function code_title($code) {
    $step = $this->step_get($code);
    return $step['title'];
  }

  /**
   * Load plugin specific data for a result.
   *
   * @param integer $result_id Result ID.
   * @return array Array of plugin specific data.
   */
  public abstract function load($result_id);

  /**
   * Record plugin specific data for a result.
   *
   * @param integer $result_id Result ID.
   * @param array $data Array of plugin specific data.
   */
  public abstract function record($result_id, array $data);

  /**
   * Delete plugin specific data for a result.
   *
   * @param integer $result_id Result ID.
   * @param array $data Array of plugin specific data.
   */
  public abstract function delete($result_id, array $data);
}
