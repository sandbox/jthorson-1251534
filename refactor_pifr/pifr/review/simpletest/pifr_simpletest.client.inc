<?php

/**
 * @file
 * Provide SimpleTest client review implementation.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

module_load_include('client.inc', 'pifr_drupal');

/**
 * SimpleTest PIFR review implementation.
 */
class pifr_client_review_pifr_simpletest extends pifr_client_review_pifr_drupal {

  /**
   * Add clone-db URL to list of files to be downloaded (if set).
   */
  public function __construct(array $test) {
    parent::__construct($test);

    // If an import database is provided then download the database export.
    if (!empty($this->arguments['simpletest.db'])) {
      $this->test['files'][] = $this->arguments['simpletest.db'];
    }

    // Set SimpleTest variables to ensure that results are left in database and
    // that verbose mode is disabled.
    $this->arguments['drupal.variables'][] = 'simpletest_clear_results:0';
    $this->arguments['drupal.variables'][] = 'simpletest_verbose:0';
  }

  /**
   * Import clone database (if set) before performing Drupal installation.
   */
  protected function install() {
    parent::install();

    if (!$this->has_error() && !empty($this->arguments['simpletest.db'])) {
      // Drupal install() will have taken care of settings.php and such so drop
      // and create database to be filled by clone-db.
      $this->database->drop_database();
      $this->database->create_database();

      // Since a database import is being used the username and password must
      // be included in arguments.
      if (empty($this->arguments['drupal.user']) || empty($this->arguments['drupal.pass'])) {
        $this->set_error(array('@reason' => 'both [drupal.user] and [drupal.pass] must be specified'));
        return;
      }

      $basename = basename($this->arguments['simpletest.db']);
      foreach ($this->files as $file) {
        if (basename($file) == $basename) {
          $this->log('Importing clone database from [' . $basename . ']...');

          if ($result = $this->database->import($file, 'clone_db')) {
            $this->log('Imported [' . $result . '] tables.');
          }
          else {
            $this->set_error(array('@reason' => 'failed to import clone database'));
          }
          break;
        }
      }

      // Reset variables since database was just rewritten.
      $this->install_variables($this->arguments['drupal.variables']);
    }
  }

  /**
   * Run SimpleTest tests.
   */
  protected function review() {
    global $base_url, $base_path;

    // If Drupal 6 then we need to perform SimpleTest installation.
    if ($this->arguments['drupal.core.version'] == 6) {
      if (!$this->drupal6_test_prepare()) {
        return;
      }
    }

    // Login to Drupal HEAD and enable the SimpleTest module.
    $base_url .= '/checkout';
    $base_path = '/checkout';

    $b = new PIFRBrowser();

    pifr_debug('Simpletest test in execution: <pre>%test</pre>', array('%test' => print_r($this->test, TRUE)));
    // Login as admin.
    $user = new stdClass();
    $user->name = $this->admin_username;
    $user->pass_raw = $this->admin_password;
    $b->drupalLogin($user);

    // Enable SimpleTest.
    $edit = array();
    if ($this->arguments['drupal.core.version'] == 6) {
      // Drupal 6.
      $edit['status[simpletest]'] = TRUE;
      $b->drupalPost('admin/build/modules', $edit, t('Save configuration'));
    }
    else {
      // Drupal 7 and above.
      $edit['modules[Core][simpletest][enable]'] = TRUE;
      $b->drupalPost('admin/modules', $edit, t('Save configuration'));
    }

    $base_url = $this->original['base_url'];
    $base_path = $this->original['base_path'];

    if (($b->results['#exception'] + $b->results['#fail']) != 0) {
      $this->set_error(array('@reason' => 'failed to enable simpletest module'));
      return;
    }

    // Assumes it is currently outside of checkout directory.
    $test_list = $this->test_list();

    // Run tests from command line.
    chdir($this->checkout_directory);

    // @TODO: Temporary until #22336 is committed.
    $run_tests = file_exists('scripts/run-tests.sh') ? 'scripts/run-tests.sh' : 'core/scripts/run-tests.sh';
    $url = escapeshellarg(url('', array('absolute' => TRUE)) . 'checkout');

    // Set up 'run-tests.sh' command
    $command = PIFR_CLIENT_PHP . ' ./' . $run_tests . ' --concurrency ' . PIFR_CLIENT_CONCURRENCY . ' --php ' . PIFR_CLIENT_PHP . ' --url ' . $url . ' ' . $test_list;
    $this->log('Cleaning run-tests.sh environment:  ' . $command);

    // Attempt to clear out any tables from previous runs
    if (!$this->exec($command . ' --clean')) {
      $this->set_error(array('@reason' => t('failed during invocation of run-tests.sh --clean')));
      return;
    }

    $this->log('Initiating run-tests.sh with command:  ' . $command);

    // Kick off actual testing
    if (!$this->exec($command)) {
      $this->set_error(array('@reason' => t('failed during invocation of run-tests.sh')));
      return;
    }

    // Get test info for use by test_info_parse().
    if (!$this->exec(PIFR_CLIENT_PHP . ' ./' . $run_tests . ' --php ' . PIFR_CLIENT_PHP . ' --url ' . $url . ' --list')) {
      $this->set_error(array('@reason' => t('failed attempting to get list of tests from run-tests.sh')));
      return;
    }

    chdir($this->original['directory']);
  }

  /**
   * Perform Drupal 6 SimpleTest installation.
   */
  protected function drupal6_test_prepare() {
    // Apply Simpletest core patch.
    $file = $this->module_directory . '/simpletest/D6-core-simpletest.patch';
    chdir($this->checkout_directory);
    $this->log('Applying in ' . getcwd(). ' file=' . $file);
    if (!$this->vcs[$this->test['vcs']['main']['repository']['type']]->apply($file, 0)) {
      $error = array('@filename' => basename($file));
      $this->set_error($error);
    	return FALSE;
    }

    // Move run-tests.sh to scripts directory.
    if (!copy($this->module_directory . '/simpletest/run-tests.sh', 'scripts/run-tests.sh')) {
      $this->set_error(array('@reason' => 'failed to install run-tests.sh'));
      return FALSE;
    }
    chdir($this->original['directory']);

    $this->log('Completed Drupal 6 SimpleTest installation.');

    return TRUE;
  }

  /**
   * Create the test argument for run-tests.sh.
   *
   * @return string Tests argument to be appended to run-test.sh command.
   */
  protected function test_list() {
    if (!empty($this->arguments['simpletest.tests'])) {
      return '--class ' . implode(',', $this->arguments['simpletest.tests']);
    }
    elseif (!empty($this->arguments['drupal.modules'])) {
      $args = array();
      foreach ($this->arguments['drupal.modules'] as $module) {
        // Check the root module directory for a test.
        $file = $this->module_path($module) . '/' . $module . '.test';
        if (file_exists($this->checkout_directory . '/' . $file)) {
          $args[] = $file;
        }

        // Scan each module's tests directory.
        $scan = file_scan_directory($this->checkout_directory . '/' . $this->module_path($module) . '/tests', '\.test$');
        foreach ($scan as $file) {
          $args[] = str_replace($this->checkout_directory . '/', '', $file->filename);
        }
      }
      if (count($args) < 1) {
        $this->set_error(array('@reason' => 'No valid tests specified.  (Empty run-tests.sh --file argument.)'));
      }
      return '--file ' . implode(',', $args);
    }
    elseif (!empty($this->arguments['test.directory.review'][0])) {
      $files = array();
      // Scan the 'test.directory.review' directory for '.test' files
      foreach ($this->arguments['test.directory.review'] as $directory) {
        $scan = file_scan_directory($this->checkout_directory . '/' . $directory, '\.test$');
        foreach ($scan as $file) {
          $files[] = str_replace($this->checkout_directory . '/', '', $file->filename);
        }
      }
      if (count($files) < 1) {
        $this->set_error(array('@reason' => 'No valid tests specified.  (Empty run-tests.sh --file argument.)'));
      }
      return '--file ' . implode(',', $files);
    }
    elseif (!($this->is_core_test())) {
      $this->log("Not a core test.  Scanning modules directory.");
      // Not a core test?  Scan the sites/all/modules directory for tests
      $scan = file_scan_directory($this->checkout_directory . '/' . $this->module_directory, '\.test$');
      foreach ($scan as $file) {
        $files[] = str_replace($this->checkout_directory . '/', '', $file->filename);
      }
      if (count($files) < 1) {
        $this->set_error(array('@reason' => 'No valid tests specified.  (Empty run-tests.sh --file argument.)'));
      }
      return '--file ' . implode(',', $files);
    }
    elseif (PIFR_SHORTCUT_CORE_TEST) {
      return '--class NonDefaultBlockAdmin';
    }
    return '--all';
  }

  /**
   * Attempt to identify whether a test is core
   */
  public function is_core_test() {
    // If project in vcs[main][repository][url] is not drupal, then this is not a core test
    if (str_replace('.' . $this->test['vcs']['main']['repository']['type'], '', array_pop(explode('/', $this->test['vcs']['main']['repository']['url']))) != 'drupal') {
      return FALSE;
    };
    // If above IS drupal, we need to check depedencies
    foreach ($this->test['vcs']['dependencies'] as $module) {
      $project = str_replace('.' . $module['repository']['type'], '', array_pop(explode('/', $module['repository']['url'])));
      switch ($this->argument['drupal.core.version']) {
        case '7':
          // D7 core has no required dependencies. If one exists, it's a module test
          if ($project != 'drupal') {
            return FALSE;
          }
          break;
        case '6':
          // D6 core has a dependency on simpletest.
          if (!in_array($project, array('drupal', 'simpletest'))) {
            return FALSE;
          }
      }
    }
    // If we get to this point, assume a core test
    return TRUE;
  }

  /**
   * Add the SimpleTest specific results.
   */
  public function get_result() {
    $result = parent::get_result();

    // If result code is fail then all the operations passed, so the test
    // results need to be added and the proper result code assigned.
    if ($result['code'] == $this->result_codes['fail']) {
      $result['details'] = array(
        '@pass' => 0,
        '@fail' => 0,
        '@exception' => 0,
        '@debug' => 0,
      );
      $result['data'] = array();

      // Cycle through assertions to fill in results.
      $info = $this->test_info_parse();

      // Process assertions in batches of 10,000 to keep down memory usage.
      $message_id = 0;
      do {
        $assertions = $this->database->query('SELECT *
                                              FROM {simpletest}
                                              WHERE message_id > ' . $message_id . '
                                              ORDER BY message_id
                                              LIMIT 10000');
        $non_pass = 0;
        foreach ($assertions as $assertion) {
          // Fill in details with summary of assertion counts.
          $result['details']['@' . $assertion['status']]++;

          // Initialize each test class.
          if (!isset($result['data'][$assertion['test_class']])) {
            $result['data'][$assertion['test_class']] = array(
              'test_name' => !empty($info[$assertion['test_class']]) ? $info[$assertion['test_class']] : $assertion['test_class'],
              'pass' => 0,
              'fail' => 0,
              'exception' => 0,
              'assertions' => array(),
            );
          }

          // Add up summary totals for test class.
          $result['data'][$assertion['test_class']][$assertion['status']]++;

          // Store non-pass assertion until reaching maximum number.
          if ($assertion['status'] != 'pass' && $non_pass < PIFR_CLIENT_MAX_ASSERTIONS) {
            // Remove non-standard keys.
            unset($assertion['message_id']);
            unset($assertion['test_id']);

            // Make file only include name, instead of absolute reference.
            $assertion['file'] = basename($assertion['file']);

            // Add assertion to set of non-pass assertions for the class.
            $result['data'][$assertion['test_class']]['assertions'][] = $assertion;

            // Keep a record of the number of non-pass assertions.
            $non_pass++;
          }
        }

        $message_id += 10000;
      }
      while ($assertions);

      // Remove class name keys.
      $result['data'] = array_values($result['data']);

      // Assign the code based on the test results.
      $result['code'] = $this->result_codes[$result['details']['@fail'] + $result['details']['@exception'] ? 'fail' : 'pass'];
    }
    return $result;
  }

  /**
   * Parse test info from 'run-tests.sh --list' output.
   *
   * @return array Associative array of test info keyed by test case.
   */
  protected function test_info_parse() {
    if (!is_array($this->output)) {
      $this->output = explode("\n", $this->output);
    }
    $this->output = array_slice($this->output, 4);
    $group = 'Unknown';
    $info = array();
    foreach ($this->output as $line) {
      if (isset($line[1]) && $line[1] == '-') {
        if (preg_match('/^ - (.*?) \((\w+)\)$/m', $line, $match)) {
          $info[$match[2]] = $match[1] . ' (' . $match[2] . ') [' . $group . ']';
        }
      }
      else {
        $group = $line;
      }
    }
    return $info;
  }

  /**
   * If on review operation then display the assertion count.
   */
  public static function status() {
    if (variable_get('pifr_client_operation', 'init') == 'review') {
      module_load_include('review.inc', 'pifr_client');
      $type = pifr_client_review_database_type();

      require_once drupal_get_path('module', 'pifr_client') . '/review/db/db.inc';
      require_once drupal_get_path('module', 'pifr_client') . '/review/db/' . $type. '.inc';

      $database = 'pifr_client_db_interface_' . $type;
      $database = new $database;

      $count = $database->query('SELECT COUNT(*) FROM {simpletest}');
      return t('Running tests, @count assertion(s).', array('@count' => number_format($count ? $count[0]['COUNT(*)'] : 0)));
    }
    return parent::status();
  }
}
