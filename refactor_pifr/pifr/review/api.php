<?php

/**
 * @file
 * Provide documentation of the PIFR shared API.
 *
 * The shared API includes both review plug-in API and hooks that can be
 * implemented by any module.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Registers the module as a pifr review plugin.
 *
 * @return array Associative array keyed by the module name with the value to
 *   display to the user when referencing the plugin.
 */
function hook_pifr_info() {
  return array(
    'mymodule' => t('My module'),
  );
}
