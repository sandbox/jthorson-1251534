<?php

/**
 * @file
 * Provide Coder server review implementation.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

module_load_include('server.inc', 'pifr_drupal');

/**
 * Coder server review implementation.
 */
class pifr_server_review_pifr_coder extends pifr_server_review_pifr_drupal {

  protected $labels = array(
    'pass' => 'minor',
    'fail' => 'critical',
    'exception' => 'normal',
  );

  protected $detail_message = '@pass minor(s), @fail critical(s), and @exception normal(s)';

  /**
   * Modify step information and append default arguments.
   */
  public function __construct() {
    parent::__construct();

    $this->argument_default += array(
      'coder.annotate' => FALSE,
      'coder.reviews' => array('comment', 'sql', 'i18n', 'style', 'security', 'coder_tough_love'),
    );

    unset($this->steps['install']);
    $this->steps['review'] = array_merge($this->steps['review'], array(
      'confirmation' => FALSE,
    ));
    $this->steps['fail'] = array_merge($this->steps['fail'], array(
      'title' => 'coder flags',
      'active title' => 'detect coder flags',
      'description' => 'Ensure that your code follows the Drupal standard and passes a coder & tough love review.',
      'confirmation' => 'pifr_coder',
    ));
    $this->steps['pass'] = array_merge($this->steps['pass'], array(
      'title' => 'coder review passed',
      'active title' => 'complete coder review with all passed',
      'summary' => '@pass minor(s)',
      'confirmation' => 'pifr_coder',
    ));
  }
}
