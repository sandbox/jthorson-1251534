<?php

/**
 * @file
 * Provide Coder client review implementation.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

module_load_include('client.inc', 'pifr_drupal');

/**
 * Coder client review implementation.
 */
class pifr_client_review_pifr_coder extends pifr_client_review_pifr_drupal {

  /**
   * Maximum run time for coder review.
   *
   * @var integer
   */
  protected $max_run_time = 600;

  /**
   * Code results collected during review().
   *
   * @var array
   */
  protected $results = array();

  public function __construct(array $test) {
    parent::__construct($test);

    variable_set('pifr_coder_status', array(
      'position' => 0,
      'total' => 0,
    ));
  }

  /**
   * Perform coder review.
   */
  protected function review() {
    // Since the coder reviews are run inside this script instead of externally
    // the script need to have enough time to finish.
    set_time_limit($this->max_run_time);

    // Get the review information for the selected reviews.
    $reviews = _coder_reviews();
    foreach ($reviews as $key => $review) {
      if (!in_array($key, $this->arguments['coder.reviews'], TRUE)) {
        unset($reviews[$key]);
      }
    }

    // Prepare the base arguments array.
    $php_extensions = array('php', 'inc', 'install', 'module', 'test');
    $args = array(
      '#reviews' => $reviews,
      '#severity' => SEVERITY_MINOR,
      '#filename' => '',
      '#patch' => '',
      '#php_extensions' => $php_extensions,
      '#include_extensions' => _coder_get_reviews_extensions($php_extensions, $reviews),
    );

    // Update the Coder severity level if desired
    if (!empty($this->arguments['coder.severity'])) {
      $args['#severity'] = $this->arguments['coder.severity'];
    }

    // Cycle through all relevant files and perform review.
    $files = $this->syntax_ignore($this->file_list());

    // Keep track of status for use by status().
    $status = variable_get('pifr_coder_status', array(
      'position' => 0,
      'total' => 0,
    ));
    $status['total'] = count($files);

    $not_found = 0;
    foreach ($files as $file) {
      // Update variable status to keep status() up-to-date.
      $status['position']++;
      variable_set('pifr_coder_status', $status);

      $filepath = $this->checkout_directory . '/' . $file;

      // Ensure that the file exists and is not a reference to a file that was
      // removed in a patch, or the like.
      if (!file_exists($filepath)) {
        $not_found++;
        continue;
      }

      $args['#filename'] = $filepath;
      $this->results[$file] = do_coder_reviews($args);

      // If annotate is TRUE, then set the group to the responsible party.
      if ($this->arguments['coder.annotate']) {
        $this->review_annotate($file, $this->results[$file]);
      }
    }

    $this->log('Reviewed [' . (count($files) - $not_found) . ' of ' . count($files) . '] relevant file(s).');
  }

  /**
   * Get a list of files that should be reviewed by coder.
   *
   * @return array List of files that should be reviewed by coder.
   */
  protected function file_list() {
    if (!empty($this->test['files'])) {
      return $this->syntax_files();
    }
    elseif (!empty($this->arguments['test.files'])) {
      return $this->syntax_files();
    }
    elseif (!empty($this->arguments['drupal.modules'])) {
      return $this->syntax_files();
    }
    elseif (!empty($this->test['review']['argument']['directory'])) {
      $files = $this->syntax_files();
      foreach ($files as $key => $file) {
        if (strpos($file, $this->test['review']['argument']['directory']) !== 0) {
          unset($files[$key]);
        }
      }
      return $files;
    }
    elseif (PIFR_SHORTCUT_CORE_TEST) {
      return array('index.php');
    }
    return $this->syntax_files();
  }

  /**
   * Use the responsible party as the group for assertions.
   *
   * @param string $file File path relative checkout directory.
   * @param array $results Coder assertion results.
   */
  protected function review_annotate($file, array &$results) {
    // TODO Provide mechanism to detect if file is located in dependency
    // repository and use the related vcs backend.
    $type = $this->test['vcs']['main']['repository']['type'];

    // Run the annotate method relative to checkout directory.
    chdir($this->checkout_directory);
    $map = $this->vcs[$type]->annotate($file);
    chdir($this->original['directory']);

    // Cycle through the results and set the group to the responsible party.
    foreach ($results as $key => &$result) {
      // Only process the entries that have a numeric key in order to ignore
      // the summary result entry.
      if (is_numeric($key)) {
        if (!empty($map[$result['line']])) {
          $result['message_group'] = $map[$result['line']];
        }
      }
    }
  }

  /**
   * Add the Coder results.
   */
  public function get_result() {
    $result = parent::get_result();

    // If result code is fail then all the operations passed, so the test
    // results need to be added and the proper result code assigned.
    if ($result['code'] == $this->result_codes['fail']) {
      $result['details'] = array(
        '@pass' => 0,
        '@fail' => 0,
        '@exception' => 0,
      );
      $result['data'] = array();

      // Cycle through results for each file.
      $count = 0;
      foreach ($this->results as $file => $review) {
        $result['data'][$file] = array(
          'test_name' => $file,
          'pass' => 0,
          'fail' => 0,
          'exception' => 0,
          'assertions' => array(),
        );

        unset($review['#stats']);

        foreach ($review as $assertion) {
          // Fill in details with summary of assertion counts.
          $result['details']['@' . $assertion['status']]++;

          // Add up summary totals for test class.
          $result['data'][$file][$assertion['status']]++;

          // Store non-pass assertion until reaching maximum number.
          if ($count < PIFR_CLIENT_MAX_ASSERTIONS) {
            // Make file only include name, instead of absolute reference.
            $assertion['file'] = basename($file);

            // Add assertion to set of non-pass assertions for the class.
            $result['data'][$file]['assertions'][] = $assertion;

            // Keep a record of the number of assertions stored.
            $count++;
          }
        }
      }

      // Remove file name keys.
      $result['data'] = array_values($result['data']);

      // Assign the code bassed on the test results.
      $result['code'] = $this->result_codes[$result['details']['@fail'] + $result['details']['@exception'] ? 'fail' : 'pass'];
    }
    return $result;
  }

  /**
   * If on review operation then display the coder review position.
   */
  public static function status() {
    if (variable_get('pifr_client_operation', 'init') == 'review') {
      $status = variable_get('pifr_coder_status', array(
        'position' => 0,
        'total' => 0,
      ));

      return t('Reviewing file @position of @total.', array(
        '@position' => number_format($status['position']),
        '@total' => number_format($status['total']),
      ));
    }
    return parent::status();
  }
}
