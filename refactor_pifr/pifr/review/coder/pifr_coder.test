<?php
/**
 * @file
 * Ensure that the coder review functionality works.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

module_load_include('test', 'pifr_drupal');

/**
 * Ensure that the coder review functionality works.
 */
class PIFRClientCoderTestCase extends PIFRClientDrupalTestCase {

  /**
   * Add SimpleTest review information.
   */
  public function __construct($test_id = NULL) {
    parent::__construct($test_id);

    $this->test = array_replace_recursive($this->test, array(
      'review' => array(
        'plugin' => 'pifr_coder',
      ),
    ));
  }

  public static function getInfo() {
    return array(
      'name' => 'PIFR Coder',
      'description' => 'Ensure that the Coder review functionality works.',
      'group' => 'PIFR Plugins',
    );
  }

  protected function setUp() {
    parent::setUp('pifr', 'pifr_client', 'pifr_assertion', 'pifr_drupal', 'pifr_coder');
  }

  /**
   * Ensure that the coder review functionality works.
   */
  protected function testReview() {
    // Setup test and perform review.
    $test = array_replace_recursive($this->test, array(
      'files' => array(
        url(drupal_get_path('module', 'pifr_coder') . '/confirmation/pass.patch', array('absolute' => TRUE)),
      ),
    ));
    $result = $this->review($test);

    // Ensure that result is correct.
    $this->assertEqual($result['code'], 9, t('Core review passed.'));

    $this->assertLogText('Fetched [pass.patch].');
    $this->assertLogText("Applied [pass.patch] to:\n > coder.php");

    // Since pass.patch adds a single file then syntax and coder reviews should
    // only be performed on one file.
    $this->assertLogText('Checked [1 of 1] relevant file(s) for syntax.');
    $this->assertLogText('Reviewed [1 of 1] relevant file(s).');
  }
}
