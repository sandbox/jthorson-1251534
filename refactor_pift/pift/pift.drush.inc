<?php
/**
 * @file
 * Provide Drush integration.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Implements hook_drush_command().
 */
function pift_drush_command() {
  $items = array();

  $items['pift-cron'] = array(
    'description' => 'Requeue expired test results, test out batch, and retrieve results.',
  );

  return $items;
}

/**
 * Requeue expired test results, test out batch, and retrieve results.
 */
function drush_pift_cron() {
  module_load_include('cron.inc', 'pift');

  // Requeue all tests that have passed the re-test interval.
//   pift_cron_retest(); TODO fix query.

  // Send a batch of queued tests.
  pift_cron_queue_batch();

  // Retrieve any results that have occured since last cron run.
  pift_cron_retrieve_results();
}
